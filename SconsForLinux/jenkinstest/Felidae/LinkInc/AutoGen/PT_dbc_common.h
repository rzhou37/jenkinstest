/**
 * This is an auto-generated file from C:\C\Common\CAN_PT.dbc. 
 * Generated at: 2017-07-27 10:36:49
 */

#ifndef _PT_DBC_COMMON_H_
#define _PT_DBC_COMMON_H_

#include <string.h>
#include "typedefs.h"

#define CAN_MSG_CLEAR(msg)      memset(&(msg), 0, sizeof(msg))

#endif /* _PT_DBC_COMMON_H_ */