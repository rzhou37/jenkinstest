/**
 *######################################################################
 *                (c) Copyright 2011 Freescale Semiconductor, Inc.
 *                         ALL RIGHTS RESERVED.
 *######################################################################
 *
 *    @file    linflex_init.h
 *    @version M4_SRC_SW_VERSION_MAJOR.M4_SRC_SW_VERSION_MINOR.M4_SRC_SW_VERSION_PATCH
 *
 *    @brief   This file initializes LINFlex registers.
 *    @details This file initializes LINFlex registers.
 *
 *    Project M4_SRC_PROJECT_NAME
 *    Platform M4_SRC_MCU_FAMILY
 *
 *   Creation date:		2-Aug-2011
 *   Author:                         b13508
 *
 */

#ifndef _LINFLEX_H
#define _LINFLEX_H
#ifdef __cplusplus

extern "C"{

#endif

  /******************************************************************************
   *                  Includes
   ******************************************************************************/
#include "mpc5643l.h"

  /******************************************************************************
   *                   Global function prototypes
   ******************************************************************************/
  void linflex_init_fnc(void);
  void linflex_siu_init(void);
  
extern void LINFlex_init(void);

#ifdef __cplusplus

}
#endif
#endif                                 /*_LINFLEX_H*/

/*
 *######################################################################
 *                           End of File
 *######################################################################
 */
