/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-06-29
* Description: PWM configurations
***************************************************************************************************/

#ifndef _FLEXPWM_INIT_H
#define _FLEXPWM_INIT_H
#ifdef __cplusplus

extern "C"{

#endif

  /******************************************************************************
   *                  Includes
   ******************************************************************************/
//#include "target.h"
#include "mpc5643l.h"
#include "MCTransform.h"
#include "FlexPWM_564xL_library.h"
  /******************************************************************************
   *                   Global function prototypes
   ******************************************************************************/
#define PWM_HALF_PERIOD 10000U

void PWM_init(void);
extern void PWM_svmGenerateor(MCT_transformFrameAlphaBeta_S* vAB, uint32_t pwmHalfPeriod, tFloat vDcBus,
        uint32_t* tAout, uint32_t* tBout, uint32_t* tCout);
extern void PWM_update(uint8_t pwmMod, uint8_t subModB, uint8_t
        subModC, uint32_t Freq, uint16_t resolution, uint16_t DutyA, uint16_t DutyB,
        uint16_t DutyC,
        uint16_t outTrigA, uint16_t outTrigB, uint16_t outTrigC, uint16_t DeadTime,
        uint16_t OCTRL_A,
        uint16_t OCTRL_B, uint16_t OCTRL_C);
#ifdef __cplusplus

}
#endif
#endif                                 /*_FLEXPWM_INIT_H*/
/*
 *######################################################################
 *                           End of File
 *######################################################################
 */
