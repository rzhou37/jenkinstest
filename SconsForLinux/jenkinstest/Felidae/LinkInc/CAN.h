/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-07-11
* Description: CAN configuration and interrupts.
***************************************************************************************************/

#ifndef CAN_H_
#define CAN_H_

extern void CAN_init(void);
extern void CAN_1_buf0_3_isr(void);
extern void CAN_1_buf4_7_isr(void);
extern void CAN_100Hz(void);
extern void CAN_1Hz(void);

#endif /* CAN_H_ */
