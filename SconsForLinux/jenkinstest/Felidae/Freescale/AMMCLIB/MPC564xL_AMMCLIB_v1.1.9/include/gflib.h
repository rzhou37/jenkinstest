/******************************************************************************
*
*   (c) Copyright 2017 NXP Semiconductors
*   All Rights Reserved.
*
******************************************************************************/
/*!
*
* @file     gflib.h
*
* @version  1.0.10.0
*
* @date     Feb-20-2017
*
* @brief    Master header file for GFLIB library.
*
******************************************************************************/
#ifndef GFLIB_H
#define GFLIB_H

/******************************************************************************
* Includes
******************************************************************************/
#include "GFLIB_Sin.h"
#include "GFLIB_SinCos.h"
#include "GFLIB_Cos.h"
#include "GFLIB_Tan.h"
#include "GFLIB_Sqrt.h"
#include "GFLIB_ControllerPIr.h"
#include "GFLIB_ControllerPIrAW.h"
#include "GFLIB_ControllerPIp.h"
#include "GFLIB_ControllerPIpAW.h"
#include "GFLIB_Asin.h"
#include "GFLIB_Acos.h"
#include "GFLIB_Atan.h"
#include "GFLIB_AtanYX.h"
#include "GFLIB_Sign.h"
#include "GFLIB_Lut1D.h"
#include "GFLIB_Lut2D.h"
#include "GFLIB_VectorLimit.h"
#include "GFLIB_Limit.h"
#include "GFLIB_LowerLimit.h"
#include "GFLIB_UpperLimit.h"
#include "GFLIB_Hyst.h"
#include "GFLIB_IntegratorTR.h"
#include "GFLIB_Ramp.h"
#include "GFLIB_AtanYXShifted.h"
#include "GFLIB_Log10.h"
#include "GFLIB_VLog10.h"

#endif /* GFLIB_H */
