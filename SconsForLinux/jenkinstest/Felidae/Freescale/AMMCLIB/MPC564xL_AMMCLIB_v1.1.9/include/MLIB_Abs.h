/******************************************************************************
*
*   (c) Copyright 2017 NXP Semiconductors
*   All Rights Reserved.
*
******************************************************************************/
/*!
*
* @file       MLIB_Abs.h
*
* @version    1.0.67.0
*
* @date       Mar-8-2017
*
* @brief      Header file for MLIB_Abs function
*
******************************************************************************/
#ifndef MLIB_ABS_H
#define MLIB_ABS_H
/*!
@if MLIB_GROUP
    @addtogroup MLIB_GROUP
@else
    @defgroup MLIB_GROUP   MLIB
@endif
*/

#ifdef __cplusplus
extern "C" {
#endif

/**
* @page misra_violations MISRA-C:2004 violations
* 
* @section MLIB_Abs_h_REF_1
* Violates MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of disallowed macro 
* definition. 
* 
* @section MLIB_Abs_h_REF_2
* Violates MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires the function-like macro 
* definition. 
* 
* @section MLIB_Abs_h_REF_3
* Violates MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter in definition of macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of unparenthesized 
* macro parameters. 
* 
* @section MLIB_Abs_h_REF_4
* Violates MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of '#/##' operators. 
* 
* @section MLIB_Abs_h_REF_5
* Violates MISRA 2004 Required Rule 8.5, Object/function definition in header file. 
* Allowing the inline functions significantly increase the speed of the library thus the lowest 
* layer (MLIB) is implemented as inline. This approach removes the overhead caused by standard 
* function calling. 
*
* @section MLIB_Abs_h_REF_6
* Violates MISRA 2004 Required Rule 10.1, Prohibited Implicit Conversion.
* This violation originates within a header provided by the compiler.
*/

#include "SWLIBS_Defines.h"

/****************************************************************************
* Defines and macros            (scope: module-local)
****************************************************************************/
#ifndef  _MATLAB_BAM_CREATE
  /*
  * @violates @ref MLIB_Abs_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
  * @violates @ref MLIB_Abs_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_Abs_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter 
  * in definition of macro. 
  */
  #define MLIB_Abs(...)     macro_dispatcher(MLIB_Abs, __VA_ARGS__)(__VA_ARGS__)     /*!< This function returns absolute value of input parameter. */

  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F32)
    /*
    * @violates @ref MLIB_Abs_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_Abs_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_Abs_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00006 */  
    #define MLIB_Abs_Dsptchr_1(In)     MLIB_Abs_Dsptchr_2(In,F32)     /*!< Function dispatcher for MLIB_Abs_Dsptchr_1, do not modify!!! */
  #endif 
  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F16)
    /*
    * @violates @ref MLIB_Abs_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_Abs_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_Abs_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00006 */
    #define MLIB_Abs_Dsptchr_1(In)     MLIB_Abs_Dsptchr_2(In,F16)     /*!< Function dispatcher for MLIB_Abs_Dsptchr_1, do not modify!!! */
  #endif 
#if (SWLIBS_SUPPORT_FLT == SWLIBS_STD_ON)
  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_FLT)
    /*
    * @violates @ref MLIB_Abs_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_Abs_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_Abs_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00006 */
    #define MLIB_Abs_Dsptchr_1(In)     MLIB_Abs_Dsptchr_2(In,FLT)     /*!< Function dispatcher for MLIB_Abs_Dsptchr_1, do not modify!!! */
  #endif 
#endif /* SWLIBS_SUPPORT_FLT == SWLIBS_STD_ON */

  /*
  * @violates @ref MLIB_Abs_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
  * @violates @ref MLIB_Abs_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_Abs_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter 
  * in definition of macro. 
  */
  #define MLIB_Abs_Dsptchr_2(In,Impl)    MLIB_Abs_Dsptchr_(In,Impl)     /*!< Function dispatcher for MLIB_Abs_Dsptchr_2, do not modify!!! */
  
  /*
  * @violates @ref MLIB_Abs_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
  * @violates @ref MLIB_Abs_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_Abs_h_REF_4 MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
  */
  /** @remarks Implements DMLIB00008 */
  #define MLIB_Abs_Dsptchr_(In,Impl)     MLIB_Abs_##Impl(In)            /*!< Function dispatcher for MLIB_Abs_Dsptchr_, do not modify!!! */
#endif
 
/****************************************************************************
* Typedefs and structures       (scope: module-local)
****************************************************************************/

/****************************************************************************
* Exported function prototypes
****************************************************************************/

/****************************************************************************
* Inline functions
****************************************************************************/
/* MLIB_Abs_F32 implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac32 Abs_F32_C(register tFrac32 f32In)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file.
*/
{
  return((f32In < (tFrac32)0) ? (-f32In) : (f32In));
}
#if (defined(MLIB_SPE1) || defined(MLIB_SPE2))
  #if defined(__ghs__)
  INLINE tFrac32 Abs_F32_GHS_SPE12(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
    * file. 
    */
    register tFrac32 f32Return;
    #pragma ghs optasm
    asm("evabs %0,%1" : "=r"(f32Return) : "r"(f32In));
    return((tFrac32)f32Return);
  }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  /* MLIB_Abs_F32 implementation variant - SPE1/2 assembly for CW toolchain */
  #pragma always_inline on
  INLINE tFrac32 Abs_F32_CW_SPE12(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
    * file. 
    */
    register tFrac32 f32Return;
    asm("evabs %0,%1" : "=r"(f32Return) : "r"(f32In));
    return((tFrac32)f32Return);
  }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file.
  */
  /* MLIB_Abs_F32 implementation variant - SPE1/2 assembly for S32DS for Power Architecture toolchain */
  INLINE tFrac32 Abs_F32_S32DSPPC_SPE12(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
    * file. 
    */
    register tFrac32 f32Return;
    __asm__ volatile("evabs %0,%1" : "=r"(f32Return) : "r"(f32In));
    return((tFrac32)f32Return);
  }
  #endif
#endif /* if (defined(MLIB_SPE1) || defined(MLIB_SPE2)) */
/* MLIB_Abs_F16 implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac16 Abs_F16_C(register tFrac16 f16In)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
* file.
*/
{
  return((f16In < (tFrac16)0) ? (-f16In) : (f16In));
}
#ifdef MLIB_SPE1
  #if defined(__ghs__)
  /* MLIB_Abs_F16 implementation variant - SPE1 assembly for GHS toolchain */
  INLINE tFrac16 Abs_F16_GHS_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file. 
  */
  {
    /*
    * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
    * file.
    */
    register tFrac32 f32Return;
    #pragma ghs optasm
    asm("evslwi %0,%1,16" : "=r"(f32Return) : "r"(f16In));
    #pragma ghs optasm
    asm("evabs %0,%1" : "=r"(f32Return) : "r"(f32Return));
    #pragma ghs optasm
    asm("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
    return((tFrac16)f32Return);
  }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  /* MLIB_Abs_F16 implementation variant - SPE1 assembly for CW toolchain */
  #pragma always_inline on
  INLINE tFrac16 Abs_F16_CW_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
    * file.
    */
    register tFrac32 f32Return;
    asm("evslwi %0,%1,16" : "=r"(f32Return) : "r"(f16In));
    asm("evabs %0,%1" : "=r"(f32Return) : "r"(f32Return));
    asm("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
    return((tFrac16)f32Return);
  }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file.
  */
  /* MLIB_Abs_F16 implementation variant - SPE1 assembly for S32DS for Power Architecture toolchain */
  INLINE tFrac16 Abs_F16_S32DSPPC_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
    * file.
    */
    register tFrac32 f32Return;
    __asm__ volatile("evslwi %0,%1,16" : "=r"(f32Return) : "r"(f16In));
    __asm__ volatile("evabs %0,%1" : "=r"(f32Return) : "r"(f32Return));
    __asm__ volatile("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
    return((tFrac16)f32Return);
  }
  #endif
#endif /* ifdef MLIB_SPE1 */
#if (SWLIBS_SUPPORT_FLT == SWLIBS_STD_ON)
/* MLIB_Abs_FLT implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFloat Abs_FLT_C(register tFloat fltIn)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
* file.
*/
{
  return((fltIn<(tFloat)0) ? (-fltIn) : (fltIn));
}
#if defined(__ghs__)
/* MLIB_Abs_FLT implementation variant - EFPU2 assembly for GHS toolchain */
INLINE tFloat Abs_FLT_GHS_EFPU2(register tFloat fltIn)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file. 
*/
{
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file. 
  */
  register tFloat fltReturn;
  #pragma ghs optasm
  asm("efsabs %0,%1" : "=r"(fltReturn) : "r"(fltIn));
  return((tFloat)fltReturn);
}
#elif defined(__CWCC__) || defined(__MWERKS__)
/* MLIB_Abs_FLT implementation variant - EFPU2 assembly for CW toolchain */
#pragma always_inline on
INLINE tFloat Abs_FLT_CW_EFPU2(register tFloat fltIn)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
* file.
*/
{
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file. 
  */
  register tFloat fltReturn;
  asm("efsabs %0,%1" : "=r"(fltReturn) : "r"(fltIn));
  return((tFloat)fltReturn);
}
#elif defined(__GNUC__) && defined(__PPC_EABI__)
/* MLIB_Abs_FLT implementation variant - EFPU2 assembly for S32DS for Power Architecture toolchain */
INLINE tFloat Abs_FLT_S32DSPPC_EFPU2(register tFloat fltIn)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
* file.
*/
{
  /*
  * @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file. 
  */
  register tFloat fltReturn;
  __asm__ volatile("efsabs %0,%1" : "=r"(fltReturn) : "r"(fltIn));
  return((tFloat)fltReturn);
}
#endif
#endif /* SWLIBS_SUPPORT_FLT == SWLIBS_STD_ON */





/****************************************************************************
* Implementation variant: 32-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@brief        This function returns absolute value of input parameter.

@param[in]    f32In      Input value.

@return       Absolute value of input parameter.

@details      The input value as well as output value is considered as 32-bit fractional data type.
              The output saturation is not implemented in this function, thus in case the absolute
              value of input parameter is outside the [-1, 1) interval, the output value will
              overflow.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_Abs_F32
              \image rtf absEq1_f32.math "MLIB_Abs_Eq1"

*/
/*!
@note         Due to effectivity reason this function is written as inline assembly, and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac32 f32In;
tFrac32 f32Out;

void main(void)
{
    // input value = -0.25
    f32In = FRAC32(-0.25);

    // output should be FRAC32(0.25)
    f32Out = MLIB_Abs_F32(f32In);

    // output should be FRAC32(0.25)
    f32Out = MLIB_Abs(f32In, F32);

    // ##############################################################
    // Available only if 32-bit fractional implementation selected
    // as default
    // ##############################################################

    // output should be FRAC32(0.25)
    f32Out = MLIB_Abs(f32In);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif /* if defined __CWCC__ || defined __MWERKS__ */
/** @remarks Implements DMLIB00007, DMLIB00000, DMLIB00003, DMLIB00005 */
INLINE tFrac32 MLIB_Abs_F32(register tFrac32 f32In)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file. 
*/
{
  #if defined(__ghs__)
      /** @remarks Implements DMLIB00004 */
      return(Abs_F32_GHS_SPE12(f32In));
  #elif defined(__CWCC__) || defined(__MWERKS__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_F32_CW_SPE12(f32In));
  #elif defined(__DCC__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_F32_C(f32In));
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00004 */
      return(Abs_F32_S32DSPPC_SPE12(f32In));
  #else
    /** @remarks Implements DMLIB00004 */
    return(Abs_F32_C(f32In));
  #endif
}





/****************************************************************************
* Implementation variant: 16-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@param[in]    f16In      Input value.

@return       Absolute value of input parameter.

@details      The input value as well as output value is considered as 16-bit fractional data type.
              The output saturation is not implemented in this function, thus in case the absolute
              value of input parameter is outside the [-1, 1) interval, the output value will
              overflow.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_Abs_F16
              \image rtf absEq1_f16.math "MLIB_Abs_Eq1"

*/
/*!
@note         Due to effectivity reason this function is written as inline assembly, and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac16 f16In;
tFrac16 f16Out;

void main(void)
{
    // input value = -0.25
    f16In = FRAC16(-0.25);

    // output should be FRAC16(0.25)
    f16Out = MLIB_Abs_F16(f16In);

    // output should be FRAC16(0.25)
    f16Out = MLIB_Abs(f16In, F16);

    // ##############################################################
    // Available only if 16-bit fractional implementation selected
    // as default
    // ##############################################################

    // output should be FRAC16(0.25)
    f16Out = MLIB_Abs(f16In);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif /* if defined __CWCC__ || defined __MWERKS__ */
/** @remarks Implements DMLIB00007, DMLIB00000, DMLIB00002, DMLIB00005 */
INLINE tFrac16 MLIB_Abs_F16(register tFrac16 f16In)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file. 
*/
{
  #if defined(__ghs__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_F16_GHS_SPE1(f16In));
  #elif defined(__CWCC__) || defined(__MWERKS__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_F16_CW_SPE1(f16In));
  #elif defined(__DCC__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_F16_C(f16In));
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_F16_S32DSPPC_SPE1(f16In));
  #else
    /** @remarks Implements DMLIB00004 */
    return(Abs_F16_C(f16In));
  #endif
}





#if (SWLIBS_SUPPORT_FLT == SWLIBS_STD_ON)
/****************************************************************************
* Implementation variant: Single precision floating point
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@param[in]    fltIn      Input value.

@return       Absolute value of input parameter.

@details      The input value as well as output value is considered as single precision floating
              point data type.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_Abs_FLT
              \image rtf absEq1_flt.math "MLIB_Abs_Eq1"

*/
/*!
@note         The function may raise floating-point exceptions (floating-point
              inexact, invalid operation).

@note         Due to effectivity reason this function is implemented as inline assembly, and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFloat fltIn;
tFloat fltOut;

void main(void)
{
    // input value = -0.25
    fltIn = (tFloat)-0.25;

    // output should be 0.25
    fltOut = MLIB_Abs_FLT(fltIn);

    // output should be 0.25
    fltOut = MLIB_Abs(fltIn, FLT);

    // ##############################################################
    // Available only if single precision floating point
    // implementation selected as default
    // ##############################################################

    // output should be 0.25
    fltOut = MLIB_Abs(fltIn);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif /* if defined __CWCC__ || defined __MWERKS__ */
/** @remarks Implements DMLIB00007, DMLIB00000, DMLIB00001, DMLIB00005 */
INLINE tFloat MLIB_Abs_FLT(register tFloat fltIn)
/*
* @violates @ref MLIB_Abs_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file. 
*/
{
  #if defined(__ghs__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_FLT_GHS_EFPU2(fltIn));
  #elif defined(__CWCC__) || defined(__MWERKS__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_FLT_CW_EFPU2(fltIn));
  #elif defined(__DCC__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_FLT_C(fltIn));
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
    /** @remarks Implements DMLIB00004 */
    return(Abs_FLT_S32DSPPC_EFPU2(fltIn));
  #else
    /** @remarks Implements DMLIB00004 */
    return(Abs_FLT_C(fltIn));
  #endif
}


#endif /* SWLIBS_SUPPORT_FLT == SWLIBS_STD_ON */
#ifdef __cplusplus
}
#endif

#endif /* MLIB_ABS_H */
