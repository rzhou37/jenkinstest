/******************************************************************************
*
*   (c) Copyright 2017 NXP Semiconductors
*   All Rights Reserved.
*
******************************************************************************/
/*!
*
* @file     gdflib.h
*
* @version  1.0.8.0
*
* @date     Jan-31-2017
*
* @brief    Master header file.
*
******************************************************************************/
#ifndef GDFLIB_H
#define GDFLIB_H

/******************************************************************************
* Includes
******************************************************************************/
#include "GDFLIB_FilterIIR1.h"
#include "GDFLIB_FilterIIR2.h"
#include "GDFLIB_FilterFIR.h"
#include "GDFLIB_FilterMA.h"

#endif /* GDFLIB_H */
