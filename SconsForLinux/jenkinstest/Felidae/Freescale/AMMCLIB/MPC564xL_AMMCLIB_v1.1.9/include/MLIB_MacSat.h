/******************************************************************************
*
*   (c) Copyright 2017 NXP Semiconductors
*
******************************************************************************/
/*!
*
* @file       MLIB_MacSat.h
*
* @version    1.0.77.0
*
* @date       Feb-27-2017
*
* @brief      Header file for MLIB_MacSat function
*
******************************************************************************/
#ifndef MLIB_MACSAT_H
#define MLIB_MACSAT_H
/*!
@if MLIB_GROUP
    @addtogroup MLIB_GROUP
@else
    @defgroup MLIB_GROUP   MLIB
@endif
*/

#ifdef __cplusplus
extern "C" {
#endif

/**
* @page misra_violations MISRA-C:2004 violations
* 
* @section MLIB_MacSat_h_REF_1
* Violates MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of disallowed macro 
* definition. 
* 
* @section MLIB_MacSat_h_REF_2
* Violates MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires the function-like macro 
* definition. 
* 
* @section MLIB_MacSat_h_REF_3
* Violates MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter in definition of macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of unparenthesized 
* macro parameters. 
* 
* @section MLIB_MacSat_h_REF_4
* Violates MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of '#/##' operators. 
* 
* @section MLIB_MacSat_h_REF_5
* Violates MISRA 2004 Required Rule 8.5, Object/function definition in header file. 
* Allowing the inline functions significantly increase the speed of the library thus the lowest 
* layer (MLIB) is implemented as inline. This approach removes the overhead caused by standard 
* function calling. 
*
* @section MLIB_MacSat_h_REF_6
* Violates MISRA 2004 Required Rule 10.1, Prohibited Implicit Conversion.
* This violation originates within a header provided by the compiler.
*
* @section MLIB_MacSat_h_REF_7
* Violates MISRA 2004 Required Rule 10.3, Cast of complex expression changes signedness.
* The cast is applied to a SIMD data type which does not posess signedness.
*/
#include "SWLIBS_Defines.h"
#ifndef AMMCLIB_TESTING_ENV
  #include "MLIB_AddSat.h"
  #include "MLIB_MulSat.h"
  #include "MLIB_ConvertPU.h"
#else
  /* Following includes serve for NXP internal testing purposes only. 
  *  These headers are not part of the release. */
  #include "CCOV_MLIB_AddSat.h"
  #include "CCOV_MLIB_MulSat.h"
  #include "CCOV_MLIB_ConvertPU.h"
#endif

/****************************************************************************
* Defines and macros            (scope: module-local)
****************************************************************************/
#ifndef  _MATLAB_BAM_CREATE
  /*
  * @violates @ref MLIB_MacSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_MacSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_MacSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
  * parameter in definition of macro. 
  */
  #define MLIB_MacSat(...)     macro_dispatcher(MLIB_MacSat, __VA_ARGS__)(__VA_ARGS__)     /*!< This function implements the multiply accumulate function saturated if necessary. */

  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F32)
    /*
    * @violates @ref MLIB_MacSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_MacSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_MacSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00227 */
    #define MLIB_MacSat_Dsptchr_3(In1,In2,In3)     MLIB_MacSat_Dsptchr_4(In1,In2,In3,F32)     /*!< Function dispatcher for MLIB_MacSat_Dsptchr_4, do not modify!!! */
  #endif 
  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F16)
    /*
    * @violates @ref MLIB_MacSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_MacSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_MacSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00227 */
    #define MLIB_MacSat_Dsptchr_3(In1,In2,In3)     MLIB_MacSat_Dsptchr_4(In1,In2,In3,F16)     /*!< Function dispatcher for MLIB_MacSat_Dsptchr_4, do not modify!!! */
  #endif 

  /*
  * @violates @ref MLIB_MacSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_MacSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_MacSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
  * parameter in definition of macro. 
  */
  #define MLIB_MacSat_Dsptchr_4(In1,In2,In3,Impl)    MLIB_MacSat_Dsptchr_(In1,In2,In3,Impl)     /*!< Function dispatcher for MLIB_MacSat_Dsptchr_5, do not modify!!! */
  
  /*
  * @violates @ref MLIB_MacSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_MacSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_MacSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
  * parameter in definition of macro. 
  * @violates @ref MLIB_MacSat_h_REF_4 MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
  */
  /** @remarks Implements DMLIB00229 */
  #define MLIB_MacSat_Dsptchr_(In1,In2,In3,Impl)     MLIB_MacSat_##Impl(In1,In2,In3)            /*!< Function dispatcher for MLIB_MacSat_Dsptchr_, do not modify!!! */
#endif
 
/****************************************************************************
* Typedefs and structures       (scope: module-local)
****************************************************************************/

/****************************************************************************
* Exported function prototypes
****************************************************************************/

/****************************************************************************
* Inline functions
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac32 MacSat_F32_C(register tFrac32 f32In1,register tFrac32 f32In2,register tFrac32 f32In3)
/*
* @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
* file.
*/
{
  return(MLIB_AddSat_F32(f32In1, MLIB_MulSat_F32(f32In2, f32In3)));
} 
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac32 MacSat_F32F16F16_C(register tFrac32 f32In1,register tFrac16 f16In2,register tFrac16 f16In3)
/*
* @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
* file.
*/
{
  return(MLIB_AddSat_F32(f32In1, MLIB_MulSat_F32F16F16(f16In2,f16In3)));
}
#ifdef MLIB_SPE1
  #if defined(__ghs__)
    INLINE tFrac32 MacSat_F32_GHS_SPE1(register tFrac32 f32In1,register tFrac32 f32In2,register tFrac32 f32In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      tFrac32 f32Return;
      #pragma ghs optasm
      asm("evmwhssfa %0,%1,%2" : "=r"(f32Return) : "r"(f32In2) "r"(f32In3));
      #pragma ghs optasm
      asm("evaddssiaaw %0,%1" : "=r"(f32Return) : "r"(f32In1));
      return((tFrac32)f32Return);
    }  
  #elif defined(__CWCC__) || defined(__MWERKS__)  
    #pragma always_inline on
    INLINE tFrac32 MacSat_F32_CW_SPE1(register tFrac32 f32In1,register tFrac32 f32In2,register tFrac32 f32In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      tFrac32 f32Return;
      asm("evmwhssfa %0,%1,%2" : "=r"(f32Return) : "r"(f32In2), "r"(f32In3));
      asm("evaddssiaaw %0,%1" : "=r"(f32Return) : "r"(f32In1));
      return((tFrac32)f32Return);
    }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
    /* MLIB_MacSat_F32 implementation variant - SPE1 assembly for S32DS for Power Architecture toolchain */
    INLINE tFrac32 MacSat_F32_S32DSPPC_SPE1(register tFrac32 f32In1,register tFrac32 f32In2,register tFrac32 f32In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      tFrac32 f32Return;
      __asm__ volatile("evmwhssfa %0,%1,%2" : "=r"(f32Return) : "r"(f32In2), "r"(f32In3));
      __asm__ volatile("evaddssiaaw %0,%1" : "=r"(f32Return) : "r"(f32In1));
      return((tFrac32)f32Return);
    }
  #endif
#endif /* ifdef MLIB_SPE1 */

#if (defined(MLIB_SPE1) || defined(MLIB_SPE2))
  #if defined(__ghs__)
    INLINE tFrac32 MacSat_F32F16F16_GHS_SPE12(register tFrac32 f32In1,register tFrac16 f16In2,register tFrac16 f16In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      register tFrac32 f32Return;
      #pragma ghs optasm
      asm("evmra %0,%1" : "=r"(f32In1) : "r"(f32In1));
      #pragma ghs optasm
      asm("evmhossfaaw %0,%1,%2" : "=r"(f32Return) : "r"(f16In2) "r"(f16In3));
      return((tFrac32)f32Return);
    }  
  #elif defined(__CWCC__) || defined(__MWERKS__)
    #pragma always_inline on
    INLINE tFrac32 MacSat_F32F16F16_CW_SPE12(register tFrac32 f32In1,register tFrac16 f16In2,register tFrac16 f16In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      register tFrac32 f32Return;
      asm("evmra %0,%1" : "=r"(f32In1) : "r"(f32In1));
      asm("evmhossfaaw %0,%1,%2" : "=r"(f32Return) : "r"(f16In2), "r"(f16In3));
      return((tFrac32)f32Return);
    }    
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
    /* MLIB_MacSat_F32F16F16 implementation variant - SPE12 assembly for S32DS for Power Architecture toolchain */
    INLINE tFrac32 MacSat_F32F16F16_S32DSPPC_SPE12(register tFrac32 f32In1,register tFrac16 f16In2,register tFrac16 f16In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      register tFrac32 f32Return;
      __asm__ volatile("evmra %0,%1" : "=r"(f32In1) : "r"(f32In1));
      __asm__ volatile("evmhossfaaw %0,%1,%2" : "=r"(f32Return) : "r"(f16In2), "r"(f16In3));
      return((tFrac32)f32Return);
    }
  #endif
#endif /* if (defined(MLIB_SPE1) || defined(MLIB_SPE2)) */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac16 MacSat_F16_C(register tFrac16 f16In1,register tFrac16 f16In2,register tFrac16 f16In3)
/*
* @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
* file.
*/
{
  return(MLIB_ConvertPU_F16F32(MLIB_AddSat_F32(MLIB_ConvertPU_F32F16(f16In1), MLIB_MulSat_F32F16F16(f16In2,f16In3))));
}
#if (defined(MLIB_SPE1) || defined(MLIB_SPE2))
  #if defined(__ghs__)
    INLINE tFrac16 MacSat_F16_GHS_SPE12(register tFrac16 f16In1,register tFrac16 f16In2,register tFrac16 f16In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      tFrac32 f32Return;
      #pragma ghs optasm
      asm("evslwi %0,%1,16" : "=r"(f32Return) : "r"(f16In1));
      #pragma ghs optasm
      asm("evmra %0,%1" : "=r"(f32Return) : "r"(f32Return));
      #pragma ghs optasm
      asm("evmhossfaaw %0,%1,%2" : "=r"(f32Return) : "r"(f16In2) "r"(f16In3));
      #pragma ghs optasm
      asm("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
      return((tFrac16)f32Return);  
    }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  #pragma always_inline on
  INLINE tFrac16 MacSat_F16_CW_SPE12(register tFrac16 f16In1,register tFrac16 f16In2,register tFrac16 f16In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      tFrac32 f32Return;

      asm("evslwi %0,%1,16" : "=r"(f32Return) : "r"(f16In1));
      asm("evmra %0,%1" : "=r"(f32Return) : "r"(f32Return));
      asm("evmhossfaaw %0,%1,%2" : "=r"(f32Return) : "r"(f16In2) , "r"(f16In3));
      asm("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
      return((tFrac16)f32Return);  
    }  
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
    /* MLIB_MacSat_F16 implementation variant - SPE12 assembly for S32DS for Power Architecture toolchain */
    INLINE tFrac16 MacSat_F16_S32DSPPC_SPE12(register tFrac16 f16In1,register tFrac16 f16In2,register tFrac16 f16In3)
  /*
  * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
    {
      /*
      * @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
      * header file. 
      */
      tFrac32 f32Return;

      __asm__ volatile("evslwi %0,%1,16" : "=r"(f32Return) : "r"(f16In1));
      __asm__ volatile("evmra %0,%1" : "=r"(f32Return) : "r"(f32Return));
      __asm__ volatile("evmhossfaaw %0,%1,%2" : "=r"(f32Return) : "r"(f16In2), "r"(f16In3));
      __asm__ volatile("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
      return((tFrac16)f32Return);  
    }
  #endif
#endif /* if (defined(MLIB_SPE1) || defined(MLIB_SPE2)) */


/****************************************************************************
* Implementation variant: 32-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@brief        This function implements the multiply accumulate function saturated if necessary.

@param[in]    f32In1     Input value to be add.

@param[in]    f32In2     First value to be multiplied.

@param[in]    f32In3     Second value to be multiplied.

@return       Multiplied second and third input value with adding of first input value. The output value is saturated
              if necessary.

@details      The input values as well as output value is considered as 32-bit fractional 
              values.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_MacSat_F32
              \image rtf macsatEq1_f32.math "MLIB_MacSat_Eq1"

*/
/*!
@note         Due to effectivity reason this function is implemented as inline assembly and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac32 f32In1;
tFrac32 f32In2;
tFrac32 f32In3;
tFrac32 f32Out;

void main(void)
{
  // input1 value = 0.25
  f32In1  = FRAC32(0.25);

  // input2 value = 0.15
  f32In2  = FRAC32(0.15);

  // input3 value = 0.35
  f32In3  = FRAC32(0.35);

  // output should be FRAC32(0.3025) = 0x26B851EB
  f32Out = MLIB_MacSat_F32(f32In1, f32In2, f32In3);

  // output should be FRAC32(0.3025) = 0x26B851EB
  f32Out = MLIB_MacSat(f32In1, f32In2, f32In3, F32);

  // ##############################################################
  // Available only if 32-bit fractional implementation selected
  // as default
  // ##############################################################

  // output should be FRAC32(0.3025) = 0x26B851EB
  f32Out = MLIB_MacSat(f32In1, f32In2, f32In3);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
/** @remarks Implements DMLIB00228, DMLIB00220, DMLIB00223, DMLIB00226 */
INLINE tFrac32 MLIB_MacSat_F32(register tFrac32 f32In1,register tFrac32 f32In2,register tFrac32 f32In3)
/*
* @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #if defined(__ghs__)
    /** @remarks Implements DMLIB00224, DMLIB00231 */
    return(MacSat_F32_GHS_SPE1(f32In1,f32In2,f32In3));
  #elif defined(__CWCC__) || defined(__MWERKS__)
    /** @remarks Implements DMLIB00224, DMLIB00231 */
    return(MacSat_F32_CW_SPE1(f32In1,f32In2,f32In3));
  #elif defined(__DCC__)
    /** @remarks Implements DMLIB00224, DMLIB00231 */
    return(MacSat_F32_C(f32In1,f32In2,f32In3));
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
    /** @remarks Implements DMLIB00224, DMLIB00231 */
    return(MacSat_F32_S32DSPPC_SPE1(f32In1,f32In2,f32In3));
  #else
    /** @remarks Implements DMLIB00224, DMLIB00231 */
    return(MacSat_F32_C(f32In1,f32In2,f32In3));
  #endif
}





/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@param[in]    f32In1     Input value to be add.

@param[in]    f16In2     First value to be multiplied.

@param[in]    f16In3     Second value to be multiplied.

@return       Multiplied second and third input value with adding of first input value. The output value is saturated
              if necessary.

@details      The first input values as well as output value is considered as 32-bit fractional
              values, second and third input values are considered as 16-bit fractional values.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_MacSat_F32F16F16
              \image rtf macsatEq1_f32f16f16.math "MLIB_MacSat_Eq1"

*/
/*!
@note         Due to effectivity reason this function is implemented as inline assembly and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac32 f32In1;
tFrac16 f16In2;
tFrac16 f16In3;
tFrac32 f32Out;

void main(void)
{
  // input1 value = 0.25
  f32In1 = FRAC32(0.25);

  // input2 value = 0.15
  f16In2 = FRAC16(0.15);

  // input3 value = 0.35
  f16In3 = FRAC16(0.35);

  // output should be FRAC32(0.3025) = 0x26B851EB
  f32Out = MLIB_MacSat_F32F16F16(f32In1, f16In2, f16In3);

  // output should be FRAC32(0.3025) = 0x26B851EB
  f32Out = MLIB_MacSat(f32In1, f16In2, f16In3, F32F16F16);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
/** @remarks Implements DMLIB00228, DMLIB00220, DMLIB00225, DMLIB00226 */
INLINE tFrac32 MLIB_MacSat_F32F16F16(register tFrac32 f32In1,register tFrac16 f16In2,register tFrac16 f16In3)
/*
* @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #if defined(__ghs__)
      /** @remarks Implements DMLIB00224, DMLIB00231 */
      return(MacSat_F32F16F16_GHS_SPE12(f32In1,f16In2,f16In3));
  #elif defined(__CWCC__) || defined(__MWERKS__)
    /** @remarks Implements DMLIB00224, DMLIB00231 */
    return(MacSat_F32F16F16_CW_SPE12(f32In1,f16In2,f16In3));
  #elif defined(__DCC__)
    /** @remarks Implements DMLIB00224, DMLIB00231 */
    return(MacSat_F32F16F16_C(f32In1,f16In2,f16In3));
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00224, DMLIB00231 */
      return(MacSat_F32F16F16_S32DSPPC_SPE12(f32In1,f16In2,f16In3));
  #else
    /** @remarks Implements DMLIB00224, DMLIB00231 */
    return(MacSat_F32F16F16_C(f32In1,f16In2,f16In3));
  #endif
}





/****************************************************************************
* Implementation variant: 16-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@param[in]    f16In1     Input value to be add.

@param[in]    f16In2     First value to be multiplied.

@param[in]    f16In3     Second value to be multiplied.

@return       Multiplied second and third input value with adding of first input value. The output value is saturated
              if necessary.

@details      The input values as well as output value is considered as 16-bit fractional 
              values.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_MacSat_F16
              \image rtf macsatEq1_f16.math "MLIB_MacSat_Eq1"

*/
/*!
@note         Due to effectivity reason this function is implemented as inline assembly and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac16 f16In1;
tFrac16 f16In2;
tFrac16 f16In3;
tFrac16 f16Out;

void main(void)
{
  // input1 value = 0.25
  f16In1 = FRAC16(0.25);

  // input2 value = 0.15
  f16In2 = FRAC16(0.15);

  // input3 value = 0.35
  f16In3 = FRAC16(0.35);

  // output should be FRAC16(0.3025) = 0x26B8
  f16Out = MLIB_MacSat_F16(f16In1, f16In2, f16In3);

  // output should be FRAC16(0.3025) = 0x26B8
  f16Out = MLIB_MacSat(f16In1, f16In2, f16In3, F16);

  // ##############################################################
  // Available only if 16-bit fractional implementation selected
  // as default
  // ##############################################################

  // output should be FRAC16(0.3025) = 0x26B8
  f16Out = MLIB_MacSat(f16In1, f16In2, f16In3);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
/** @remarks Implements DMLIB00228, DMLIB00222, DMLIB00220, DMLIB00226 */
INLINE tFrac16 MLIB_MacSat_F16(register tFrac16 f16In1,register tFrac16 f16In2,register tFrac16 f16In3)
/*
* @violates @ref MLIB_MacSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #if defined(__ghs__)
    /** @remarks Implements DMLIB00224, DMLIB00232 */
    return(MacSat_F16_GHS_SPE12(f16In1,f16In2,f16In3));
  #elif defined(__CWCC__) || defined(__MWERKS__)
    /** @remarks Implements DMLIB00224, DMLIB00232 */
    return(MacSat_F16_CW_SPE12(f16In1,f16In2,f16In3));
  #elif defined(__DCC__)
    /** @remarks Implements DMLIB00224, DMLIB00232 */
    return(MacSat_F16_C(f16In1,f16In2,f16In3));
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00224, DMLIB00232 */
      return(MacSat_F16_S32DSPPC_SPE12(f16In1,f16In2,f16In3));
  #else
    /** @remarks Implements DMLIB00224, DMLIB00232 */
    return(MacSat_F16_C(f16In1,f16In2,f16In3));
  #endif
}


#ifdef __cplusplus
}
#endif /* ifdef __cplusplus */

#endif /* ifndef MLIB_MACSAT_H */
