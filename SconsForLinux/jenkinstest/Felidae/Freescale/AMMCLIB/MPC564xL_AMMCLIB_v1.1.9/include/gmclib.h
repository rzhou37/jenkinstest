/******************************************************************************
*
*   (c) Copyright 2017 NXP Semiconductors
*   All Rights Reserved.
*
******************************************************************************/
/*!
*
* @file     gmclib.h
*
* @version  1.0.8.0
* 
* @date     Jan-31-2017
* 
* @brief    Master header file.
*
******************************************************************************/
#ifndef GMCLIB_H
#define GMCLIB_H

/******************************************************************************
* Includes
******************************************************************************/
#include "gflib.h"
#include "GMCLIB_Clark.h"
#include "GMCLIB_ClarkInv.h"
#include "GMCLIB_Park.h"
#include "GMCLIB_ParkInv.h"
#include "GMCLIB_SvmStd.h"
#include "GMCLIB_ElimDcBusRip.h"
#include "GMCLIB_DecouplingPMSM.h"

#endif /* GMCLIB_H */
