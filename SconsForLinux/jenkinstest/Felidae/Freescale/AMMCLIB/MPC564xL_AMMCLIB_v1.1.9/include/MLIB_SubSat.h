/******************************************************************************
*
*   (c) Copyright 2017 NXP Semiconductors
*
******************************************************************************/
/*!
*
* @file       MLIB_SubSat.h
*
* @version    1.0.76.0
*
* @date       Mar-15-2017
*
* @brief      Header file for MLIB_SubSat function
*
******************************************************************************/
#ifndef MLIB_SUBSAT_H
#define MLIB_SUBSAT_H
/*!
@if MLIB_GROUP
    @addtogroup MLIB_GROUP
@else
    @defgroup MLIB_GROUP   MLIB
@endif
*/

#ifdef __cplusplus
extern "C" {
#endif

/**
* @page misra_violations MISRA-C:2004 violations
* 
* @section MLIB_SubSat_h_REF_1
* Violates MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of disallowed macro 
* definition. 
* 
* @section MLIB_SubSat_h_REF_2
* Violates MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires the function-like macro 
* definition. 
* 
* @section MLIB_SubSat_h_REF_3
* Violates MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter in definition of macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of unparenthesized 
* macro parameters. 
* 
* @section MLIB_SubSat_h_REF_4
* Violates MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of '#/##' operators. 
* 
* @section MLIB_SubSat_h_REF_5
* Violates MISRA 2004 Required Rule 8.5, Object/function definition in header file. 
* Allowing the inline functions significantly increase the speed of the library thus the lowest 
* layer (MLIB) is implemented as inline. This approach removes the overhead caused by standard 
* function calling. 
* 
* @section MLIB_SubSat_h_REF_6
* Violates MISRA 2004 Required Rule 12.7, Bitwise operator applied to signed underlying type. 
* The fractional arithmetic requires the bit-wise operations on signed values. 
*
* @section MLIB_SubSat_h_REF_7
* Violates MISRA 2004 Required Rule 10.1, Prohibited Implicit Conversion.
* This violation originates within a header provided by the compiler.
*/
#include "SWLIBS_Defines.h"
#ifndef AMMCLIB_TESTING_ENV
  #include "MLIB_Sub.h"
#else
  /* Following include serves for NXP internal testing purposes only. 
  *  This header is not part of the release. */
  #include "CCOV_MLIB_Sub.h"
#endif

/****************************************************************************
* Defines and macros            (scope: module-local)
****************************************************************************/
#ifndef  _MATLAB_BAM_CREATE
  /*
  * @violates @ref MLIB_SubSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_SubSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_SubSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
  * parameter in definition of macro. 
  */
  #define MLIB_SubSat(...)     macro_dispatcher(MLIB_SubSat, __VA_ARGS__)(__VA_ARGS__)     /*!< This function subtracts the second parameter from the first one and saturate if necessary. */

  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F32)
    /*
    * @violates @ref MLIB_SubSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_SubSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_SubSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00078 */    
    #define MLIB_SubSat_Dsptchr_2(In1,In2)     MLIB_SubSat_Dsptchr_3(In1,In2,F32)     /*!< Function dispatcher for MLIB_SubSat_Dsptchr_2, do not modify!!! */
  #endif
  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F16)
    /*
    * @violates @ref MLIB_SubSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_SubSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_SubSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00078 */
    #define MLIB_SubSat_Dsptchr_2(In1,In2)     MLIB_SubSat_Dsptchr_3(In1,In2,F16)     /*!< Function dispatcher for MLIB_SubSat_Dsptchr_2, do not modify!!! */
  #endif

  /*
  * @violates @ref MLIB_SubSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_SubSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_SubSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
  * parameter in definition of macro. 
  */
  #define MLIB_SubSat_Dsptchr_3(In1,In2,Impl)    MLIB_SubSat_Dsptchr_(In1,In2,Impl)     /*!< Function dispatcher for MLIB_SubSat_Dsptchr_3, do not modify!!! */
  
  /*
  * @violates @ref MLIB_SubSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_SubSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_SubSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
  * parameter in definition of macro. 
  * @violates @ref MLIB_SubSat_h_REF_4 MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
  */
  /** @remarks Implements DMLIB00080 */
  #define MLIB_SubSat_Dsptchr_(In1,In2,Impl)     MLIB_SubSat_##Impl(In1,In2)            /*!< Function dispatcher for MLIB_SubSat_Dsptchr_, do not modify!!! */
#endif /* _MATLAB_BAM_CREATE */

/****************************************************************************
* Typedefs and structures       (scope: module-local)
****************************************************************************/

/****************************************************************************
* Exported function prototypes
****************************************************************************/

/****************************************************************************
* Inline functions
****************************************************************************/
/* MLIB_SubSat_F32 implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac32 SubSat_F32_C(register tFrac32 f32In1,register tFrac32 f32In2)
/*
* @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file.
*/
{
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
  * header file.
  */
  register tS32 s32Temp;
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
  * header file.
  */
  register tS32 s32satmin, s32satmax;

  s32Temp = F32TOINT32(MLIB_Sub_F32(f32In1, f32In2));

  /*
  * @violates @ref MLIB_SubSat_h_REF_6 MISRA 2004 Required Rule 12.7, Bitwise operator applied to
  * signed underlying type (&).
  */
  s32satmax = (~F32TOINT32(f32In1) & F32TOINT32(f32In2)) & s32Temp;
  /*
  * @violates @ref MLIB_SubSat_h_REF_6 MISRA 2004 Required Rule 12.7, Bitwise operator applied to
  * signed underlying type (~).
  */
  s32satmin = F32TOINT32(f32In1) & (~F32TOINT32(f32In2)) & (~s32Temp);

  s32Temp = (s32satmin < 0) ? INT32_MIN : s32Temp;
  s32Temp = (s32satmax < 0) ? INT32_MAX : s32Temp;
  return(INT32TOF32(s32Temp));
}
#ifdef MLIB_SPE1
  #if defined(__ghs__)
  INLINE tFrac32 SubSat_F32_GHS_SPE1(register tFrac32 f32In1,register tFrac32 f32In2)
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Return;
    #pragma ghs optasm
    asm("evmra %0,%1" : "=r"(f32In1) : "r"(f32In1));
    #pragma ghs optasm
    asm("evsubfssiaaw %0,%1" : "=r"(f32Return) : "r"(f32In2));
    return((tFrac32)f32Return);
  }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  /* MLIB_SubSat_F32 implementation variant - SPE1 assembly for CW toolchain */
  #pragma always_inline on
  INLINE tFrac32 SubSat_F32_CW_SPE1(register tFrac32 f32In1,register tFrac32 f32In2)
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Return;
    asm("evmra %0,%1" : "=r"(f32In1) : "r"(f32In1));
    asm("evsubfssiaaw %0,%1" : "=r"(f32Return) : "r"(f32In2));
    return((tFrac32)f32Return);
  }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
  /* MLIB_SubSat_F32 implementation variant - SPE1 assembly for S32DS for Power Architecture toolchain */
  INLINE tFrac32 SubSat_F32_S32DSPPC_SPE1(register tFrac32 f32In1,register tFrac32 f32In2)
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Return;
    __asm__ volatile("evmra %0,%1" : "=r"(f32In1) : "r"(f32In1));
    __asm__ volatile("evsubfssiaaw %0,%1" : "=r"(f32Return) : "r"(f32In2));
    return((tFrac32)f32Return);
  }
  #endif
#endif /* ifdef MLIB_SPE1 */
/* MLIB_SubSat_F16 implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac16 SubSat_F16_C(register tFrac16 f16In1,register tFrac16 f16In2)
/*
* @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file.
*/
{
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
  * header file.
  */
  register tS32 s32temp;

  s32temp = F32TOINT32(MLIB_Sub_F32((tFrac32)f16In1, (tFrac32)f16In2));

  s32temp = (s32temp > (tS32) INT16_MAX) ? (tS32) INT16_MAX : s32temp;
  s32temp = (s32temp < (tS32) INT16_MIN) ? (tS32) INT16_MIN : s32temp;
  return(INT32TOF16(s32temp));
}
#ifdef MLIB_SPE1
  #if defined(__ghs__)
  INLINE tFrac16 SubSat_F16_GHS_SPE1(register tFrac16 f16In1,register tFrac16 f16In2)
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tFrac32 f32Return, f32Temp1, f32Temp2;
    #pragma ghs optasm
    asm("evslwi %0,%1,16" : "=r"(f32Temp2) : "r"(f16In2));
    #pragma ghs optasm
    asm("evslwi %0,%1,16" : "=r"(f32Temp1) : "r"(f16In1));
    #pragma ghs optasm
    asm("evmra %0,%1" : "=r"(f32Temp1) : "r"(f32Temp1));
    #pragma ghs optasm
    asm("evsubfssiaaw %0,%1" : "=r"(f32Return) : "r"(f32Temp2));
    #pragma ghs optasm
    asm("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
    return((tFrac16)f32Return);
  }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  /* MLIB_SubSat_F16 implementation variant - SPE1 assembly for CW toolchain */
  #pragma always_inline on
  INLINE tFrac16 SubSat_F16_CW_SPE1(register tFrac16 f16In1,register tFrac16 f16In2)
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tFrac32 f32Return, f32Temp1, f32Temp2;
    asm("evslwi %0,%1,16" : "=r"(f32Temp2) : "r"(f16In2));
    asm("evslwi %0,%1,16" : "=r"(f32Temp1) : "r"(f16In1));
    asm("evmra %0,%1" : "=r"(f32Temp1) : "r"(f32Temp1));
    asm("evsubfssiaaw %0,%1" : "=r"(f32Return) : "r"(f32Temp2));
    asm("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
    return((tFrac16)f32Return);
  }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
  /* MLIB_SubSat_F16 implementation variant - SPE1 assembly for S32DS for Power Architecture toolchain */
  INLINE tFrac16 SubSat_F16_S32DSPPC_SPE1(register tFrac16 f16In1,register tFrac16 f16In2)
  /*
  * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tFrac32 f32Return, f32Temp1, f32Temp2;
    __asm__ volatile("evslwi %0,%1,16" : "=r"(f32Temp2) : "r"(f16In2));
    __asm__ volatile("evslwi %0,%1,16" : "=r"(f32Temp1) : "r"(f16In1));
    __asm__ volatile("evmra %0,%1" : "=r"(f32Temp1) : "r"(f32Temp1));
    __asm__ volatile("evsubfssiaaw %0,%1" : "=r"(f32Return) : "r"(f32Temp2));
    __asm__ volatile("evsrwis %0,%1,16" : "=r"(f32Return) : "r"(f32Return));
    return((tFrac16)f32Return);
  }
  #endif
#endif /* ifdef MLIB_SPE1 */





/****************************************************************************
* Implementation variant: 32-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@brief        This function subtracts the second parameter from the first one and
              saturate if necessary.

@param[in]    f32In1     Operand is a 32-bit number normalized between [-1,1).

@param[in]    f32In2     Operand is a 32-bit number normalized between [-1,1).

@return       The subtraction of the second argument from the first argument.

@details      The input values as well as output value are considered as 32-bit fractional data
              type.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_SubSat_F32
              \image rtf subsatEq1_f32.math "MLIB_SubSat_Eq1"

*/
/*!
@note         Due to effectivity reason this function is implemented as inline assembly and  thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac32 f32In1;
tFrac32 f32In2;
tFrac32 f32Out;

void main(void)
{
  // first input = 0.5
  f32In1 = FRAC32(0.5);

  // second input = 0.25
  f32In2 = FRAC32(0.25);

  // output should be 0x20000000
  f32Out = MLIB_SubSat_F32(f32In1,f32In2);

  // output should be 0x20000000
  f32Out = MLIB_SubSat(f32In1,f32In2,F32);

  // ##############################################################
  // Available only if 32-bit fractional implementation selected
  // as default
  // ##############################################################

  // output should be 0x20000000
  f32Out = MLIB_SubSat(f32In1,f32In2);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
/** @remarks Implements DMLIB00079, DMLIB00073, DMLIB00075, DMLIB00077, DMLIB00082 */
INLINE tFrac32 MLIB_SubSat_F32(register tFrac32 f32In1, register tFrac32 f32In2)
/*
* @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #if (defined(MLIB_SPE1) || defined(MLIB_SPE2))
    #if defined(__ghs__)
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F32_GHS_SPE1(f32In1,f32In2));
    #elif defined(__DCC__)
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F32_C(f32In1,f32In2));
    #elif defined(__CWCC__) || defined(__MWERKS__)
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F32_CW_SPE1(f32In1,f32In2));
    #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F32_S32DSPPC_SPE1(f32In1,f32In2));
    #else
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F32_C(f32In1,f32In2));
    #endif
  #else
    /** @remarks Implements DMLIB00076 */
    return(SubSat_F32_C(f32In1,f32In2));
  #endif
}





/****************************************************************************
* Implementation variant: 16-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@param[in]    f16In1     Operand is a 16-bit number normalized between [-1,1).

@param[in]    f16In2     Operand is a 16-bit number normalized between [-1,1).

@return       The subtraction of the second argument from the first argument.

@details      The input values as well as output value are considered as 16-bit fractional data
              type.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_SubSat_F16
              \image rtf subsatEq1_f16.math "MLIB_SubSat_Eq1"

*/
/*!
@note         Due to effectivity reason this function is implemented as inline assembly and  thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac16 f16In1;
tFrac16 f16In2;
tFrac16 f16Out;

void main(void)
{
  // first input = 0.5
  f16In1 = FRAC16(0.5);

  // second input = 0.25
  f16In2 = FRAC16(0.25);

  // output should be 0x2000
  f16Out = MLIB_SubSat_F16(f16In1,f16In2);

  // output should be 0x2000
  f16Out = MLIB_SubSat(f16In1,f16In2,F16);

  // ##############################################################
  // Available only if 16-bit fractional implementation selected
  // as default
  // ##############################################################

  // output should be 0x2000
  f16Out = MLIB_SubSat(f16In1,f16In2);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
/** @remarks Implements DMLIB00079, DMLIB00073, DMLIB00074, DMLIB00077, DMLIB00083 */
INLINE tFrac16 MLIB_SubSat_F16(register tFrac16 f16In1,register tFrac16 f16In2)
/*
* @violates @ref MLIB_SubSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #if (defined(MLIB_SPE1) || defined(MLIB_SPE2))
    #if defined(__ghs__)
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F16_GHS_SPE1(f16In1,f16In2));
    #elif defined(__DCC__)
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F16_C(f16In1,f16In2));
    #elif defined(__CWCC__) || defined(__MWERKS__)
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F16_CW_SPE1(f16In1,f16In2));
    #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F16_S32DSPPC_SPE1(f16In1,f16In2));
    #else
      /** @remarks Implements DMLIB00076 */
      return(SubSat_F16_C(f16In1,f16In2));
    #endif
  #else
    /** @remarks Implements DMLIB00076 */
    return(SubSat_F16_C(f16In1,f16In2));
  #endif
}

#ifdef __cplusplus
}
#endif

#endif /* MLIB_SUBSAT_H */
