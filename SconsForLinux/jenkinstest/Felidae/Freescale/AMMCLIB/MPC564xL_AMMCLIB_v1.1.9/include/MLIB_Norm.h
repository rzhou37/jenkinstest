/******************************************************************************
*
*   (c) Copyright 2017 NXP Semiconductors
*
******************************************************************************/
/*!
*
* @file       MLIB_Norm.h
*
* @version    1.0.78.0
*
* @date       Mar-8-2017
*
* @brief      Header file for MLIB_Norm function
*
******************************************************************************/
#ifndef MLIB_NORM_H
#define MLIB_NORM_H
/*!
@if MLIB_GROUP
    @addtogroup MLIB_GROUP
@else
    @defgroup MLIB_GROUP   MLIB
@endif
*/

#ifdef __cplusplus
extern "C" {
#endif

/**
* @page misra_violations MISRA-C:2004 violations
* 
* @section MLIB_Norm_h_REF_1
* Violates MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of disallowed macro 
* definition. 
* 
* @section MLIB_Norm_h_REF_2
* Violates MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires the function-like macro 
* definition. 
* 
* @section MLIB_Norm_h_REF_3
* Violates MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter in definition of macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of unparenthesized 
* macro parameters. 
* 
* @section MLIB_Norm_h_REF_4
* Violates MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of '#/##' operators. 
* 
* @section MLIB_Norm_h_REF_5
* Violates MISRA 2004 Required Rule 8.5, Object/function definition in header file. 
* Allowing the inline functions significantly increase the speed of the library thus the lowest 
* layer (MLIB) is implemented as inline. This approach removes the overhead caused by standard 
* function calling. 
* 
* @section MLIB_Norm_h_REF_6
* Violates MISRA 2004 Required Rule 14.7, Return statement before end of function. 
* There is no required code which needs to be executed after the calculation in the branch, thus the 
* performance of the function will be negatively influenced in case the single return point is used. 
* 
* @section MLIB_Norm_h_REF_7
* Violates MISRA 2004 Required Rule 12.7, Bitwise operator applied to signed underlying type. 
* The fractional arithmetic requires the bit-wise operations on signed values. 
*
* @section MLIB_Norm_h_REF_8
* Violates MISRA 2004 Required Rule 10.1, Prohibited Implicit Conversion.
* This violation originates within a header provided by the compiler.
*/
#include "SWLIBS_Defines.h"
#ifndef AMMCLIB_TESTING_ENV
  #include "MLIB_ShR.h"
  #include "MLIB_ShL.h"
#else
  /* Following includes serve for NXP internal testing purposes only. 
  *  These headers are not part of the release. */
  #include "CCOV_MLIB_ShR.h"
  #include "CCOV_MLIB_ShL.h"
#endif

/****************************************************************************
* Defines and macros            (scope: module-local)
****************************************************************************/
#ifndef  _MATLAB_BAM_CREATE
  /*
  * @violates @ref MLIB_Norm_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
  * @violates @ref MLIB_Norm_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_Norm_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter 
  * in definition of macro. 
  */
  #define MLIB_Norm(...)     macro_dispatcher(MLIB_Norm, __VA_ARGS__)(__VA_ARGS__)     /*!< This function returns the number of left shifts needed to normalize the input parameter. */

  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F32)
    /*
    * @violates @ref MLIB_Norm_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_Norm_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_Norm_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00195 */
    #define MLIB_Norm_Dsptchr_1(In)     MLIB_Norm_Dsptchr_2(In,F32)     /*!< Function dispatcher for MLIB_Norm_Dsptchr_1, do not modify!!! */
  #endif 
  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F16)
    /*
    * @violates @ref MLIB_Norm_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_Norm_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_Norm_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00195 */
    #define MLIB_Norm_Dsptchr_1(In)     MLIB_Norm_Dsptchr_2(In,F16)     /*!< Function dispatcher for MLIB_Norm_Dsptchr_1, do not modify!!! */
  #endif 

  /*
  * @violates @ref MLIB_Norm_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
  * @violates @ref MLIB_Norm_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_Norm_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter 
  * in definition of macro. 
  */
  #define MLIB_Norm_Dsptchr_2(In,Impl)    MLIB_Norm_Dsptchr_(In,Impl)   /*!< Function dispatcher for MLIB_Norm_Dsptchr_2, do not modify!!! */
  
  /*
  * @violates @ref MLIB_Norm_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
  * @violates @ref MLIB_Norm_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_Norm_h_REF_4 MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
  */
  /** @remarks Implements DMLIB00197 */
  #define MLIB_Norm_Dsptchr_(In,Impl)     MLIB_Norm_##Impl(In)          /*!< Function dispatcher for MLIB_Norm_Dsptchr_, do not modify!!! */
#endif /* _MATLAB_BAM_CREATE */
 
/****************************************************************************
* Typedefs and structures       (scope: module-local)
****************************************************************************/

/****************************************************************************
* Exported function prototypes
****************************************************************************/

/****************************************************************************
* Inline functions
****************************************************************************/
/* MLIB_Norm_F32 implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tU16 Norm_F32_C(register tFrac32 f32In)
/*
* @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
* header file.
*/
{
  /*
  * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
  * header file.
  */
  register tU16 u16ShiftCount = (tU16)0;

  if(f32In == (tFrac32)0){
    /*
    * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
    * function.
    */
    return((tU16)0);
  }
  if(f32In > (tFrac32)0){
    if (f32In < (tFrac32)0x00FF0000u){
      do{
        f32In = MLIB_ShR_F32(f32In,(tU16)1);
        u16ShiftCount += (tU16)1;
      } while(f32In > (tFrac32)0);
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)31-u16ShiftCount);
    }else{
      /*
      * @violates @ref MLIB_Norm_h_REF_7 MISRA 2004 Required Rule 12.7, Bitwise operator applied to 
      * signed underlying type. 
      */
      while(((f32In & 0x40000000)==0)){
        f32In = MLIB_ShL_F32(f32In,(tU16)1);
        u16ShiftCount += (tU16)1;
      }
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)u16ShiftCount);
    }  
  }else{
    if (f32In > (tFrac32)0xFF000000u){
      if(f32In == (tFrac32)-1){
        /*
        * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
        * function.
        */
        return((tU16)31);    
      }
      do{
        f32In = MLIB_ShR_F32(f32In,(tU16)1);
        u16ShiftCount += (tU16)1;
      } while(f32In != (tFrac32)-1);
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)31-u16ShiftCount);
    }else{
      /*
      * @violates @ref MLIB_Norm_h_REF_7 MISRA 2004 Required Rule 12.7, Bitwise operator applied to 
      * signed underlying type. 
      */
      while((f32In | (tFrac32)0xBFFFFFFFu) != (tFrac32)0xBFFFFFFFu){
        f32In = MLIB_ShL_F32(f32In,(tU16)1);
        u16ShiftCount += (tU16)1;
      }
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)u16ShiftCount);
    }  
  }      
}
#if (defined(MLIB_SPE1) || defined(MLIB_SPE2))
  #if defined(__ghs__)
  /* MLIB_Norm_F32 implementation variant - SPE1/2 assembly for GHS toolchain */
  INLINE tU16 Norm_F32_GHS_SPE12(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
  * header file.
  */
  {
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tU16 u16Return;

    if(f32In == (tFrac32)0){
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)0);
    }
    #pragma ghs optasm
    asm("evcntlsw %0,%1" : "=r"(u16Return) : "r"(f32In));
    u16Return = u16Return-(tU16)1;
    return((tU16)u16Return);
  }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  /* MLIB_Norm_F32 implementation variant - SPE1/2 assembly for CW toolchain */
  #pragma always_inline on
  INLINE tU16 Norm_F32_CW_SPE12(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tU16 u16Return;

    if(f32In == (tFrac32)0){
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)0);
    }
    asm("evcntlsw %0,%1" : "=r"(u16Return) : "r"(f32In));
    u16Return = u16Return-(tU16)1;
    return((tU16)u16Return);
  }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
  /* MLIB_Norm_F32 implementation variant - SPE1/2 assembly for S32DS for Power Architecture toolchain */
  INLINE tU16 Norm_F32_S32DSPPC_SPE12(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tU16 u16Return;

    if(f32In == (tFrac32)0){
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)0);
    }
    __asm__ volatile("evcntlsw %0,%1" : "=r"(u16Return) : "r"(f32In));
    u16Return = u16Return-(tU16)1;
    return((tU16)u16Return);
  }
  #endif
#endif /* if (defined(MLIB_SPE1) || defined(MLIB_SPE2)) */
/* MLIB_Norm_F16 implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tU16 Norm_F16_C(register tFrac16 f16In)
/*
* @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
* header file.
*/
{
  /*
  * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
  * header file.
  */
  register tU16 u16Return = (tU16)0;

  if(f16In == (tFrac16)0){
    /*
    * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
    * function.
    */
    return((tU16)0);
  }
  if(f16In > (tFrac16)0){
    if (f16In < (tFrac16)0x00FF){
      do{
        f16In = MLIB_ShR_F16(f16In,(tU16)1);
        u16Return += (tU16)1;
      } while(f16In > (tFrac16)0);
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)15-u16Return);
    }else{
      /*
      * @violates @ref MLIB_Norm_h_REF_7 MISRA 2004 Required Rule 12.7, Bitwise operator applied to 
      * signed underlying type. 
      */
      while(((f16In & 0x4000)==0)){
        f16In = MLIB_ShL_F16(f16In,(tU16)1);
        u16Return += (tU16)1;
      }
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)u16Return);
    }  
  }else{
    if (f16In > (tFrac16)0xFF00){
      if(f16In == (tFrac16)0xFFFF){
        /*
        * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
        * function.
        */
        return((tU16)15);    
      }  
      do{
        f16In = MLIB_ShR_F16(f16In,(tU16)1);
        u16Return += (tU16)1;
      } while(f16In != (tFrac16)0xFFFF);
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)15-u16Return);
    }else{
      /*
      * @violates @ref MLIB_Norm_h_REF_7 MISRA 2004 Required Rule 12.7, Bitwise operator applied to 
      * signed underlying type. 
      */
      while((f16In | (tFrac16)0xBFFF) != (tFrac16)0xBFFF){
        f16In = MLIB_ShL_F16(f16In,(tU16)1);
        u16Return += (tU16)1;
      }
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)u16Return);
    }  
  }
}
#ifdef MLIB_SPE1
  #if defined(__ghs__)
  /* MLIB_Norm_F16 implementation variant - SPE1 assembly for GHS toolchain */
  INLINE tU16 Norm_F16_GHS_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
  * header file.
  */
  {
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tFrac32 f32InTmp;
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tU16 u16Return;

    if(f16In == (tFrac16)0){
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)0);
    }
    #pragma ghs optasm
    asm("evslwi %0,%1,16" : "=r"(f32InTmp) : "r"(f16In));
    #pragma ghs optasm
    asm("evcntlsw %0,%1" : "=r"(u16Return) : "r"(f32InTmp));
    u16Return = u16Return-(tU16)1;
    return((tU16)u16Return);
  }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  /* MLIB_Norm_F16 implementation variant - SPE1 assembly for CW toolchain */
  #pragma always_inline on
  INLINE tU16 Norm_F16_CW_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tFrac32 f32InTmp;
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tU16 u16Return;

    if(f16In == (tFrac16)0){
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)0);
    }
    asm("evslwi %0,%1,16" : "=r"(f32InTmp) : "r"(f16In));
    asm("evcntlsw %0,%1" : "=r"(u16Return) : "r"(f32InTmp));
    u16Return = u16Return-(tU16)1;
    return((tU16)u16Return);
  }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
  /* MLIB_Norm_F16 implementation variant - SPE1 assembly for S32DS for Power Architecture toolchain */
  INLINE tU16 Norm_F16_S32DSPPC_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tFrac32 f32InTmp;
    /*
    * @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in
    * header file.
    */
    register tU16 u16Return;

    if(f16In == (tFrac16)0){
      /*
      * @violates @ref MLIB_Norm_h_REF_6 MISRA 2004 Required Rule 14.7, Return statement before end of
      * function.
      */
      return((tU16)0);
    }
    __asm__ volatile("evslwi %0,%1,16" : "=r"(f32InTmp) : "r"(f16In));
    __asm__ volatile("evcntlsw %0,%1" : "=r"(u16Return) : "r"(f32InTmp));
    u16Return = u16Return-(tU16)1;
    return((tU16)u16Return);
  }
  #endif
#endif /* ifdef MLIB_SPE1 */





/****************************************************************************
* Implementation variant: 32-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@brief        This function returns the number of left shifts needed to normalize
              the input parameter.

@param[in]         f32In     The first value to be normalized.

@return       The number of left shift needed to normalize the argument.

*/
/*!
@note         Due to effectivity reason this function is implemented as inline assembly and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac32 f32In;
tU16 u16Out;

void main(void)
{
    // first input = 0.00005
    f32In = FRAC32(0.00005);

    // output should be 14
    u16Out = MLIB_Norm_F32(f32In);

    // output should be 14
    u16Out = MLIB_Norm(f32In,F32);

    // ##############################################################
    // Available only if 32-bit fractional implementation selected
    // as default
    // ##############################################################

    // output should be 14
    u16Out = MLIB_Norm(f32In);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
/** @remarks Implements DMLIB00196, DMLIB00189, DMLIB00192, DMLIB00194 */
INLINE tU16 MLIB_Norm_F32(register tFrac32 f32In)
/*
* @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #if (defined(MLIB_SPE1) || defined(MLIB_SPE2))
    #if defined(__ghs__)
      /** @remarks Implements DMLIB00193 */
      return(Norm_F32_GHS_SPE12(f32In));
    #elif defined(__DCC__)
      /** @remarks Implements DMLIB00193 */
      return(Norm_F32_C(f32In));
    #elif defined(__CWCC__) || defined(__MWERKS__)
      /** @remarks Implements DMLIB00193 */
      return(Norm_F32_CW_SPE12(f32In));
    #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00193 */
      return(Norm_F32_S32DSPPC_SPE12(f32In));
    #else
      /** @remarks Implements DMLIB00193 */
      return(Norm_F32_C(f32In));
    #endif
  #else
    /** @remarks Implements DMLIB00193 */
    return(Norm_F32_C(f32In));
  #endif
}





/****************************************************************************
* Implementation variant: 16-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@param[in]         f16In     The first value to be normalized.

@return       The number of left shift needed to normalize the argument.

*/
/*!
@note         Due to effectivity reason this function is implemented as inline assembly and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac16 f16In;
tU16 u16Out;

void main(void)
{
    // first input = 0.00005
    f16In = FRAC16(0.00005);

    // output should be 14
    u16Out = MLIB_Norm_F16(f16In);

    // output should be 14
    u16Out = MLIB_Norm(f16In,F16);

    // ##############################################################
    // Available only if 16-bit fractional implementation selected
    // as default
    // ##############################################################

    // output should be 14
    u16Out = MLIB_Norm(f16In);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
/** @remarks Implements DMLIB00196, DMLIB00189, DMLIB00191, DMLIB00194 */
INLINE tU16 MLIB_Norm_F16(register tFrac16 f16In)
/*
* @violates @ref MLIB_Norm_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #if (defined(MLIB_SPE1) || defined(MLIB_SPE2))
    #if defined(__ghs__)
      /** @remarks Implements DMLIB00193 */
      return(Norm_F16_GHS_SPE1(f16In));
    #elif defined(__DCC__)
      /** @remarks Implements DMLIB00193 */
      return(Norm_F16_C(f16In));
    #elif defined(__CWCC__) || defined(__MWERKS__)
      /** @remarks Implements DMLIB00193 */
      return(Norm_F16_CW_SPE1(f16In));
    #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00193 */
      return(Norm_F16_S32DSPPC_SPE1(f16In));
    #else
      /** @remarks Implements DMLIB00193 */
      return(Norm_F16_C(f16In));
    #endif
  #else
    /** @remarks Implements DMLIB00193 */
    return(Norm_F16_C(f16In));
  #endif
}

#ifdef __cplusplus
}
#endif

#endif /* MLIB_NORM_H */
