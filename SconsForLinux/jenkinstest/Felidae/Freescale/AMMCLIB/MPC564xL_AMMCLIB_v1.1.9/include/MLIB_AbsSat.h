/******************************************************************************
*
*   (c) Copyright 2017 NXP Semiconductors
*   All Rights Reserved.
*
******************************************************************************/
/*!
*
* @file       MLIB_AbsSat.h
*
* @version    1.0.70.0
*
* @date       Mar-8-2017
*
* @brief      Header file for MLIB_AbsSat function
*
******************************************************************************/
#ifndef MLIB_ABSSAT_H
#define MLIB_ABSSAT_H
/*!
@if MLIB_GROUP
    @addtogroup MLIB_GROUP
@else
    @defgroup MLIB_GROUP   MLIB
@endif
*/

#ifdef __cplusplus
extern "C" {
#endif

/**
* @page misra_violations MISRA-C:2004 violations
* 
* @section MLIB_AbsSat_h_REF_1
* Violates MISRA 2004 Required Rule 19.4, Disallowed definition for macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of disallowed macro 
* definition. 
* 
* @section MLIB_AbsSat_h_REF_2
* Violates MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires the function-like macro 
* definition. 
* 
* @section MLIB_AbsSat_h_REF_3
* Violates MISRA 2004 Required Rule 19.10, Unparenthesized macro parameter in definition of macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of unparenthesized 
* macro parameters. 
* 
* @section MLIB_AbsSat_h_REF_4
* Violates MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
* To allow the user utilize the benefits of using all three supported implementation of each 
* function in user application, the macro dispatcher inevitably requires use of '#/##' operators. 
* 
* @section MLIB_AbsSat_h_REF_5
* Violates MISRA 2004 Required Rule 8.5, Object/function definition in header file. 
* Allowing the inline functions significantly increase the speed of the library thus the lowest 
* layer (MLIB) is implemented as inline. This approach removes the overhead caused by standard 
* function calling. 
* 
* @section MLIB_AbsSat_h_REF_6
* Violates MISRA 2004 Required Rule 12.7, Bitwise operator applied to signed underlying type. 
* The fractional arithmetic requires the bit-wise operations on signed values. 
*
* @section MLIB_AbsSat_h_REF_7
* Violates MISRA 2004 Required Rule 10.1, Prohibited Implicit Conversion.
* This violation originates within a header provided by the compiler.
*/

#include "SWLIBS_Defines.h"
#ifndef AMMCLIB_TESTING_ENV
  #include "MLIB_Abs.h"
  #include "MLIB_ShL.h"
  #include "MLIB_ShR.h"
#else
  /* Following include serves for NXP internal testing purposes only. 
  *  This header is not part of the release. */
  #include "CCOV_MLIB_Abs.h"
  #include "CCOV_MLIB_ShL.h"
  #include "CCOV_MLIB_ShR.h"
#endif

/****************************************************************************
* Defines and macros            (scope: module-local)
****************************************************************************/
#ifndef  _MATLAB_BAM_CREATE
  /*
  * @violates @ref MLIB_AbsSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_AbsSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_AbsSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
  * parameter in definition of macro. 
  */
  #define MLIB_AbsSat(...)     macro_dispatcher(MLIB_AbsSat, __VA_ARGS__)(__VA_ARGS__)     /*!< This function returns absolute value of input parameter and saturate if necessary. */

  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F32)
    /*
    * @violates @ref MLIB_AbsSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_AbsSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_AbsSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00015 */
    #define MLIB_AbsSat_Dsptchr_1(In)     MLIB_AbsSat_Dsptchr_2(In,F32)     /*!< Function dispatcher for MLIB_AbsSat_Dsptchr_1, do not modify!!! */
  #endif 
  #if (SWLIBS_DEFAULT_IMPLEMENTATION == SWLIBS_DEFAULT_IMPLEMENTATION_F16)
    /*
    * @violates @ref MLIB_AbsSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
    * macro. 
    * @violates @ref MLIB_AbsSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
    * @violates @ref MLIB_AbsSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
    * parameter in definition of macro. 
    */
    /** @remarks Implements DMLIB00015 */
    #define MLIB_AbsSat_Dsptchr_1(In)     MLIB_AbsSat_Dsptchr_2(In,F16)     /*!< Function dispatcher for MLIB_AbsSat_Dsptchr_1, do not modify!!! */
  #endif 

  /*
  * @violates @ref MLIB_AbsSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_AbsSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_AbsSat_h_REF_3 MISRA 2004 Required Rule 19.10, Unparenthesized macro 
  * parameter in definition of macro. 
  */
  #define MLIB_AbsSat_Dsptchr_2(In,Impl)    MLIB_AbsSat_Dsptchr_(In,Impl)     /*!< Function dispatcher for MLIB_AbsSat_Dsptchr_2, do not modify!!! */
  
  /*
  * @violates @ref MLIB_AbsSat_h_REF_1 MISRA 2004 Required Rule 19.4, Disallowed definition for 
  * macro. 
  * @violates @ref MLIB_AbsSat_h_REF_2 MISRA 2004 Advisory Rule 19.7, Function-like macro defined. 
  * @violates @ref MLIB_AbsSat_h_REF_4 MISRA 2004 Advisory Rule 19.13, #/##' operator used in macro. 
  */
  /** @remarks Implements DMLIB00017 */
  #define MLIB_AbsSat_Dsptchr_(In,Impl)     MLIB_AbsSat_##Impl(In)            /*!< Function dispatcher for MLIB_AbsSat_Dsptchr_, do not modify!!! */
#endif
 
/****************************************************************************
* Typedefs and structures       (scope: module-local)
****************************************************************************/

/****************************************************************************
* Exported function prototypes
****************************************************************************/

/****************************************************************************
* Inline functions
****************************************************************************/
/* MLIB_AbsSat_F32 implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac32 AbsSat_F32_C(register tFrac32 f32In)
/*
* @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file.
*/
{
  /*
  * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
  * header file. 
  */
  register tS32 s32Temp;
  
  s32Temp = F32TOINT32(f32In);
  if (s32Temp == INT32_MIN){
    s32Temp = INT32_MAX;  
  }
  return(MLIB_Abs_F32(INT32TOF32(s32Temp)));
}
#ifdef MLIB_SPE1
  #if defined(__ghs__)
  /* MLIB_AbsSat_F32 implementation variant - SPE1 assembly for GHS toolchain */
  INLINE tFrac32 AbsSat_F32_GHS_SPE1(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file.
  */
  {
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Return, f32SatMax;

    #pragma ghs optasm
    asm(" evabs %0,%1" : "=r"(f32Return): "r"(f32In));
    #pragma ghs optasm
    asm(" nor. %0,%1,%1" : "=r"(f32SatMax): "r"(f32Return) : "cc");
    #pragma ghs optasm
    asm(" isel %0,%1,%2,0" : "+r"(f32Return) : "b"(f32Return) "b"(f32SatMax) : "cc");
    return(f32Return);
  }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  /* MLIB_AbsSat_F32 implementation variant - SPE1 assembly for CW toolchain */
  #pragma always_inline on
  INLINE tFrac32 AbsSat_F32_CW_SPE1(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Return, f32SatMax;

    asm(" evabs %0,%1" : "=r"(f32Return): "r"(f32In));
    asm(" nor. %0,%1,%1" : "=r"(f32SatMax): "r"(f32Return) : "cc");
    asm(" isel %0,%1,%2,0" : "+r"(f32Return) : "b"(f32Return), "b"(f32SatMax) : "cc");
    return(f32Return);
  }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
  /* MLIB_AbsSat_F32 implementation variant - SPE1 assembly for S32DS for Power Architecture toolchain */
  INLINE tFrac32 AbsSat_F32_S32DSPPC_SPE1(register tFrac32 f32In)
  /*
  * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Return, f32SatMax;

    __asm__ volatile(" evabs %0,%1" : "=r"(f32Return): "r"(f32In));
    __asm__ volatile(" nor. %0,%1,%1" : "=r"(f32SatMax): "r"(f32Return) : "cc");
    __asm__ volatile(" isel %0,%1,%2,0" : "+r"(f32Return) : "b"(f32Return), "b"(f32SatMax) : "cc");
    return(f32Return);
  }
  #endif
#endif /* ifdef MLIB_SPE1 */
/* MLIB_AbsSat_F16 implementation variant - C */
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif
INLINE tFrac16 AbsSat_F16_C(register tFrac16 f16In)
/*
* @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
* file.
*/
{
  /*
  * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
  * header file. 
  */
  register tS16 s16Temp;

  s16Temp = F16TOINT16(f16In);
  if (s16Temp == INT16_MIN){
    s16Temp = INT16_MAX;
  }
  return(MLIB_Abs_F16(INT16TOF16(s16Temp)));
}
#ifdef MLIB_SPE1
  #if defined(__ghs__)
  /* MLIB_AbsSat_F16 implementation variant - SPE1 assembly for GHS toolchain */
  INLINE tFrac16 AbsSat_F16_GHS_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header 
  * file.
  */
  {
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Tmp1, f32SatMax;
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac16 f16Return;
    
    f32Tmp1 = MLIB_ShL_F32((tFrac32)f16In, 16u);
    #pragma ghs optasm
    asm(" evabs %0,%1" : "+r"(f32Tmp1): "r"(f32Tmp1));
    #pragma ghs optasm
    asm(" nor. %0,%1,%1" : "=r"(f32SatMax): "r"(f32Tmp1) : "cc");
    #pragma ghs optasm
    asm(" isel %0,%1,%2,0" : "+r"(f32Tmp1) : "b"(f32Tmp1) "b"(f32SatMax) : "cc");
    f16Return = (tFrac16)MLIB_ShR_F32(f32Tmp1, 16u);
    return(f16Return);
  }
  #elif defined(__CWCC__) || defined(__MWERKS__)
  /* MLIB_AbsSat_F16 implementation variant - SPE1 assembly for CW toolchain */
  #pragma always_inline on
  INLINE tFrac16 AbsSat_F16_CW_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Tmp1, f32SatMax;
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac16 f16Return;
    
    f32Tmp1 = MLIB_ShL_F32((tFrac32)f16In, 16u);
    asm(" evabs %0,%1" : "+r"(f32Tmp1): "r"(f32Tmp1));
    asm(" nor. %0,%1,%1" : "=r"(f32SatMax): "r"(f32Tmp1) : "cc");
    asm(" isel %0,%1,%2,0" : "+r"(f32Tmp1) : "b"(f32Tmp1), "b"(f32SatMax) : "cc");
    f16Return = (tFrac16)MLIB_ShR_F32(f32Tmp1, 16u);
    return(f16Return);
  }
  #elif defined(__GNUC__) && defined(__PPC_EABI__)
  /* MLIB_AbsSat_F16 implementation variant - SPE1 assembly for S32DS for Power Architecture toolchain */
  INLINE tFrac16 AbsSat_F16_S32DSPPC_SPE1(register tFrac16 f16In)
  /*
  * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in header
  * file.
  */
  {
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac32 f32Tmp1, f32SatMax;
    /*
    * @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
    * header file. 
    */
    register tFrac16 f16Return;
    
    f32Tmp1 = MLIB_ShL_F32((tFrac32)f16In, 16u);
    __asm__ volatile(" evabs %0,%1" : "+r"(f32Tmp1): "r"(f32Tmp1));
    __asm__ volatile(" nor. %0,%1,%1" : "=r"(f32SatMax): "r"(f32Tmp1) : "cc");
    __asm__ volatile(" isel %0,%1,%2,0" : "+r"(f32Tmp1) : "b"(f32Tmp1), "b"(f32SatMax) : "cc");
    f16Return = (tFrac16)MLIB_ShR_F32(f32Tmp1, 16u);
    return(f16Return);
  }
  #endif
#endif /* ifdef MLIB_SPE1 */





/****************************************************************************
* Implementation variant: 32-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@brief        This function returns absolute value of input parameter and saturate if necessary.

@param[in]    f32In      Input value.

@return       Absolute value of input parameter, saturated if necessary.

@details      The input values as well as output value is considered as 32-bit fractional data 
              type.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_AbsSat_F32
              \image rtf abssatEq1_f32.math "MLIB_AbsSat_Eq1"

*/
/*!
@note         Due to effectivity reason this function is implemented as inline assembly, and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac32 f32In;
tFrac32 f32Out;

void main(void)
{
    // input value = -0.25
    f32In = FRAC32(-0.25);

    // output should be FRAC32(0.25)
    f32Out = MLIB_AbsSat_F32(f32In);

    // output should be FRAC32(0.25)
    f32Out = MLIB_AbsSat(f32In, F32);

    // ##############################################################
    // Available only if 32-bit fractional implementation selected
    // as default
    // ##############################################################

    // output should be FRAC32(0.25)
    f32Out = MLIB_AbsSat(f32In);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif /* if defined __CWCC__ || defined __MWERKS__ */
/** @remarks Implements DMLIB00016, DMLIB00010, DMLIB00012, DMLIB00014, DMLIB00019 */
INLINE tFrac32 MLIB_AbsSat_F32(register tFrac32 f32In)
/*
* @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #ifdef MLIB_SPE1
    #if defined(__ghs__)
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F32_GHS_SPE1(f32In));
    #elif defined(__DCC__)
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F32_C(f32In));
    #elif defined(__CWCC__) || defined(__MWERKS__)
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F32_CW_SPE1(f32In));
    #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F32_S32DSPPC_SPE1(f32In));
    #else
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F32_C(f32In));
    #endif
  #else
    /** @remarks Implements DMLIB00013 */
    return(AbsSat_F32_C(f32In));
  #endif
}





/****************************************************************************
* Implementation variant: 16-bit fractional
****************************************************************************/
/***************************************************************************/
/*!
@ingroup    MLIB_GROUP

@param[in]    f16In      Input value.

@return       Absolute value of input parameter, saturated if necessary.

@details      The input values as well as output value is considered as 16-bit fractional data 
              type.

              \par

              The output of the function is defined by the following simple equation:
              \anchor eq1_AbsSat_F16
              \image rtf abssatEq1_f16.math "MLIB_AbsSat_Eq1"

*/
/*!
@note         Due to effectivity reason this function is implemented as inline, and thus is not ANSI-C compliant.
*/
/*!

@par Code Example
\code
#include "mlib.h"

tFrac16 f16In;
tFrac16 f16Out;

void main(void)
{
    // input value = -0.25
    f16In = FRAC16(-0.25);

    // output should be FRAC16(0.25)
    f16Out = MLIB_AbsSat_F16(f16In);

    // output should be FRAC16(0.25)
    f16Out = MLIB_AbsSat(f16In, F16);

    // ##############################################################
    // Available only if 16-bit fractional implementation selected
    // as default
    // ##############################################################

    // output should be FRAC16(0.25)
    f16Out = MLIB_AbsSat(f16In);
}
\endcode
****************************************************************************/
#if defined(__CWCC__) || defined(__MWERKS__)
#pragma always_inline on
#endif /* if defined __CWCC__ || defined __MWERKS__ */
/** @remarks Implements DMLIB00016, DMLIB00010, DMLIB00011, DMLIB00014, DMLIB00020 */
INLINE tFrac16 MLIB_AbsSat_F16(register tFrac16 f16In)
/*
* @violates @ref MLIB_AbsSat_h_REF_5 MISRA 2004 Required Rule 8.5, Object/function definition in 
* header file. 
*/
{
  #ifdef MLIB_SPE1
    #if defined(__ghs__)
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F16_GHS_SPE1(f16In));
    #elif defined(__DCC__)
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F16_C(f16In));
    #elif defined(__CWCC__) || defined(__MWERKS__)
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F16_CW_SPE1(f16In));
    #elif defined(__GNUC__) && defined(__PPC_EABI__)
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F16_S32DSPPC_SPE1(f16In));
    #else
      /** @remarks Implements DMLIB00013 */
      return(AbsSat_F16_C(f16In));
    #endif
  #else
    /** @remarks Implements DMLIB00013 */
    return(AbsSat_F16_C(f16In));
  #endif
}

#ifdef __cplusplus
}
#endif

#endif /* MLIB_ABSSAT_H */
