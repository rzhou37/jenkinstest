MPC564xL_AMMCLIB_1.1.9
=======================
Revision 1.1.9
30-June-2017

RELEASE NOTES
=============
This is the NXP Service Release for rev 1.1.9 of the Automotive Math and Motor Control Library Set for NXP MPC564xL devices, supporting the Power Architecture e200z4 core based devices.

Installation instructions:
==========================
Run MPC564xL_AMMCLIB_RTM_1_1_9.exe self executable file, in order to start the installation procedure and follow the instruction on the screen. The recommended destination path (the default one) is: C:\Freescale\AMMCLIB\MPC564xL_AMMCLIB_1.1.9

Important notes:
================
As the Automotive Math and Motor Control Library Set for NXP MPC564xL devices supports the global configuration option, it is highly recommended to copy the SWLIBS_Config.h file to your local structure and refer the configuration to this local copy. This approach will prevent the incorrect setup of default configuration option, in case multiple projects with different default configuration are used.

The Automotive Math and Motor Control Library Set for NXP MPC564xL devices is delivered without pre-selected default configuration option in the SWLIBS_Config.h file. The user is responsible to set the default configuration option according the application need, otherwise the error message is displayed and the compilation of the user application did not succeed.

The Automotive Math and Motor Control Library Set for NXP MPC564xL devices was compiled and tested without using the float-only makefile switch (e.g. -floatonly in GreenHills compiler). This means all implicit operations in floating point math are considered as double precisions including the constants. In case user needs the single precision operations or single precisions constants, these have to be properly casted. Special care have to be taken in account in case of FRAC32(x) and FRAC16(x) macro-functions use. If the user target application is compiled using the float-only switch (thus all floating point operations are by default considered as single precision floating point), the conversion between the floating point number and the 32-bit/16-bit fraction number might be imprecise.

Known limitations and issues:
=============================
- To fully utilize the MPC564xL capabilities while keeping the code efficient, the Automotive Math and Motor Control Library Set for NXP MPC564xL devices might include the compiler standard header files.
- A special care have to be taken, in case user-specific datatypes are defined in user application to avoid the compiler standard data type redefinition.

The library was built and tested using the following compilers:
===============================================================
- GreenHills MULTI for PPC version 2015.1.4
- Wind River Compiler version 5.9.4.8
- CodeWarrior Development Studio for NXP MPC55xx, MPC56xx Eclipse IDE, version 10.6.4
- S32 Design Studio for Power Architecture v1.1

What's new in 1.1.9:
====================
- (MCL-382) IAR compiler - optimization level in several functions had to be lowered as a workaround for a compiler bug.
- (MCL-367) Removed floating-point BAM blocks from ammclib_bam.mdl file for platforms which do not support the floating-point implementation.
- (MCL-374) Removed duplicated eq. 7 and 8 from the documentation of GMCLIB_SvmStd.
- (MCL-368) Removed include of lsp.h for S32 Design Studio.
- (MCL-379) Changed supported IAR compiler version to v8.11.1.13263. Older versions contain an optimizer bug and are incompatible with new AMMCLib functions.
- (MCL-355) Added new scalar and vector base-10 logarithm functions - GFLIB_Log10_FLT, GFLIB_VLog10_FLT.
- (MCL-363) Changed supported version of the S32 Design Studio for Power Architecture platforms to v1.2.

Release history:
================
Rev 1.1.8
- (MCL-343) Repeated sections were merged into a common chapters in the User Guide. This approach was applied to all AMMCLIB functions.
- (MCL-345) The macro dispatcher names for default-configuration options of all AMCLIB_TrackObserver implementations was corrected.
- (MCL-347) Missing "#pragma always_inline" were added in front of the MLIB_DivSat_F16, MLIB_Add_F16 and MLIB_ShR_F16 functions.
- (MCL-354) Parentheses around the argument of the FRAC16 and FRAC32 macros were added.

Rev 1.1.7
- New advanced motor control functions AMCLIB_BemfObsrvDQ and AMCLIB_TrackObsrv were developed and added into AMMCLIB library set.
- (ENGR00384609) SPE implementation for MLIB_AbsSat function was added.
- (ENGR00384779) As per GPIS marketing decision the production ready SW term was replaced by automotive grade SW.
- (ENGR00385951) Assembly optimizations for MLIB functions for Wind River Diab compiler have been removed as a workaround for compiler defects TCDIAB-14184 and TCDIAB-14186.

Rev 1.1.6
- (ENGR00380372) Performance of several GFLIB and GMCLIB library functions has been improved by assembly optimization.
- (ENGR00381792) Enabled use of intrinsic functions for signal processing engine. Certain compiler-supplied header files must be included during library compilation, see "library integration" chapter in the User Guide.
- (ENGR00381819) Changed WindRiver Diab compiler version to 5.9.4.8. New compiler options are required for library compilation, see "Pre-compiled library compilation options" section below.
- (ENGR00382672) The GreenHills compiler does not inline functions declared as static __inline. The inline keyword in SWLIBS_Defines.h has been changed.
- (ENGR00382930) The fixed-point MLIB_Msu and MLIB_Mnac functions have been added to the precision table in user manual. All fixed-point implementations of these functions are nonsaturating.

Rev 1.1.5
- (ENGR00357322) Added information about possible exceptions in the User Guide.
- (ENGR00375386) The 16-bit fractional implementations of the MLIB_Mac, MLIB_MacSat, MLIB_Mnac, MLIB_Msu and MLIB_VMac functions were modified to calculate the intermediate results in full 32-bit precision.
- (ENGR00375549) Criteria for the floating point variants of the GFLIB_AtanYX and GFLIB_AtanYXShifted functions have been corrected. Error bounds of these functions have been updated as well.
- (ENGR00377411) The GFLIB_ControllerPIp_Eq4 equation for all implementations was corrected to reflect the Bilinear transformation of integral part.
- (ENGR00377712) The function precision table in User Guide was updated to reflect the MLIB_VMac_F16 maximum error 2LSB.
- (ENGR00377714) Precision of the 32-bit fractional implementation of the GMCLIB_Park function was improved by removing unnecessary shifts used for intermediate results calculation.
- (ENGR00377711) New MLIB_RndSat_F16F32 function was added. Accuracy of the 16-bit fractional implementations of the GMCLIB_Park and GFLIB_ControllerPIp functions was increased.
- (ENGR00377771) Implementation of fixed-point controllers has been changed to prevent undesirable saturation in cases when the coefficients are scaled to the full fractional range.
- (ENGR00373622) All MPC57xx devices and selected MPC56xx platforms (MPC560xB, MPC560xP and MPC564xL) now support the S32 Design Studio for Power Architecture compiler.
- (ENGR00376793) All critical MLIB functions and GFLIB_Sqrt function were optimized for GCC (S32 Design Studio for Power Architecture) compiler using SPE1/SPE2/LSP/EFPU2 extensions.
- (ENGR00375342) The example code for GFLIB_ControllerPIrAW_FLT and GFLIB_ControllerPIr_FLT functions were corrected in the AMMCLIB User Guides.
- (ENGR00376029) The chapter "Library Integration into a S32 Design Studio for Power Architecture Environment" was added in the User Guide.
- (ENGR00379470) The equation of transfer function in GFLIB_ControllerPIr and GFLIB_ControllerPIrAW in User Guide was corrected.

Rev 1.1.4
- (ENGR00371353) Implementation of fix32-to-fix16 rounding error correction in the GFLIB_ControllerPIr_F16, GFLIB_ControllerPIrAW_F16, GFLIB_ConlrollerPIpAW_F16 and GFLIB_IntegratorTR_F16 functions was changed to respect the value on 15-bit position.
- (ENGR00372154) The "Matlab Integration" chapter in user guides was updated/corrected.

Rev 1.1.3
- (ENGR00362922) New unified single library file ammclib_bam.mdl for all BAM models was created.
- (ENGR00368436) The organization of bam folder in the installation directory was synchronized with the arichitecture of the User Guide and single Matlab/Simulink library, thus all Bit Accurate Models are stored in the following structure: <Library module> - <Implementation> - <all BAM models from the same library module and implementation>
- (ENGR00369315) During the floating point precision documentation generation phase, presence of all applicable equations is checked by new script.
- GHS Multi Compiler version was increased to version 2015.1.4.

Rev 1.1.2
- (ENGR00362902) New functions, GFLIB_SinCos, for all implementations added, calculating the Sine and Cosine from input angle.
- (ENGR00363338) A new document describing the accuracy of floating-point functions has been included into the release.
- (ENGR00365065) The installation package, quality pack and release notes filenames were synchronized with the rest of the FSL SW products. The naming convention is as follows:
    <Platform>_AMMCLIB_RTM_<MajorReleaseNumber>_<MinorReleaseNumber>_<BuildNumber>.exe
    <Platform>_AMMCLIB_RTM_<MajorReleaseNumber>_<MinorReleaseNumber>_<BuildNumber>_ReleaseNotes.txt
    <Platform>_AMMCLIB_RTM_<MajorReleaseNumber>_<MinorReleaseNumber>_<BuildNumber>QualityPackage.zip
    <Platform>_AMMCLIB_RTM_<MajorReleaseNumber>_<MinorReleaseNumber>_<BuildNumber>_EVAL.exe
    <Platform>_AMMCLIB_RTM_<MajorReleaseNumber>_<MinorReleaseNumber>_<BuildNumber>_EVAL_ReleaseNotes.txt
- (ENGR00365931) The description of GDFLIB_IntegratorTR_F32/GFLIB_IntegratorTR_F16 behavior in case of incorrect scaling added to the User Guide.
- (ENGR00365999) The BlackDuck open source scanning now covering all AMMCLIB platforms as well as free and paid variants. Output Bill of Material reports from BlackDuck scan are stored in the QualityPack in all installation packages.

Rev 1.1.1
- (ENGR00362392) The SPE1 implementation of MLIB_VMac_F32F16F16 and MLIB_VMac_F16 functions were modified in the GHS, CW10x and DIAB compilers sections to prevent the 64-bit register usage.
- (ENGR00362295) The SPE1 implementation of MLIB_VMac_F32 function was modified on GHS compiler section to prevent the 64-bit register usage.
- (ENGR00362247) Due to the performance reasons, the MLIB_Mnac_FLT, MLIB_Msu_FLT and MLIB_Mac_FLT functions are implemented using the ASM instructions in the free of charge releases as well.
- (ENGR00361555) The 32*32->32 multiplication in MLIB_Convert_FLTF32 function was replaced by 64*64->32 to meet the precision expectations.
- (ENGR00360912) Guaranteed accuracy of single precision floating point implementation of the GFLIB_Sqrt function has been improved.
- (ENGR00359995) Checking for zero divisor in the single precision floating point implementation of the MLIB_Div function was removed.
- (ENGR00357802) Accuracy of single precision floating point implementation of the GMCLIB_ClarkInv function has been improved.
- (ENGR00357427) Handling of denormalized inputs has been added into single precision floating point implementation of the GFLIB_Sin function.
- (ENGR00357365) The single precision floating point implementation of the GFLIB_IntegratorTR function has been changed to improve performance.
- (ENGR00357204) Common parts of single precision floating point implementation of the GFLIB_ControllerPIrAW and GFLIB_ControllerPIr functions have been consolidated to use the same algorithm.
- (ENGR00357094) Computation of the integral part of single precision floating point implementation of the GFLIB_ControllerPIpAW function has been consolidated with GFLIB_ControllerPIp function.
- (ENGR00357058) Accuracy of single precision floating point implementation of the GFLIB_ControllerPIp function has been improved.
- (ENGR00356446) Handling of denormalized inputs has been added into single precision floating point implementation of the GFLIB_Atan function.
- (ENGR00356328) Accuracy of single precision floating point implementation of the GFLIB_Acos function has been improved.
- (ENGR00356244) Accuracy of single precision floating point implementation of the GFLIB_Asin function has been improved.
- (ENGR00356072) Number of the significant digits of the calculated IIR1/IIR2 coefficients in the MATLAB examples for GDFLIB_IIR1/GDFLIB_IIR2 functions in the user guide was increased.
- (ENGR00354724) Execution speed and accuracy of single precision floating point implementation of the GDFLIB_FilterIIR1 and GDFLIB_FilterIIR2 functions has been improved.
- (ENGR00353734) The "__clb16_32" and "__clb16_16" intrinsic functions were used instead of the asm wrappers in the MLIB_Norm_F16 and MLIB_Norm_F32 functions.
- (ENGR00353246) The redundant C implementations were removed from the GFLIB_Sqrt function, avoiding the code duplicities.
- (ENGR00353035) GMCLIB_ElimDcBusRip_FLT prevents division by denormalized values.
- (ENGR00352979) New MLIB functions MLIB_Msu and MLIB_Mnac were added.
- (ENGR00352971) Accuracy of single precision floating point implementation of the GMCLIB_DecouplingPMSM function has been improved.
- (ENGR00352794) Changed GreenHills compiler option -fhard to -fsingle to support SW emulation of double arithmetic.
- (ENGR00352709) Compilers versions were increased to:
    - GHS Multi 6.1.6/compiler 2014.1.6 for Power Architecture and Cortex platforms
    - Diab 5.9.4.4  for Power Architecture platforms
    - CodeWarrior 10.6.4  for Power Architecture, MagniV and Cortex platforms
- (ENGR00352698) Single precision floating point implementation of the MLIB_VMac function has been improved to eliminate cases of catastrophic cancellation.
- (ENGR00352252) Single precision floating point implementation of the MLIB_Mac function on WindRiver Diab compiler uses dedicated instruction.
- (ENGR00352108) Parameter in the input structure and implementation of single precision floating point variant of the GDFLIB_FilterMA function has been changed.
- (ENGR00350929) Accuracy of single precision floating point implementation of the GFLIB_Tan function has been increased.

Rev 1.1.0
- (ENGR00327538) The Bit Accurate models of the GFLIB_ControllerPIrAW, GFLIB_ContrllerPIpAW and GMCLIB_ElimDCBusRip functions were modified to support the on-run modification of the DcBus and Limits parametes. These parameters are now accessible as standard inputs.
- (ENGR00343742) The Qorivva brand name has been retired and was removed from all customer related documentations (user guide, release notes, reports, quality pack, web-page, onepager etc.).
- (ENGR00345955) Release number was removed from the final precompiled library name *.a for all supported compilers. Affected user guide and release notes were modified accordingly.
- (ENGR00346085) The release numbers were harmonized across the supported devices. Starting with the Release to Market revision 1.1.0, the similar content of the Automotive Math and Motor Control Library Set for NXP MPC564xL devices is released with the same revision number, independently on the supported device.
- (ENGR00347932) The redundant C implementations were removed from appropriate MLIB functions, avoiding the code duplicities.
- (ENGR00350929) The accuracy of GFLIB_Tan_FLT has been increased.   

Rev 1.0.4
- (ENGR00335129) The rendundant shift in each multiplication was removed in GFLIB_Sqrt_F32/F16 functions.
- (ENGR00321295) Misplaced traceability tags in files MLIB_NegSat.h, MLIB_AddSat.h, MLIB_MulSat.h, and MLIB_SubSat.h were corrected.
- (ENGR00316625) Equation describing functions MLIB_Convert_FLTF16 and MLIB_Convert_FLTF32 were corrected.
- (ENGR00315789) The definition of MLIB_Round functionality has been reformulated and newly implemented.
- (ENGR00311279) The MLIB_Div_F32 function was corrected. The MLIB_Abs_F32 function and surounding code was replaced by the MLIB_Norm_F32 function to correctly handle the division by -1.
- (ENGR00333259) The reference model of the MLIB_Norm function was updated to correctly handle the negative numbers.
- (ENGR00331246) Redundant normalization removed from the MLIB_Div_F32 function.
- (ENGR00327332) Removing the redundant normalization in the GFLIB_AtanYX_F32 implementation.
- (ENGR00335150) The redundant if-else section was removed from MLIB_Div_F32/F16 implementation as the In1>=In2 cases are not supported and the output of the function is undefined for these cases.
- (ENGR00335289) The MLIB_DivSat_F32/F16 function was modified to better performance by moving the limit calculation to section where the limits are used.
- (ENGR00336372) Redundant MLIB_AddSat_F16 removed from the F16 implementation of the GMCLIB_Park_F16/GMCLIB_ParkInv_F16 function.
- CodeWarrior Classic compiler was removed from the list of supported compilers.

Rev 1.0.3
- The basic mathematical functions in the MLIB library are implemented and optimized using the Signal Processing Engine version 1 (SPE1) extension.
- Compiler Wind River version 5.9.4.4 support added.
- Revision history table has been added to the User Guide.
- Issue in non-symmetric look-up table in the floating point implementation of the GFLIB_Lut2D_FLT function  was corrected. This error was corrected also in 16-bit as well as in 32-bit implementations.
- Pragma "always_inline on" was added before the definition of all MLIB functions for CodeWarrior compiler to ensure that all MLIB functions are correctly inlined.
- The accumulation output error caused by incorrectly compensated right shift loose of precision in the GFLIB_IntegratorTR_F16 was corrected by moving the correction constant addition after the accumulator store.

Rev 1.0.2
- Redundant negation operation was removed in the GMCLIB_ClarkInv_FLT function.
- The User Guide section describing the u16NSamples range for GDFLIB_FilterMA_F16 was harmonized across the function description.
- Missing sections for some functions in the User Guide was added.
- Application examples in the user documentation for GFLIB_Lut2D function were corrected.
- Casting for single floating point data type of the input value was removed from FRAC32(x) macro.
- Hexadecimal representations of the maximal fractional numbers in the FRAC16/FRAC32 macro functions were replaced by double floating point values and incorrect casting of the input value to float data type was removed.
- The boundary limit in GFLIB_Asin_FLT functions has been correctly casted to float.
- Wrong "float only" switches as "fp spfp" for CW and CW10x compilers, "floatsingle" for GHS compiler and "Xfp-float-only" for DIAB compiler were removed from library makefile for all supported NXP platforms.
- Parameter "mcDefaultImpl" of the structure SWLIBS_VERSION_T is redundant and was removed from structures in the SWLIBS_Version.c and SWLIBS_Version.h files.
- The GFLIB_Sqrt_F32 and GFLIB_Sqrt_F16 functions were optimized to reach better performance results.

Rev 1.0.1
- The 64-bit installation of the Matlab is supported, thus the BAM models in 32-bit and 64-bit variants are provided for all supported functions.
- Missing feedback loop was corrected for the GFLIB_Controller_PIp, GFLIB_Controller_PIpAW, GFLIB_Controller_PIr, GFLIB_Controller_PIrAW, GFLIB_IntegratorTR, GDFLIB_FilterMA and GDFLIB_FilterFIR Bit Accurate Models.
- The u16NSamples range for GDFLIB_FilterMA_F16 function was harmonized accross the function description.
- The multiplication constat FLOAT_0_5 used in the first summand in the GMCLIB_ClarkInv_FLT function was replaced by negative value FLOAT_MINUS_0_5 thus negation in the mathematical expression became unnecessary and was removed.
- Default values for pointers in the structures containing 1D and 2D look-up table parameters in the GFLIB_Lut1D and GFLIB_Lut2D functions were corrected from zeros to the pointers to zero.

Rev 1.0.0
- All functions tested using MATLAB Bit Accurate Models and using target-in-loop test method.
- Precision of all 16-bit function implementations was changed from 14 bits to full 16 bits.
- The default installation destination directory was changed to c:\Freescale\AMMCLIB\MPC564xL_AMMCLIB_v1.0.0
- Antiwindup implementation of the GFLIB_Controller_PIrAW was changed to the output limitation model.
- The GFLIB_ControllerPIp and GFLIB_ControllerPIpAW was modified to accept the negative proportional and integration gain shift.
- Incorrect condition in the MLIB_Convert_FLTF16 and MLIB_Convert_FLTF32 functions were corrected.
- Macro dispatchers of several functions were corrected to accept the multiple parameters.
- Default configuration macro and supported macro prefixes were changed from MLIB to SWLIBS.
- The user guide was updated to reflect the change of the installation process.
- The s16ShamIntvl/s16ShamIntvl1/s16ShamIntvl2 parameters for 16-bit fractional implemetation as well as the s32ShamIntvl/s32ShamIntvl1/s32ShamIntvl2 parameters for 32-bit fractional implementation were removed from GFLIB_LUT1D_T_F16/GFLIB_LUT1D_T_F32/GFLIB_LUT2D_T_F16/GFLIB_LUT2D_T_F32 parameter structures. 

Rev 0.94
- New cross-platform concept introduced allowing the user to change the implementation on-run without any limitation.
- Support of single precision floating point, fixed-point 16-bit fractional and fixed-point 32-bit fractional implementations.
- All functions tested using MATLAB Bit Accurate Models and using target-in-loop test method.
- NXP CodeWarrior Eclipse IDE support added.
- The internal accumulator of GFLIB_ControllerPIr and GFLIB_ControllerPIrAW was changed to 32-bit to eliminate the accumulation error.
- Incorrect call of MLIB_NegSat_F32 function instead of MLIB_NegSat_F16 function corrected in GFLIB_Acos_F16 and GFLIB_Asin_F16 functions.
- Variables "f32Cos" and "f32Sin" declared as volatile were changed to static declaration, thus variables can not be modified and accessed from outside of the GFLIB_VectorLimit_F16 function.

Pre-compiled library compilation options:
=========================================
The Automotive Math and Motor Control Library Set for NXP MPC564xL devices is delivered with pre-compiled libraries for all supported compilers. The following options were used to build these libraries:
GreenHills compiler for Power Architecture platforms:
    -cpu=ppc564xl \
    -bsp generic \
    -Ospeed -Opeep -Opipeline -isel -OI -Omax -OL -OM -Oconstprop -Ominmax -Ocse -Otailrecursion -Ointerproc \
    -auto_sda \
    -fsingle \
    -no_precise_signed_zero \
    -MD \
    -SPE \
    -vle \
    --gnu_asm

Diab Wind River compiler:
    -tPPCE200Z4VEG:simple \
    -Xdialect-ansi \
    -Xaddr-sconst=0x11 -Xaddr-sdata=0x11 -Xsmall-const=0x50 -Xsmall-data=0 \
    -Xno-common \
    -O -XO -Xsize-opt \
    -Xopt-count=2 -Xsavefpr-avoid -Xstmw-fast \
    -W:as:$(_COMMA_CHAR_)-l \
    -Wa$(_COMMA_CHAR_)-Xisa-vle \
    -vle \
    -Xforce-declarations \
    -Xc-new

CodeWarrior Eclipse IDE:
    -abi eabi \
    -processor zen \
    -fp spfp \
    -spe_vector \
    -schedule on \
    -use_isel on \
    -use_lmw_stmw on \
    -vle \
    -ansi off \
    -inline smart$(_COMMA_CHAR_)auto$(_COMMA_CHAR_)bottomup \
    -func_align 16 \
    -opt speed$(_COMMA_CHAR_)level=4$(_COMMA_CHAR_)peephole$(_COMMA_CHAR_)schedule$(_COMMA_CHAR_)dead$(_COMMA_CHAR_)propagation$(_COMMA_CHAR_)loopinvariants \
    -requireprotos \
    -warnings all \
    -sym off \
    -Cpp_exceptions off \
    -RTTI off \
    -gccext on \
    -nosyspath
Note: The compiler requires inclusion of the following paths:
<compiler main dir>\MCU\PA_Support\ewl\EWL_Runtime\Runtime_PA\Include
<compiler main dir>\MCU\PA_Support\ewl\EWL_C\include

GCC compiler in S32 Design Studio IDE:
    -mcpu=e200z4 \
    -mspe \
    -mbig -mvle -mhard-float \
    -fmessage-length=0 -Wall -c \
    -std=c99 \
    -O3 -ffunction-sections -fdata-sections -mregnames \
    -fsigned-char

