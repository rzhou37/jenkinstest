/*
 * Trial License - for use to evaluate programs for possible purchase as
 * an end-user only.
 *
 * File: flexcan_init.c
 *
 * Code generated for Simulink model 'swg_RESOLVER'.
 *
 * Model version                  : 1.310
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Mon Jun 26 14:28:07 2017
 *
 * Target selection: rappid564xl.tlc
 * Embedded hardware selection: Freescale->32-bit PowerPC
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

/********************  Dependent Include files here **********************/
#include "mpc5643l.h"
#include "flexcan_init.h"

/**********************  Initialization Function(s) *************************/
void flexcan0_init (void)
{
  {
    uint8_t status, x, i;
    CAN_0.MCR.B.MDIS = 0;
    CAN_0.MCR.B.SOFT_RST = 1;
    while (CAN_0.MCR.B.SOFT_RST ==1) {
    }

    CAN_0.MCR.B.MDIS = 1;
    CAN_0.CR.B.CLK_SRC = 0;

    /*Enable the CAN module */
    CAN_0.MCR.B.MDIS = 0;

    /* Set FRZ bit */
    CAN_0.MCR.B.FRZ = 1;

    /* Set HALT bit */
    CAN_0.MCR.B.HALT = 1;
    CAN_0.MCR.R = 1342177343;
    CAN_0.CR.R = 0x04DB0086;

    /* Set the FlexCAN Maximum Buffer Number */
    CAN_0.MCR.B.MAXMB = 32;

    /* clear memory from message buffer 0 to 15 */
    for (x=0; x < 32; x++) {
      CAN_0.BUF[x].CS.R = 0;
      CAN_0.BUF[x].ID.R = 0;
      for (i=0; i < 2; i++) {
        CAN_0.BUF[x].DATA.W[i] = 0;
      }
    }

    for (x=0 ; x<32 ; x++) {
      CAN_0.RXIMR[x].R = 0xFFFFFFFF;
    }

    CAN_0.IMRL.R = 0x00000000;
    CAN_0.MCR.B.HALT = 0;
    CAN_0.MCR.B.FRZ = 0;

    /* await synchronization (delay) */
    for (x=1; x < 255; x++) {
    }

    if (CAN_0.MCR.B.NOT_RDY == 1) {
      status = 1;
    } else {
      status = 0;
    }
    (void) status;

    /* Can Pins */
    SIU.PCR[16].R = 0x0624;
    SIU.PCR[17].R = 0x0100;
    SIU.PSMI[33].R = 0x01;
  }
}

/*

 *######################################################################

 *                           End of File

 *######################################################################

 */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
