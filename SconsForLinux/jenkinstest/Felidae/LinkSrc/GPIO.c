/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-06-29
* Description: GPIO configurations
***************************************************************************************************/

#include "GPIO.h"

void GPIO_init(void)
{
    /* Initialize Pad Configuration Register 95,83 as Output */
    gpo_init_pcr_out_564xl_fnc( 83 );   // LED3
    gpo_init_pcr_out_564xl_fnc( 86 );   // LED1
    gpo_init_pcr_out_564xl_fnc( 79 );   // LED2
    gpo_init_pcr_out_564xl_fnc( 45 );   // 33937 enable
    gpo_pin_update_564xl_fnc(45, 0);    // 33937 enable
    gpo_pin_update_564xl_fnc(45, 1);    // 33937 enable
    gpo_init_pcr_out_564xl_fnc( 99 );   // FAULT_CLEAR
    gpo_pin_update_564xl_fnc(0, 0);
}
