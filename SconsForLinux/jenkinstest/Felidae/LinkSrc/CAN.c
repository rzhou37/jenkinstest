/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-07-11
* Description: CAN configuration and interrupts.
***************************************************************************************************/
#include <string.h>
#include "mpc5643l.h"

#include "GeneralFunction.h"
#include "PT_dbc_rx.h"
#include "PT_dbc_tx.h"

/*******************************************************************************
* Constants and macros
*******************************************************************************/
// FlexCAN module register's bit masks
#define CAN_MCR_MDIS                        0x80000000U
#define CAN_MCR_FRZ                         0x40000000U
#define CAN_MCR_FEN                         0x20000000U
#define CAN_MCR_HALT                        0x10000000U
#define CAN_MCR_NOTRDY                      0x08000000U
#define CAN_MCR_SOFTRST                     0x02000000U
#define CAN_MCR_FRZACK                      0x01000000U
#define CAN_MCR_WRNEN                       0x00200000U
#define CAN_MCR_LPMACK                      0x00100000U
#define CAN_MCR_SRXDIS                      0x00020000U
#define CAN_MCR_BCC                         0x00010000U
#define CAN_MCR_LPRIOEN                     0x00002000U
#define CAN_MCR_AEN                         0x00001000U

#define CAN_BUF_CODE_SHIFT                  24U                 // CODE shift
#define CAN_BUF_RX_EMPTY_B                  0x4U                // Receive active and empty in bit field
#define CAN_BUF_RX_EMPTY_R                  (CAN_BUF_RX_EMPTY_B << CAN_BUF_CODE_SHIFT)      // Receive active and empty in register
#define CAN_BUF_TX_INACTIVE_B               0x8U                // Inactive in transmit data mode in bit field
#define CAN_BUF_TX_INACTIVE_R               (CAN_BUF_TX_INACTIVE_B << CAN_BUF_CODE_SHIFT)   // Inactive in transmit data mode once in register
#define CAN_BUF_TX_EN_B                     0xCU                // Transmit data once in bit field
#define CAN_BUF_TX_EN_R                     (CAN_BUF_TX_EN_B << CAN_BUF_CODE_SHIFT)         // Transmit data once in register

#define CAN_BUF_STANDARD                    0x00000000U
#define CAN_BUF_EXTENDED                    0x00200000U

#define CAN_RXIMR_STANDARD_ID               0x1FFC0000U         // Filter to match full standard ID, 11 bits
#define CAN_RXIMR_EXTENDED_ID               0x0003FFFFU         // Filter to match full standard ID extended, 18 bits
#define CAN_RXIMR_FULL_ID                   0x1FFFFFFFU         // Filter to match full standard ID and ID extended, 29 bits

// Mailbox usage definition
#define CAN_BUFFER_NUM_FM_TX                0U                  // Reserved for Freemaster TX
#define CAN_BUFFER_NUM_FM_RX                1U                  // Reserved for Freemaster RX
#define CAN_BUFFER_NUM_VCU_TORQUE           2U
#define CAN_BUFFER_NUM_TEST                 5U
#define CAN_RX_INT_EN                       ((1U << CAN_BUFFER_NUM_VCU_TORQUE) | (1U << CAN_BUFFER_NUM_TEST))

#define FM_RESERVED_ID                      0U                  // Reserved for Freemaster

#define BYTES_PER_WORD                      4U                  // 4 8-bit bytes per 32-bit word

typedef struct s_can_data
{
    uint16_t id;                                                // Message ID
    uint8_t len;                                                // Data length in bytes: 0-8
    union
    {
        uint32_t word[2];                                       // Date stored in words (32 bits)
        uint16_t half[4];                                       // Date stored in half-words (16 bits)
        uint8_t  byte[8];                                       // Date stored in bytes (8 bits)
    } data;
    uint16_t timeStamp;
} CAN_DATA_S;

typedef struct s_can_tx_package
{
    uint8_t             mb;                                     // Mailbox number
    CAN_DATA_S          msgTx;                                  // Message to be transmitted
    void                (*packFunc)(CAN_DATA_S* const msgTx, uint16_t* const counter);         // Message pack function
    uint16_t*           counter;                                // Counter used for each message
} CAN_TX_PACKAGE_S;

/****************************************************************
 *                     C A N   D R I V E R                      *
 ****************************************************************/
/****************************************************************
 * Function for CAN module initialization
 *
 * @para[in] canModule:         CAN module address (CAN_0 or CAN_1)
 * @para[in] msgCs:             CAN mailbox configs
 * @para[in] msgId:             CAN message ID
 * @para[in] filterMask:        CAN mailbox filter mask
 * @para[in] mbInterrupt:       CAN mailbox interrupt
 ****************************************************************/
static void can_initModule_MPC564xL(volatile FLEXCAN_tag* canModule, const uint32_t* const msgCs, const uint32_t* const msgId, const uint32_t* const filterMask, const uint32_t mbInterrupt)
{
    uint16_t i, j;

    canModule->MCR.B.MDIS = 1U;                     // Disable the CAN module
    canModule->CTRL.B.CLK_SRC = 0;                  // Choose clock source only when module is disabled

    canModule->MCR.B.MDIS = 0;                      // Enable the CAN module
    canModule->MCR.B.SOFT_RST = 1U;                 // Reset CAN module
    while (canModule->MCR.B.SOFT_RST == 1U) {}      // Waiting for soft reset ready

    canModule->MCR.B.FRZ = 1U;                      // Enable to enter Freeze Mode
    canModule->MCR.B.HALT = 1U;                     // Enter Freeze Mode
    while(canModule->MCR.B.NOT_RDY == 0) {}         // Wait until enter Freeze Mode

    canModule->MCR.B.BCC = 1U;                      // Enable individual RX mailbox/FIFO masking
    canModule->MCR.B.MAXMB = 31U;                   // Maximum 32 mailboxes

    canModule->CTRL.R = 0x04DB0086;                 // Configure for 40MHz OSC, 500kbit/s

    // Clear memory from message buffer 0 to 31
    for (i = 0; i < 32U; i++)
    {
        canModule->MB[i].MSG_CS.R = *(msgCs + i);
        canModule->MB[i].MSG_ID.R = *(msgId + i);
        for (j = 0; j < 2U; j++)
        {
            canModule->MB[i].DATA.W[j] = 0;
        }
    }

    // Config individual filer mask, default setting is only filter 11-bit message ID
    for (i = 0; i < 32U; i++)
    {
        canModule->RXIMR[i].R = *(filterMask + i);
    }

    canModule->IMRL.R = mbInterrupt;                // Enable interrupt for mailboxes

    canModule->MCR.B.HALT = 0U;                     // Exit Freeze Mode
    canModule->MCR.B.FRZ = 0U;                      // Disable to enter Freeze Mode

    // Await synchronization (delay)
    for (i = 0; i < 255; i++)
    {
    }

    while(canModule->MCR.B.NOT_RDY == 1U) {}
}

/****************************************************************
 * Function for getting RX mailbox interrupt flag
 *
 * @para[in] canModule:         CAN module address (CAN_0 or CAN_1)
 * @para[in] mb:                CAN mailbox number
 ****************************************************************/
static tBool can_getRxMbIntFlag_MPC564xL(volatile FLEXCAN_tag* canModule, uint8_t mb)
{
    return (tBool)(canModule->IFLAG1.R & (0x1U << mb));
}

/****************************************************************
 * Function for clearing RX/TX mailbox interrupt flag
 *
 * @para[in] canModule:         CAN module address (CAN_0 or CAN_1)
 * @para[in] mb:                CAN mailbox number
 ****************************************************************/
static void can_clearMbIntFlag_MPC564xL(volatile FLEXCAN_tag* canModule, uint8_t mb)
{
    canModule->IFLAG1.R = (0x1U << mb);
}

/****************************************************************
 * Function for activating RX mailbox internal lock
 *
 * @para[in] canModule:         CAN module address (CAN_0 or CAN_1)
 * @para[in] mb:                CAN mailbox number
 ****************************************************************/
static void can_activateRxMbLock_MPC564xL(volatile FLEXCAN_tag* canModule, uint8_t mb)
{
    // Activate the internal lock by reading Control and Status register
    (void) canModule->MB[mb].MSG_CS.R;
}

/****************************************************************
 * Function for getting RX mailbox information
 *
 * @para[in] canModule:         CAN module address (CAN_0 or CAN_1)
 * @para[in] mb:                CAN mailbox number
 *
 * @para[out] msgRx:            CAN message RX result
 ****************************************************************/
static void can_getMsg_MPC564xL(volatile FLEXCAN_tag* canModule, uint8_t mb, CAN_DATA_S* msgRx)
{
    // Get message standard ID
    msgRx->id = canModule->MB[mb].MSG_ID.B.STD_ID;

    // Get message length
    msgRx->len = canModule->MB[mb].MSG_CS.B.LENGTH;

    // Get message data
    msgRx->data.word[0] = canModule->MB[mb].DATA.W[0];
    msgRx->data.word[1] = canModule->MB[mb].DATA.W[1];

    msgRx->timeStamp = canModule->MB[mb].MSG_CS.B.TIMESTAMP;
}

/****************************************************************
 * Function for setting TX mailbox information
 *
 * @para[in] msgTx:             CAN message TX to send
 * @para[in] canModule:         CAN module address (CAN_0 or CAN_1)
 * @para[in] mb:                CAN mailbox number
 *
 ****************************************************************/
static void can_sendMsg_MPC564xL(CAN_DATA_S* msgTx, volatile FLEXCAN_tag* canModule, uint8_t mb)
{
    canModule->BUF[mb].MSG_CS.B.IDE = 0;
    canModule->BUF[mb].MSG_CS.B.RTR = 0;

    canModule->BUF[mb].MSG_ID.B.STD_ID = msgTx->id;
    canModule->BUF[mb].MSG_CS.B.LENGTH = msgTx->len;

    canModule->BUF[mb].DATA.W[0] = msgTx->data.word[0];
    canModule->BUF[mb].DATA.W[1] = msgTx->data.word[1];

    // Trigger the one time sending
    canModule->BUF[mb].MSG_CS.B.CODE = CAN_BUF_TX_EN_B;
}

/****************************************************************
 * Function for releasing RX/TX mailbox internal lock
 *
 * @para[in] canModule:         CAN module address (CAN_0 or CAN_1)
 ****************************************************************/
static void can_releaseMbLock_MPC564xL(volatile FLEXCAN_tag* canModule)
{
    // Release the internal lock by reading free running timer
    (void) canModule->TIMER.R;
}

static void can_0_initPin_MPC564xL(void)
{
    SIU.PCR[16].R = 0x0624;
    SIU.PCR[17].R = 0x0100;
    SIU.PSMI[33].R = 0x01;
}

static void can_1_initPin_MPC564xL(void)
{
    SIU.PCR[14].R = 0x0624;
    SIU.PCR[15].R = 0x0100;
    SIU.PSMI[34].R = 0x00;
}

static tBool can_rx_MPC564xL(volatile FLEXCAN_tag* canModule, uint8_t mb, CAN_DATA_S* msgRx)
{
    tBool result;

    if (can_getRxMbIntFlag_MPC564xL(canModule, mb))
    {
        can_activateRxMbLock_MPC564xL(canModule, mb);

        can_getMsg_MPC564xL(canModule, mb, msgRx);

        can_releaseMbLock_MPC564xL(canModule);

        can_clearMbIntFlag_MPC564xL(canModule, mb);

        result = TRUE;
    }
    else
    {
        result = FALSE;
    }
    return result;
}

static void can_tx_MPC564xL(CAN_DATA_S* msgTx, volatile FLEXCAN_tag* canModule, uint8_t mb)
{
    can_sendMsg_MPC564xL(msgTx, canModule, mb);
    can_releaseMbLock_MPC564xL(canModule);
}

/****************************************************************
 *                          C A N   R X                         *
 ****************************************************************/
void CAN_init(void)
{
    const uint32_t msgCs1[32] = {
                            CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,    // 0-3
                            CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,    // 4-7
                            CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,    // 8-11
                            CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,      CAN_BUF_RX_EMPTY_R | CAN_BUF_STANDARD,    // 12-15
                            CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD, // 16-19
                            CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD, // 20-23
                            CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD, // 24-27
                            CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD,   CAN_BUF_TX_INACTIVE_R | CAN_BUF_STANDARD, // 28-31
                          };
    const uint32_t msgId1[32] = {
                            FM_RESERVED_ID,                             FM_RESERVED_ID,                             PT_VCU_TORQUE_MID << 18U,                   0,                                      // 0-3
                            0,                                          1021U << 18U,                               0,                                          0,                                      // 4-7
                            0,                                          0,                                          0,                                          0,                                      // 8-11
                            0,                                          0,                                          0,                                          0,                                      // 12-15
                            0,                                          0,                                          0,                                          0,                                      // 16-19
                            0,                                          0,                                          0,                                          0,                                      // 20-23
                            0,                                          0,                                          0,                                          0,                                      // 24-27
                            0,                                          0,                                          0,                                          0,                                      // 28-31
                          };
    const uint32_t filterMask1[32] = {
                            CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                  // 0-3
                            CAN_RXIMR_STANDARD_ID,                      0x1FF00000,                                 CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                  // 4-7
                            CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                  // 8-11
                            CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                  // 12-15
                            CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                  // 16-19
                            CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                  // 20-23
                            CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                  // 24-27
                            CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                      CAN_RXIMR_STANDARD_ID,                  // 28-31
                               };
    const uint32_t mbInterrupt1 = CAN_RX_INT_EN;

    // CAN 0 initialization

    // CAN 1 initialization
    can_initModule_MPC564xL(&CAN_1, msgCs1, msgId1, filterMask1, mbInterrupt1);
    can_1_initPin_MPC564xL();

    // INT enable
    INTC.PSR[88].R = 0x0C;                     // CAN1 interrupt buffer 0-3 INT enable
    INTC.PSR[89].R = 0x0C;                     // CAN1 interrupt buffer 4-7 INT enable
}

/****************************************************************
 * Function for CAN1 interrupt buffer 0-3
 ****************************************************************/
void CAN_1_buf0_3_isr(void)
{
    CAN_DATA_S msgRx;
    memset(&msgRx, 0, sizeof(msgRx));

    (void) can_rx_MPC564xL(&CAN_1, CAN_BUFFER_NUM_VCU_TORQUE, &msgRx);
}

/****************************************************************
 * Function for CAN1 interrupt buffer 4-7
 ****************************************************************/
void CAN_1_buf4_7_isr(void)
{
    CAN_DATA_S msgRx;
    memset(&msgRx, 0, sizeof(msgRx));

    (void) can_rx_MPC564xL(&CAN_1, CAN_BUFFER_NUM_TEST, &msgRx);
}

/****************************************************************
 *                          C A N   T X                         *
 ****************************************************************/
static void CAN_pack_MCUR0_torque(CAN_DATA_S* const msgTx, uint16_t* const counter)
{
    PT_MCUR0_torque_U MCUR0_torque;

    // Clear the message structure
    CAN_MSG_CLEAR(MCUR0_torque);

    set_PT_mcur0_torqueAvbl(&MCUR0_torque, 0.0F, 0.0F, 0.0F);
    set_PT_mcur0_torqueCmdEcho(&MCUR0_torque, 0.0F, 0.0F, 0.0F);
    set_PT_mcur0_torqueEst(&MCUR0_torque, 0.0F, 0.0F, 0.0F);

    // Update counter. Overflow is desired.
    (*counter)++;
    setRaw_PT_mcur0_torqueCounter(&MCUR0_torque, *counter);

    // Fill the ID and length
    msgTx->id = PT_MCUR0_TORQUE_MID;
    msgTx->len = PT_MCUR0_TORQUE_LEN;

    // Fill the data in words (32 bits)
    msgTx->data.word[0] = MCUR0_torque.rawData[0];
#if (PT_MCUR0_TORQUE_LEN > BYTES_PER_WORD)
    msgTx->data.word[1] = MCUR0_torque.rawData[1];
#endif
}

static void CAN_pack_MCUR0_state(CAN_DATA_S* const msgTx, uint16_t* const counter)
{
    PT_MCUR0_state_U MCUR0_state;

    // Clear the message structure
    CAN_MSG_CLEAR(MCUR0_state);

    setRaw_PT_mcur0_state(&MCUR0_state, 0U);
    set_PT_mcur0_speed(&MCUR0_state, 0.0F, 0.0F, 0.0F);

    // Update counter. Overflow is desired.
    (*counter)++;
    setRaw_PT_mcur0_stateCounter(&MCUR0_state, *counter);

    // Fill the ID and length
    msgTx->id = PT_MCUR0_STATE_MID;
    msgTx->len = PT_MCUR0_STATE_LEN;

    // Fill the data in words (32 bits)
    msgTx->data.word[0] = MCUR0_state.rawData[0];
#if (PT_MCUR0_STATE_LEN > BYTES_PER_WORD)
    msgTx->data.word[1] = MCUR0_state.rawData[1];
#endif
}

static void CAN_pack_MCUR0_state2(CAN_DATA_S* const msgTx, uint16_t* const counter)
{
    PT_MCUR0_state2_U MCUR0_state2;

    // Clear the message structure
    CAN_MSG_CLEAR(MCUR0_state2);

    set_PT_mcur0_vBus(&MCUR0_state2, 0.0F, 0.0F, 0.0F);

    // Counter not used
    (void)(*counter);

    // Fill the ID and length
    msgTx->id = PT_MCUR0_STATE2_MID;
    msgTx->len = PT_MCUR0_STATE2_LEN;

    // Fill the data in words (32 bits)
    msgTx->data.word[0] = MCUR0_state2.rawData[0];
#if (PT_MCUR0_STATE2_LEN > BYTES_PER_WORD)
    msgTx->data.word[1] = MCUR0_state2.rawData[1];
#endif
}

static void CAN_pack_MCUR0_temperature(CAN_DATA_S* const msgTx, uint16_t* const counter)
{
    PT_MCUR0_temperature_U MCUR0_temperature;

    // Clear the message structure
    CAN_MSG_CLEAR(MCUR0_temperature);

    set_PT_mcur0_tempCoolant(&MCUR0_temperature, 0.0F, 0.0F, 0.0F);

    // Counter not used
    (void)(*counter);

    // Fill the ID and length
    msgTx->id = PT_MCUR0_TEMPERATURE_MID;
    msgTx->len = PT_MCUR0_TEMPERATURE_LEN;

    // Fill the data in words (32 bits)
    msgTx->data.word[0] = MCUR0_temperature.rawData[0];
#if (PT_MCUR0_TEMPERATURE_LEN > BYTES_PER_WORD)
    msgTx->data.word[1] = MCUR0_temperature.rawData[1];
#endif
}

void CAN_100Hz(void)
{
    uint8_t i;
    static uint16_t torqueCounter = 0;
    static uint16_t stateCounter = 0;
    static CAN_TX_PACKAGE_S txPackage_100Hz[] = {
            {.mb = 16U, .packFunc = &CAN_pack_MCUR0_torque, .counter = &torqueCounter},
            {.mb = 17U, .packFunc = &CAN_pack_MCUR0_state,  .counter = &stateCounter},
            {.mb = 18U, .packFunc = &CAN_pack_MCUR0_state2, .counter = NULL},
    };

    for (i = 0; i < (sizeof(txPackage_100Hz) / sizeof(CAN_TX_PACKAGE_S)); i++)
    {
        txPackage_100Hz[i].packFunc(&txPackage_100Hz[i].msgTx, txPackage_100Hz[i].counter);

        can_tx_MPC564xL(&txPackage_100Hz[i].msgTx, &CAN_1, txPackage_100Hz[i].mb);
    }
}

void CAN_1Hz(void)
{
    uint8_t i;
    static CAN_TX_PACKAGE_S txPackage_1Hz[] = {
            {.mb = 19U, .packFunc = &CAN_pack_MCUR0_temperature, .counter = NULL},
    };

    for (i = 0; i < (sizeof(txPackage_1Hz) / sizeof(CAN_TX_PACKAGE_S)); i++)
    {
        txPackage_1Hz[i].packFunc(&txPackage_1Hz[i].msgTx, txPackage_1Hz[i].counter);

        can_tx_MPC564xL(&txPackage_1Hz[i].msgTx, &CAN_1, txPackage_1Hz[i].mb);
    }
}
