/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-06-29
* Description: System configurations
***************************************************************************************************/

#include "mpc5643l.h"
#include "sysclk_init.h"
#include "siu_init.h"
#include "System.h"
/*-----------------------------------------------------------*/
/*        FCCU CF Key Register (FCCU_CFK)                    */
/*-----------------------------------------------------------*/
#define FCCU_CFK_KEY                   0x618B7A50                /* Critical fault key */
#define IOFREQMIN 70U
#define IOFREQMAX 3495U
#define SYSCLK 15000000U
#define DIVIDER (((double)0x100000)/SYSCLK)
/*-----------------------------------------------------------*/
/*        FCCU NCF Key Register (FCCU_NCFK)                  */
/*-----------------------------------------------------------*/
#define FCCU_NCFK_KEY                  0xAB3498FE                /* Non-Critical fault key */

static uint16_t calculateIOFREQ (uint16_t frequency)
{
    uint16_t f = (uint16_t)(frequency * DIVIDER);
    if (f < IOFREQMIN)
        f = IOFREQMIN;
    if (f > IOFREQMAX)
        f = IOFREQMAX;
    return f;
}

static void sine_wave_init_564xL (uint16_t frequency, uint8_t IOAMPL)
{
    SGENDIG.CTRL.B.PDS = 0;
    SGENDIG.CTRL.B.IOAMPL = IOAMPL;
    SGENDIG.CTRL.B.LDOS = 0;
    while (SGENDIG.CTRL.B.LDOS) ;      /* Ensure that CTRL[LDOS] = 0 */
    SGENDIG.CTRL.B.IOFREQ = calculateIOFREQ(frequency);;
    SGENDIG.CTRL.B.LDOS = 1;
}

static void DisableWatchdog(void)
{
    SWT.SR.R = 0x0000c520;     /* Write keys to clear soft lock bit */
    SWT.SR.R = 0x0000d928;
    SWT.CR.R = 0x8000010A;     /* Clear watchdog enable (WEN) */

    /* e200 Core Watchdog Timer */

     asm("e_li  %r3, 0");
     asm ("mtspr   340, %r3");

}

void SYS_init(void)
{
    DisableWatchdog();

    {
        uint32_t i;
        uint32_t a[4];
        for (i=0;i<4;i++)
        {
            FCCU.CFK.R = FCCU_CFK_KEY;
            FCCU.CF_S[i].R = 0xFFFFFFFF;
            while (FCCU.CTRL.B.OPS != 0x03) ;/* wait for the completion of the operation */
            a[i]= FCCU.CF_S[i].R;
            (void) a;
        }
    }
    {
        uint32_t i;
        uint32_t b[4];
        for (i=0;i<4;i++)
        {
            FCCU.NCFK.R = FCCU_NCFK_KEY;
            FCCU.NCF_S[i].R = 0xFFFFFFFF;
            while (FCCU.CTRL.B.OPS != 0x03) ;/* wait for the completion of the operation */
            b[i]= FCCU.NCF_S[i].R;
            (void) b;
        }
    }
    /* ----------------------------------------------------------- */
    /*                   System Initialization Functions              */
    /* ----------------------------------------------------------- */

    /* ----------------------------------------------------------- */
    /*                  Reset Determination Goes Here             */
    /* ----------------------------------------------------------- */

    /* ----------------------------------------------------------- */
    /*            Initialize the System Clock, Mode Entry (ME) & CMU             */
    /* ----------------------------------------------------------- */
    sysclk_module_init_fnc();

    /* ----------------------------------------------------------- */
    /*            Initialize the Mode Entry Post Configuration             */
    /* ----------------------------------------------------------- */
    mode_entry_post_config_fnc();
    sine_wave_init_564xL (7550,13);
    siu_init_fnc();
}



