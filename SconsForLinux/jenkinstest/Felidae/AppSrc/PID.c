/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-05-19
* Description: PID algorithm
***************************************************************************************************/

#include "PID.h"
#include "GeneralFunction.h"

void PID_init(pid_S* pid, 
                tFloat paraP, 
                tFloat paraI, 
                tFloat paraD, 
                tFloat feedbackLimitP, 
                tFloat errorLimitI, 
                tFloat feedbackLimitD)
{
    pid->errorIntegral = 0.0F;
    pid->errorLast = 0.0F;
    
    pid->para.p = paraP;
    pid->para.i = paraI;
    pid->para.d = paraD;
    
    pid->limit.feedbackP = feedbackLimitP;
    pid->limit.errorI = errorLimitI;
    pid->limit.feedbackD = feedbackLimitD;
}

void PID_clear(pid_S* pid)
{
    pid->errorIntegral = 0.0F;
    pid->errorLast = 0.0F;
}

tFloat PID_periodic(tFloat real, tFloat target, pid_S* pid)
{
    tFloat error = target - real;
    tFloat feedbackP, feedbackI, feedbackD, feedback;
    
    /*
    * Proportional feedback calculation
    */
    feedbackP = pid->para.p * error;
    feedbackP = GF_SAT(feedbackP, -pid->limit.feedbackP, pid->limit.feedbackP);
    /*
    // Saturate proportional feedback
    if (feedbackP > pid->limit.feedbackP)
    {
        feedbackP = pid->limit.feedbackP;
    }
    else if (feedbackP < -pid->limit.feedbackP)
    {
        feedbackP = -pid->limit.feedbackP;
    }
    else
    {
        // MISRA
    } 
    */
    /*
    * Integral feedback calculation
    */
    pid->errorIntegral += error;
    pid->errorIntegral = GF_SAT(pid->errorIntegral, -pid->limit.errorI, pid->limit.errorI);
    /*
    // Saturate integral error
    if (pid->errorIntegral > pid->limit.errorI)
    {
        pid->errorIntegral = pid->limit.errorI;
    }
    else if (pid->errorIntegral < -pid->limit.errorI)
    {
        pid->errorIntegral = -pid->limit.errorI;
    }
    else
    {
        // MISRA
    }
    */
    feedbackI = pid->para.i * pid->errorIntegral;
    
    /*
    * Derivative feedback calculation
    */
    feedbackD = pid->para.d * (error - pid->errorLast);
    feedbackD = GF_SAT(feedbackD, -pid->limit.feedbackD, pid->limit.feedbackD);
    /*
    // Saturate derivative feedback
    if (feedbackD > pid->limit.feedbackD)
    {
        feedbackD = pid->limit.feedbackD;
    }
    else if (feedbackD < -pid->limit.feedbackD)
    {
        feedbackD = -pid->limit.feedbackD;
    }
    else
    {
        // MISRA
    } 
    */
    feedback = feedbackP + feedbackI + feedbackD;

    return feedback;
}
