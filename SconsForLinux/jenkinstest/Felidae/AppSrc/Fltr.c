/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: patel.reddy@sfmotors.com
* Date: 2017-05-19
* Description: Filter functions
***************************************************************************************************/

#include <math.h>
#include <gflib.h>
#include <mlib.h>

#include "GeneralFunction.h"
#include "Fltr.h"
#include "MCTransform.h"

void FLTR_init(fltr_S* fltr,
                tFloat dT,
                tFloat q,
                tFloat fC1,
                fltrMode_E fltrMode)
{
    tFloat expAT, expBT;
    fltr->dT = dT;
    fltr->fC1 = fC1;
    fltr->q = q;
    fltr->wC1 = FLOAT_2_PI*fC1;
    fltr->fltrMode = fltrMode;

    if (fltrMode == E_FLTR_LOWP)
    {
        /* FIXME: FIR implementation not correct, require future fix
        fltr->a = (1.0F - GFLIB_Sqrt_FLT(1.0F - 4.0F * (q * q))) / (2.0F * (fltr->wC1 / q));
        fltr->b = (fltr->wC1 * fltr->wC1) / fltr->a;
        expAT = expf(-1.0F * fltr->a * fltr->dT);
        expBT = expf(-1.0F * fltr->b * fltr->dT);
        fltr->b1 = (1.0F - expAT)*(1.0F - expBT);
        fltr->b2 = 0.0F;
        fltr->h1 = expAT+expBT;
        fltr->h2 = (expAT*expBT);
        fltr->yN1 = 0.0F;
        fltr->yN2 = 0.0F;
        fltr->xN1 = 0.0F;
        */
        fltr->b1 = 1.0F - expf(-fltr->dT * fltr->wC1);
        fltr->b2 = 0.0F;
        fltr->h1 = expf(-fltr->dT * fltr->wC1);
        fltr->h2 = 0.0F;
        fltr->yN1 = 0.0F;
        fltr->yN2 = 0.0F;
    }
    if (fltrMode == E_FLTR_HIGHP)
    {
        fltr->b1 = 1.0F;
        fltr->b2 = -1.0F;
        fltr->h1 = expf(-fltr->dT * fltr->wC1);
        fltr->h2 = 0.0F;
        fltr->yN1 = 0.0F;
        fltr->yN2 = 0.0F;
    }
}

tFloat FLTR_periodic(tFloat xI, fltr_S* fltr)
{
    tFloat yO;
    yO = (xI * fltr->b1) + (fltr->xN1 * fltr->b2) + (fltr->yN1 * fltr->h1) + (fltr->yN2 * fltr->h2);
    fltr->xN1 = xI;
    fltr->yN2 = fltr->yN1;
    fltr->yN1 = yO;
    return yO;
}
