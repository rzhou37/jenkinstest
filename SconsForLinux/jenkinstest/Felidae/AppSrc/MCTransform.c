/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-05-19
* Description: Motor control transforms
***************************************************************************************************/

#include <math.h>
#include <gflib.h>
#include <mlib.h>

#include "MCTransform.h"
#include "GeneralFunction.h"

void MCT_clarkeTransform(const MCT_motorIabc_S* const iabc, MCT_motorIAlphaBeta_S* const iAB)
{
    iAB->alpha = (FLOAT_2_OVER_3 * iabc->a) - (FLOAT_1_OVER_3 * iabc->b) - (FLOAT_1_OVER_3 * iabc->c);
    iAB->beta = (FLOAT_DIVBY_SQRT3 * iabc->b) - (FLOAT_DIVBY_SQRT3 * iabc->c);
    iAB->zeroAB = (FLOAT_1_OVER_3  * iabc->a) + (FLOAT_1_OVER_3 * iabc->b) + (FLOAT_1_OVER_3 * iabc->c);
}

void MCT_clarkeInvTransform(const MCT_motorVAlphaBeta_S* const vAB, MCT_motorVabc_S* const vabc)
{
    vabc->a = vAB->alpha + vAB->zeroAB;
    vabc->b = -(0.5F * vAB->alpha)
            + (FLOAT_SQRT3_DIVBY_2 * vAB->beta)
            + vAB->zeroAB;
    vabc->c = -(0.5F * vAB->alpha)
            - (FLOAT_SQRT3_DIVBY_2 * vAB->beta)
            + vAB->zeroAB;
}

void MCT_angleTrigonometric(const tFloat angle, MCT_angleTrigonometric_S* const trigonometric)
{
    trigonometric->sin = GFLIB_Sin_FLT(angle,GFLIB_SIN_DEFAULT_FLT);
    trigonometric->cos = GFLIB_Cos_FLT(angle,GFLIB_COS_DEFAULT_FLT);
}

void MCT_parkTransform(const MCT_angleTrigonometric_S* const rotorFluxAngle, const MCT_motorIAlphaBeta_S* const iAB, MCT_motorIdq_S* const idq)
{
    idq->d = (rotorFluxAngle->cos * iAB->alpha)
            + (rotorFluxAngle->sin * iAB->beta);
    idq->q = -(rotorFluxAngle->sin * iAB->alpha)
            + (rotorFluxAngle->cos * iAB->beta);
    idq->zeroDQ = iAB->zeroAB;
}

void MCT_parkInvTransform(const MCT_angleTrigonometric_S* const rotorFluxAngle, const MCT_motorVdq_S* const vdq, MCT_motorVAlphaBeta_S* const vAB)
{
    vAB->alpha = (rotorFluxAngle->cos * vdq->d)
                - (rotorFluxAngle->sin * vdq->q);
    vAB->beta = (rotorFluxAngle->sin * vdq->d)
                + (rotorFluxAngle->cos * vdq->q);
    vAB->zeroAB = vdq->zeroDQ;
}

tFloat MCT_rms(const MCT_motorIabc_S* const iabc)
{
    tFloat irms;
    MCT_motorIAlphaBeta_S iAB;

    MCT_clarkeTransform(iabc, &iAB);
    irms = GFLIB_Sqrt_FLT((iAB.alpha * iAB.alpha) + (iAB.beta * iAB.beta)) / FLOAT_SQUARE_ROOT_2;
    return irms;
}

//wrap input angle (in rad) to a range of [-pi, pi)
tFloat MCT_thetaWrap(tFloat thetaIn)
{
    tFloat thetaOut;

    if ((thetaIn >= -FLOAT_PI) && (thetaIn < FLOAT_PI))
    {
        thetaOut = thetaIn;
    }
    else
    {
        thetaOut = thetaIn - (int32_t)(thetaIn * FLOAT_1_OVER_2_PI) * FLOAT_2_PI;   //wrap angle to [-2*pi,2*pi)
        thetaOut = (thetaOut >= FLOAT_PI)?(thetaOut - FLOAT_2_PI):thetaOut;         //convert [pi,2*pi) to [-pi, 0)
        thetaOut = (thetaOut < -FLOAT_PI)?(thetaOut + FLOAT_2_PI):thetaOut;         //convert (-2*pi, -pi) to (0, pi)
    }
    return thetaOut;
}
