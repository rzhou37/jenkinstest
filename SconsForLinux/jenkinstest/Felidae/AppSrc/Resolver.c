/***************************************************************************************************
* SF Motors Confidential ?Copyright [2017], all rights reserved.
* Author: xiaoxi.sun@sfmotors.com/ruonan.zhou@sfmotors.com
* Date: 2017-07-14
* Description: resolver source file
***************************************************************************************************/
#include <gflib.h>

#include "Resolver.h"
#include "PID.h"
#include "ADC.h"
#include "CTU.h"
#include "MCTransform.h"
#include "GeneralFunction.h"
#include "MotorParameter.h"
#include "MotorControl.h"

#define LPFILTER_500HZ_CFCT     0.9615F
#define LPFILTER_50HZ_CFCT      0.9844F
#define CALAVRGCNTLIM           8U

typedef struct s_resolver
{
    pid_S pidRslvrTracking;
    tFloat rslvrSinFltrOld;
    tFloat rslvrCosFltrOld;
    uint32_t avgSum;
    uint32_t avgSumCntr;
    uint32_t avgRunCntr;
    uint32_t avgRecord;
} res_S;

RES_S RES;
static res_S res;

/* FIXME: Redesign this function*/
static void calcSwgAverage_20kHz(uint32_t* inputArray)
{
    if (res.avgSumCntr < CALAVRGCNTLIM) // calculate res.avg only when res.avgSumCntr reach the limit
    {
        res.avgSum += inputArray[0] + inputArray[1] + inputArray[2] + inputArray[3];
        res.avgSumCntr++;
    }
    else
    {
        uint32_t thisTimeAvg = res.avgSum/(4 * CALAVRGCNTLIM);
        res.avgRecord = (res.avgRecord * (res.avgRunCntr - 1U) + thisTimeAvg) / res.avgRunCntr;
        res.avgSum = 0U;
        res.avgSumCntr = 0U;
        if (res.avgRunCntr == 10000)
        {
            res.avgRunCntr = 1;
        }
        else
        {
            res.avgRunCntr++;
        }
    }
}

static void resolverTrackingObsvr(tFloat* spdEst, tFloat* thetaEst, tFloat rslvrSin, tFloat rslvrCos,
                                  tFloat* thetaElecEst, tFloat* spdMechEst)
{
    tFloat thetaErr;
    tFloat spdElecEst;

    thetaErr = rslvrSin * GFLIB_Cos_FLT(*thetaEst,GFLIB_COS_DEFAULT_FLT) - rslvrCos * GFLIB_Sin_FLT(*thetaEst,GFLIB_SIN_DEFAULT_FLT);
    *spdEst =  PID_periodic(-thetaErr, 0.0, &res.pidRslvrTracking);
    *thetaEst = *thetaEst + *spdEst * SAMPLETIME_20K_HZ;
    *thetaEst = MCT_thetaWrap(*thetaEst);
    *spdMechEst = *spdEst * (1.0/RES_POLE_PAIR);
    spdElecEst = *spdMechEst * MP_POLE_PAIR;
    *thetaElecEst = *thetaElecEst + spdElecEst * SAMPLETIME_20K_HZ;
    *thetaElecEst = MCT_thetaWrap(*thetaElecEst);
}

static tFloat LPFilter(tFloat input, tFloat outputOld, tFloat LPF_GAIN)
{
    tFloat LPFoutput;

    LPFoutput = input * (1 - LPF_GAIN) + outputOld * LPF_GAIN;
    return LPFoutput;
}

void RES_20kHz(void)
{
    tFloat rslvrSinFltr, rslvrCosFltr;
    uint8_t i=0;
    calcSwgAverage_20kHz(CTUS.swgAdcRaw);
    for (i = 0; i < 4; i++)
    {
        RES.rslvrSinInput[i] = ADC_CONVERSION(CTUS.sinAdcRaw[i],2.0F, -1.0F);
        RES.rslvrCosInput[i] = ADC_CONVERSION(CTUS.cosAdcRaw[i],2.0F, -1.0F);
        RES.rslvrSwgInput[i] = ADC_CONVERSION(CTUS.swgAdcRaw[i],4095.0F / res.avgRecord, -1.0F);        // FIXME: Redesign this calculation.
        rslvrSinFltr = LPFilter(RES.rslvrSinInput[i] * RES.rslvrSwgInput[i], res.rslvrSinFltrOld,LPFILTER_500HZ_CFCT);
        res.rslvrSinFltrOld = rslvrSinFltr;
        rslvrCosFltr = LPFilter(RES.rslvrCosInput[i] * RES.rslvrSwgInput[i], res.rslvrCosFltrOld,LPFILTER_500HZ_CFCT);
        res.rslvrCosFltrOld = rslvrCosFltr;
    }
    resolverTrackingObsvr(&RES.spd, &RES.theta, rslvrSinFltr,rslvrCosFltr, &RES.thetaElec, &MC.spdFb);
}

void RES_init(void)
{
    PID_init(&res.pidRslvrTracking,
        314.0F,
        0.9870F,
        0.0F,
        1000.0F,
        1000.0F,
        1000.0F);
    res.avgRecord = 1950U;
}
