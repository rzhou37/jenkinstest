/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-05-19
* Description: Motor control algorithm
***************************************************************************************************/

#ifndef MOTORCONTROL_H_
#define MOTORCONTROL_H_

#include "typedefs.h"
#include "MCTransform.h"

#define MC_CONTROLLERBW             200.0F
#define MC_FLUXOBSTRANSSPDA         200.0F
#define MC_FLUXOBSTRANSSPDB         300.0F
#define MC_FLUXOBSTRANSSPDDIFF      (MC_FLUXOBSTRANSSPDB - MC_FLUXOBSTRANSSPDA)

typedef enum e_MC_mode
{
    E_MC_MODE_OPEN_LOOP_I = 1,
    E_MC_MODE_OPEN_LOOP_V = 2,
    E_MC_MODE_OPEN_LOOP_M = 3,
    E_MC_MODE_CLOSE_LOOP = 4,
} MC_mode_E;

typedef enum e_MC_stateMachine
{
    E_MC_STATE_STANDBY = 0,
    E_MC_STATE_PWMLOW = 1,
    E_MC_STATE_PWMHIGH = 2,
    E_MC_STATE_RUN = 3,
    E_MC_STATE_FAULT = 4,
} MC_stateMachine_E;

typedef struct s_MC
{
    // Torque
    tFloat trqCmd;
    tFloat spdCmd;
    tFloat slipAngle;
    tFloat slipFreqCmd;
    tFloat iMagCmd;                     // Current Magnitude Command

    tFloat estSlip;
    //Speed Mode Enable
    tBool spdModeEnbl;

    // ADC
    tFloat ia;
    tFloat ib;
    tFloat dcBusV;
    tFloat spdFb;
    tFloat theta_er;
    tFloat iRms;

    // Resolver
    tFloat rotorFluxAngle;
    tFloat resolverOffsetAngle;

    // Motor control
    uint32_t phaseTimeA;
    uint32_t phaseTimeB;
    uint32_t phaseTimeC;

    tFloat openLoopVoltCmd;     		// Open loop voltage command
    tFloat openLoopFreqCmd;     		// Open loop frequency command
    tFloat openLoopModulationIndex;  	// Open loop modulation index command

    MC_mode_E motorCtrlMode;
    MC_stateMachine_E motorControlState; //motor control state machine for initial PWM power up

    MCT_motorVabc_S vabc;
    MCT_motorRotorFluxdq_S rotorFluxEstdq;
    MCT_motorStatorFluxAlphaBeta_S statorFluxEstAB;
    MCT_motorIAlphaBeta_S iEstAB;


    tFloat vMaxOutput;

    // Debug
    tFloat vA;
    tFloat vB;
    tFloat vD;
    tFloat vQ;
} MC_S;

extern MC_S MC;

extern void MC_init(void);
extern void MC_20kHz(void);
extern void MC_10Hz(void);

#endif /* MOTORCONTROL_H_ */
