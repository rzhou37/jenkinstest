/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-05-19
* Description: PID algorithm
***************************************************************************************************/

#ifndef PID_H
#define PID_H

#include "typedefs.h"

typedef struct s_pid
{
    struct 
    {
        tFloat p;
        tFloat i;
        tFloat d;
    } para;
    struct
    {
        tFloat feedbackP;
        tFloat errorI;
        tFloat feedbackD;
    } limit;
    tFloat errorIntegral;
    tFloat errorLast;
} pid_S;

extern void PID_init(pid_S* pid, tFloat paraP, tFloat paraI, tFloat paraD, tFloat feedbackLimitP, tFloat errorLimitI, tFloat feedbackLimitD);           
extern void PID_clear(pid_S* pid);
extern tFloat PID_periodic(tFloat real, tFloat target, pid_S* pid);
                
#endif /* PID_H */
