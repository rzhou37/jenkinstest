/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: ruonan.zhou@sfmotors.com
* Date: 2017-06-07
* Description: Motor parameters
***************************************************************************************************/

#ifndef MOTORPARAMETER_H_
#define MOTORPARAMETER_H_
#include "typedefs.h"

#define EVPM                false


#if EVPM
#define MP_RS               0.31F
#define MP_LS               0.00038F
#define MP_LAMBDA_PM        0.0043F
#define MP_JP               0.000007F
#define MP_POLENUM          6U
#define MP_LEQ              0.00038F
#define MP_REQ              0.31F
#define MP_TAU              0.00119F
#else
#define MP_RS               0.0107F
#define MP_JP               0.2120F
#define MP_BP               0.0001F
#define MP_POLENUM          4U
#define MP_LS               0.0028355F
#define MP_LEQ              0.00009027F
#define MP_REQ              0.9789F
#define MP_TAU              0.000092219F
#define MP_SIGMA            0.0318356F
#define MP_ROTORTIMECONST   0.9997955F
#define MP_STATORTIMECONST  0.982088F
#define MP_STATORREQ        0.0163154F
#endif

#define MP_RR               0.0058F
#define MP_LM               0.00279F
#define MP_LLR              0.0000455F
#define MP_LR               0.0028355F
#define MP_LLS              0.0000355F
#define MP_RATEDFLUX        0.04000F
#define MP_POLE_PAIR        (MP_POLENUM / 2U)

#endif /* MOTORPARAMETER_H_ */
