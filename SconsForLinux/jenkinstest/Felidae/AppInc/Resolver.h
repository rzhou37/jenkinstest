/***************************************************************************************************
* SF Motors Confidential ?Copyright [2017], all rights reserved.
* Author: xiaoxi.sun@sfmotors.com/ruonan.zhou@sfmotors.com
* Date: 2017-07-14
* Description: resolver header file
***************************************************************************************************/

#ifndef RESOLVER_H_
#define RESOLVER_H_

#include "typedefs.h"

#define RES_POLE_NUM            8U
#define RES_POLE_PAIR           (RES_POLE_NUM / 2U)

typedef struct s_RESOLVER
{
    tFloat rslvrCosInput[4];
    tFloat rslvrSinInput[4];
    tFloat rslvrSwgInput[4];
    tFloat spd;
    tFloat theta;
    tFloat thetaElec;
}RES_S;

extern RES_S RES;

extern void RES_init(void);
extern void RES_20kHz(void);
#endif /* RESOLVER_H_ */
