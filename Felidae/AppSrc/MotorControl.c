/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-05-19
* Description: Motor control algorithm
***************************************************************************************************/

#include <math.h>
#include <gflib.h>
#include <mlib.h>

#include "GeneralFunction.h"
#include "PID.h"
#include "MCTransform.h"
#include "MotorControl.h"
#include "Fltr.h"
#include "PWM.h"
#include "Resolver.h"
#include "MotorParameter.h"
#include "ADC.h"
#include "CTU.h"
#include "GPIO.h"

// Max. voltage available
#define VD_PHASE_MAX_M                          0.907F
#define VQ_PHASE_MAX_M                          0.907F

#define GATEDRV_POWER_UP_PERIOD                 5000U
#define CURRENT_OFFSET_LEARN_SHIFT              16U
#define CURRENT_OFFSET_LEARN_PERIOD             (1UL << CURRENT_OFFSET_LEARN_SHIFT)
#define CURRENT_OFFSET_BOUND                    50U

// FIXME: Move to CurrentSensor.h

#define CS_IA_ZERO_RAW                          2067U         // ADC raw data when zero current
#define CS_IB_ZERO_RAW                          2067U         // ADC raw data when zero current
#define CS_IA_HALF_RANGE_RAW                    1829U          // Half of ADC raw data range
#define CS_IB_HALF_RANGE_RAW                    1829U          // Half of ADC raw data range

#define CS_IA_MAX                               1200.0F         // Maximum current value
#define CS_IB_MAX                               1200.0F         // Maximum current value

#define DCBUS_VOL_RANGE                         1600.0F
#define DCBUS_VOL_OFFSET                        0.0F
#define DCBUS_VOLTAGE_OPENLOOP                  50.0F
#define VOL_CMD_OPENLOOP                        5.0F
#define FREQ_CMD_OPENLOOP                       0.0F
#define DEAD_TIME                               3U

#define SLIP_FREQ_CMD                           0.0F
#define SPD_SLEWRATE                            5.0F
#define SPD_SLEW_INPUT_CMD                      20.0F
#define IMAG_SLEWRATE                           1.0F
#define IMAG_SLEW_INPUT_CMD                     5.0F

#define DEAD_TIME_1US_PERIOD                    120U
#define DEAD_TIME_CAL(val)                      ((val) * DEAD_TIME_1US_PERIOD)

typedef struct s_mc
{
    pid_S pidId;
    pid_S pidIq;
    pid_S pidSpd;
    MCT_motorVdq_S vdq;
    fltrLP fltrLPA2kHz;
    fltrLP fltrLPB2kHz;
    fltrLP fltrLPC2kHz;
    fltrHP fltrHPFluxA30Hz;
    fltrHP fltrHPFluxB30Hz;
    pid_S pidIdObs;
    pid_S pidIqObs;
    uint32_t gateDrvPwrUpCnt;
    MCT_motorVabc_S vabcOld;

    uint32_t currentOffsetLearnCnt;
    uint32_t iaOffsetSumRaw;
    uint32_t ibOffsetSumRaw;
    int16_t iaOffsetRaw;
    int16_t ibOffsetRaw;
} mc_S;

MC_S MC;

static mc_S mc;

static void currentControllerParaInit(tFloat* Kp, tFloat* Ki)
{
    tFloat z = expf(-FLOAT_2_PI * MC_CONTROLLERBW * SAMPLETIME_20K_HZ);
    tFloat sysPole = expf(-SAMPLETIME_20K_HZ / MP_TAU);
    tFloat K = (1 - z) * MP_REQ / (1 - sysPole);

    *Kp = K * sysPole;
    *Ki = K - *Kp;
}

static void openLoopVM(uint32_t* counter, MCT_angleTrigonometric_S* angleTri)
{
    uint32_t counterLimit;
    tFloat angleCmd;

    // Frequency divider
    if (MC.openLoopFreqCmd == 0.0F)
    {
        angleCmd = 0.0F;
    }
    else
    {
        counterLimit = GF_FREQ_DIVIDER(FREQ_20K_HZ, MC.openLoopFreqCmd);
        if (*counter >= counterLimit)
        {
            *counter = 0U;
        }
        (*counter)++;

        angleCmd = MCT_thetaWrap(((tFloat)*counter / (tFloat)counterLimit) * FLOAT_2_PI);
    }

    MCT_angleTrigonometric(angleCmd, angleTri);
}

static void regulateId(tFloat* idFdb, tFloat* idCmd)
{
    // PI Current Regulator
    mc.vdq.d = PID_periodic(*idFdb, *idCmd, &mc.pidId);
    // Calculate based on limits
    if (mc.vdq.d >= 0)
    {
        mc.vdq.d = GF_MIN(mc.vdq.d, VD_PHASE_MAX_M * MC.vMaxOutput);
    }
    else
    {
        mc.vdq.d = GF_MAX(mc.vdq.d, -VD_PHASE_MAX_M * MC.vMaxOutput);
    }
  }

static void regulateIq(tFloat* iqFdb, tFloat* iqCmd)
{
    // PI Current Regulator
    mc.vdq.q = PID_periodic(*iqFdb, *iqCmd, &mc.pidIq);
    // Calculate based on limits
    if (mc.vdq.q >=0)
    {
        mc.vdq.q = GF_MIN(mc.vdq.q, GFLIB_Sqrt_FLT((VD_PHASE_MAX_M * MC.vMaxOutput * VD_PHASE_MAX_M * MC.vMaxOutput) - (mc.vdq.d * mc.vdq.d)));
    }
    else
    {
        mc.vdq.q = GF_MAX(mc.vdq.q, -GFLIB_Sqrt_FLT((VD_PHASE_MAX_M * MC.vMaxOutput * VD_PHASE_MAX_M * MC.vMaxOutput) - (mc.vdq.d * mc.vdq.d)));
    }
 }

static MCT_motorIdq_S currentCmdCal(tFloat trqCmd, tFloat fluxCmd)
{
    MCT_motorIdq_S idqCmd;
#if EVPM
    idqCmd.d = 0.0F;
    idqCmd.q = trqCmd * FLOAT_4_OVER_3 / MP_POLENUM / MP_LAMBDA_PM;
#else
    idqCmd.d = fluxCmd / MP_LM;
    idqCmd.q = trqCmd * FLOAT_4_OVER_3 * MP_LR / MP_LM / MP_POLENUM / fluxCmd;
#endif
    return idqCmd;
}
#if !EVPM
static MCT_motorRotorFluxdq_S currentBasedRotorFluxObs(MCT_motorIAlphaBeta_S* iAB, tFloat rotorElecAngle, MCT_motorRotorFluxdq_S* rotorFluxEstdqOld)
{
    MCT_motorRotorFluxdq_S rotorFluxEstdq;
    MCT_angleTrigonometric_S angleTri;
    MCT_motorIdq_S idq;

    // Calculate sin/cos for rotor flux angle
    MCT_angleTrigonometric(rotorElecAngle, &angleTri);
    MCT_parkTransform(&angleTri, iAB, &idq);

    rotorFluxEstdq.d = MP_LM * (1 - MP_ROTORTIMECONST) * idq.d + MP_ROTORTIMECONST * rotorFluxEstdqOld->d;
    rotorFluxEstdq.q = MP_LM * (1 - MP_ROTORTIMECONST) * idq.q + MP_ROTORTIMECONST * rotorFluxEstdqOld->q;

    return rotorFluxEstdq;
}

static MCT_motorIAlphaBeta_S currentObs(MCT_motorIAlphaBeta_S* iAB, MCT_motorIAlphaBeta_S* iEstAB_old, MCT_motorRotorFluxAlphaBeta_S* rotorFluxEstAB, MCT_motorVAlphaBeta_S* vAB, tFloat rotorSpeedElec)
{
    MCT_motorIAlphaBeta_S iEstAB;
    MCT_motorVAlphaBeta_S vABsum;

    const tFloat vA_pid = PID_periodic(iEstAB_old->alpha, iAB->alpha, &mc.pidIdObs);
    const tFloat vB_pid = PID_periodic(iEstAB_old->beta, iAB->beta, &mc.pidIqObs);

    vABsum.alpha = vA_pid + vAB->alpha + MP_LM * MP_RR / MP_LR / MP_LR * rotorFluxEstAB->alpha + MP_LM /MP_LR * rotorFluxEstAB->beta * rotorSpeedElec;
    vABsum.beta =  vB_pid + vAB->beta + MP_LM * MP_RR / MP_LR / MP_LR * rotorFluxEstAB->beta - MP_LM /MP_LR * rotorFluxEstAB->alpha * rotorSpeedElec;

    iEstAB.beta = vABsum.beta / MP_STATORREQ * (1.0F - MP_STATORTIMECONST) + iEstAB_old->beta * MP_STATORTIMECONST;
    iEstAB.alpha = vABsum.alpha / MP_STATORREQ * (1.0F - MP_STATORTIMECONST) + iEstAB_old->alpha * MP_STATORTIMECONST;


    return iEstAB;
}

static MCT_motorStatorFluxAlphaBeta_S currentBasedStatorFluxObs(MCT_motorIAlphaBeta_S* iAB, tFloat rotorElecAngle, tFloat rotorSpeedElec, MCT_motorVAlphaBeta_S* vAB_obs)
{
    MCT_motorStatorFluxAlphaBeta_S statorFluxEstAB;
    static MCT_motorRotorFluxAlphaBeta_S rotorFluxEstAB = (MCT_motorRotorFluxAlphaBeta_S){0,0};
    MCT_angleTrigonometric_S angleTri;

    // Calculate sin/cos for rotor flux angle
    MCT_angleTrigonometric(rotorElecAngle, &angleTri);

    MC.rotorFluxEstdq = currentBasedRotorFluxObs(iAB, rotorElecAngle, &MC.rotorFluxEstdq);
    MCT_parkInvTransform(&angleTri, &MC.rotorFluxEstdq , &rotorFluxEstAB);

    statorFluxEstAB.alpha = iAB->alpha * MP_SIGMA * MP_LS + rotorFluxEstAB.alpha * MP_LM/MP_LR;
    statorFluxEstAB.beta = iAB->beta * MP_SIGMA * MP_LS + rotorFluxEstAB.beta * MP_LM/MP_LR;

    MC.iEstAB = currentObs(iAB, &MC.iEstAB, &rotorFluxEstAB, vAB_obs, rotorSpeedElec);

    return statorFluxEstAB;
}

static MCT_motorStatorFluxAlphaBeta_S HPFltr_compensation(fltr_S* fltr, MCT_motorStatorFluxAlphaBeta_S* inputFlux, tFloat elecFreq)
{
    MCT_motorStatorFluxAlphaBeta_S statorFluxAfterMagComp;
    MCT_motorStatorFluxAlphaBeta_S statorFluxAfterAngleComp;
    tFloat zReal, zImag, zImag_sqr, xFuncReal, xFuncImag, xFuncInvMag, timeCnst;

    if (elecFreq == 0)
    {
        statorFluxAfterAngleComp.alpha = inputFlux->alpha;
        statorFluxAfterAngleComp.beta = inputFlux->beta;

    }
    else
    {
        timeCnst = MCT_thetaWrap(elecFreq * SAMPLETIME_20K_HZ);
        zReal = GFLIB_Cos_FLT(timeCnst, GFLIB_COS_DEFAULT_FLT);
        zImag = GFLIB_Sin_FLT(timeCnst, GFLIB_SIN_DEFAULT_FLT);
        zImag_sqr = zImag * zImag;
        xFuncReal = ((zReal - fltr->h1) * (zReal - 1) + zImag_sqr)
                                / ((zReal - 1) * (zReal - 1) + zImag_sqr);
        xFuncImag = zImag * (fltr->h1 - 1)
                                / ((zReal - 1) * (zReal - 1) + zImag_sqr);
        xFuncInvMag = GFLIB_Sqrt_FLT(xFuncReal * xFuncReal + xFuncImag * xFuncImag);

        statorFluxAfterMagComp.alpha = inputFlux->alpha * xFuncInvMag;
        statorFluxAfterMagComp.beta = inputFlux->beta * xFuncInvMag;

        MCT_angleTrigonometric_S angleTri;

        angleTri.cos = xFuncReal / xFuncInvMag;
        angleTri.sin = xFuncImag / xFuncInvMag;

        statorFluxAfterAngleComp.alpha = (statorFluxAfterMagComp.alpha * angleTri.cos)
                                            - (statorFluxAfterMagComp.beta * angleTri.sin);
        statorFluxAfterAngleComp.beta =  + (statorFluxAfterMagComp.alpha *  angleTri.sin)
                                           +  (statorFluxAfterMagComp.beta * angleTri.cos);
    }

    return statorFluxAfterAngleComp;
}

static MCT_motorStatorFluxAlphaBeta_S voltageBasedStatorFluxObs(MCT_motorIAlphaBeta_S* iAB, MCT_motorVAlphaBeta_S* vAB, tFloat elecFreq)
{
    static MCT_motorStatorFluxAlphaBeta_S statorFluxEstAB, statorFluxDiffHPFAB, statorFluxEstAftCompAB;

    statorFluxDiffHPFAB.alpha = FLTR_periodic(vAB->alpha - MP_RS * iAB->alpha, &mc.fltrHPFluxA30Hz);
    statorFluxDiffHPFAB.beta = FLTR_periodic(vAB->beta - MP_RS * iAB->beta, &mc.fltrHPFluxB30Hz);

    statorFluxEstAB.alpha += statorFluxDiffHPFAB.alpha * SAMPLETIME_20K_HZ;
    statorFluxEstAB.beta += statorFluxDiffHPFAB.beta * SAMPLETIME_20K_HZ;

    statorFluxEstAftCompAB = HPFltr_compensation(&mc.fltrHPFluxB30Hz, &statorFluxEstAB, elecFreq);

    return statorFluxEstAftCompAB;
}

static MCT_motorVabc_S dutyToVoltage(tFloat vDC, uint32_t dA, uint32_t dB, uint32_t dC)
{
    MCT_motorVabc_S vabc;

    vabc.a = ((tFloat)(dA) / (tFloat)PWM_HALF_PERIOD - 0.5F) * vDC;
    vabc.b = ((tFloat)(dB) / (tFloat)PWM_HALF_PERIOD - 0.5F) * vDC;
    vabc.c = ((tFloat)(dC) / (tFloat)PWM_HALF_PERIOD - 0.5F) * vDC;

    return vabc;
}

static MCT_motorStatorFluxAlphaBeta_S blendModelFluxObs(MCT_motorStatorFluxAlphaBeta_S* iBasedStatorFluxEstAB, MCT_motorStatorFluxAlphaBeta_S* vBasedStatorFluxEstAB, tFloat rotorSpeedElec)
{
    MCT_motorStatorFluxAlphaBeta_S blendStatorFluxEstAB;

    if (MLIB_Abs_FLT(rotorSpeedElec) < MC_FLUXOBSTRANSSPDA)
       {
        blendStatorFluxEstAB = *iBasedStatorFluxEstAB;
       }
       else
       {
       if (MLIB_Abs_FLT(rotorSpeedElec) > MC_FLUXOBSTRANSSPDB)
       {
           blendStatorFluxEstAB = *vBasedStatorFluxEstAB;
       }
       else
       {
           blendStatorFluxEstAB.alpha = iBasedStatorFluxEstAB->alpha *
                               ((MLIB_Abs_FLT(rotorSpeedElec) - MC_FLUXOBSTRANSSPDA) / MC_FLUXOBSTRANSSPDDIFF)
                              + vBasedStatorFluxEstAB->alpha *
                              ((MC_FLUXOBSTRANSSPDB - MLIB_Abs_FLT(rotorSpeedElec)) / MC_FLUXOBSTRANSSPDDIFF);
           blendStatorFluxEstAB.beta = iBasedStatorFluxEstAB->beta *
                               ((MLIB_Abs_FLT(rotorSpeedElec) - MC_FLUXOBSTRANSSPDA) / MC_FLUXOBSTRANSSPDDIFF)
                              + vBasedStatorFluxEstAB->beta *
                              ((MC_FLUXOBSTRANSSPDB - MLIB_Abs_FLT(rotorSpeedElec)) / MC_FLUXOBSTRANSSPDDIFF);
       }
       }

       return blendStatorFluxEstAB;
}

static MCT_motorStatorFluxAlphaBeta_S statorFluxObs(MCT_motorIAlphaBeta_S* iAB, tFloat rotorElecAngle, uint32_t dA, uint32_t dB, uint32_t dC, tFloat rotorSpeedElec, tFloat estSlip)
{
    MCT_motorStatorFluxAlphaBeta_S statorFluxEstAB, iBasedStatorFluxEstAB, vBasedStatorFluxEstAB;
    MCT_motorVAlphaBeta_S vAB_obs;

    MC.vabc = dutyToVoltage(MC.dcBusV, dA, dB, dC);
    MCT_clarkeTransform(&mc.vabcOld, &vAB_obs);

    iBasedStatorFluxEstAB = currentBasedStatorFluxObs(iAB, rotorElecAngle, rotorSpeedElec, &vAB_obs);
    vBasedStatorFluxEstAB = voltageBasedStatorFluxObs(&MC.iEstAB, &vAB_obs, rotorSpeedElec + estSlip);

    statorFluxEstAB = blendModelFluxObs(&iBasedStatorFluxEstAB, &vBasedStatorFluxEstAB, rotorSpeedElec);

    mc.vabcOld = MC.vabc;

    return statorFluxEstAB;
}
#endif

static void runStateMain(void)
{
    MCT_motorIabc_S iabc, iabcfltr;
    MCT_motorIAlphaBeta_S iAB;
    MCT_motorIdq_S idqFdb, idqFltr;
    MCT_motorIdq_S idqCmd;
    MCT_angleTrigonometric_S angleTri;
    tFloat estSlip, angleCmd;

    MCT_motorVAlphaBeta_S vAB;
    MC.vMaxOutput = FLOAT_2_DIVBY_PI * MC.dcBusV;  //FIXME: need 200Hz filter
    // Calculate phase A/B current
    iabc.a = MC.ia;
    iabc.b = MC.ib;
    iabc.c = - MC.ia - MC.ib;

    iabcfltr.a = FLTR_periodic(iabc.a, &mc.fltrLPA2kHz);
    iabcfltr.b = FLTR_periodic(iabc.b, &mc.fltrLPB2kHz);
    iabcfltr.c = FLTR_periodic(iabc.c, &mc.fltrLPC2kHz);
    MC.iRms = MCT_rms(&iabc);

    MC.theta_er = MCT_thetaWrap(RES.thetaElec - MC.resolverOffsetAngle);
    // Clarke transforms
    MCT_clarkeTransform(&iabc, &iAB);
//    MC.statorFluxEstAB = statorFluxObs(&iAB, MC.theta_er, MC.phaseTimeA, MC.phaseTimeB, MC.phaseTimeC, MC.spdFb * MP_POLE_PAIR, estSlip);
    switch (MC.motorCtrlMode)
    {
        case E_MC_MODE_OPEN_LOOP_V:
        {
            static uint32_t counter = 0U;
            mc.vdq.d = GF_MIN(MC.openLoopVoltCmd, FLOAT_2_DIVBY_PI * MC.dcBusV);
            mc.vdq.q = 0.0F;

            openLoopVM(&counter, &angleTri);
            // Park Transforms
            MCT_parkTransform(&angleTri, &iAB, &idqFdb);

            break;
        }
        case E_MC_MODE_OPEN_LOOP_M:
        {
            static uint32_t counter = 0U;

            mc.vdq.d = GF_SAT(MC.openLoopModulationIndex, 0.0F, 1.0F) * (FLOAT_2_DIVBY_PI * MC.dcBusV);
            mc.vdq.q = 0.0F;
            openLoopVM(&counter, &angleTri);

        	break;
        }
        case E_MC_MODE_OPEN_LOOP_I:
        {
            // Get Current Command
            idqCmd.d = MC.iMagCmd;
            idqCmd.q = 0.0F;

            // Estimate new electrical angle
            MC.slipAngle += MC.slipFreqCmd * (FLOAT_2_PI * SAMPLETIME_20K_HZ);

#if EVPM
            angleCmd = MCT_thetaWrap(MC.slipAngle);
#else
            //angleCmd = MCT_thetaWrap(MC.slipAngle + MC.theta_er);
            angleCmd = MCT_thetaWrap(0.0F);
#endif
            // Calculate sin/cos for rotor flux angle
            MCT_angleTrigonometric(angleCmd, &angleTri);

            // Park Transforms
            MCT_parkTransform(&angleTri, &iAB, &idqFdb);

            // Filtering
            //idqFltr.d = idqFdb.d;
            //idqFltr.q = idqFdb.q;

            // PI Current Regulator
            regulateId(&idqFdb.d, &idqCmd.d);
            regulateIq(&idqFdb.q, &idqCmd.q);
            //mc.vdq.q = 0.0F;


            break;
        }
        case E_MC_MODE_CLOSE_LOOP:
        {
            // Calculate ID/IQ command
            const tFloat spdModeTrqCmd = PID_periodic(MC.spdFb, MC.spdCmd, &mc.pidSpd);
            const tFloat trqCmd = MC.spdModeEnbl? (spdModeTrqCmd) : (MC.trqCmd);

            //Calculate feedforward slip command
            idqCmd = currentCmdCal(trqCmd, MP_RATEDFLUX);
            MC.vQ = trqCmd;
#if EVPM
            MC.rotorFluxAngle  = MCT_thetaWrap(MC.theta_er);
#else
            estSlip = idqCmd.q / idqCmd.d * MP_RR / MP_LR;
            MC.slipAngle = MCT_thetaWrap(MC.slipAngle + estSlip * SAMPLETIME_20K_HZ);
            MC.rotorFluxAngle  = MCT_thetaWrap(MC.slipAngle + MC.theta_er);
#endif

            // Calculate sin/cos for rotor flux angle
            MCT_angleTrigonometric(MC.rotorFluxAngle, &angleTri);

            MCT_parkTransform(&angleTri, &iAB, &idqFdb);

            // PI current regulator
            regulateId(&idqFdb.d, &idqCmd.d);   // calculate vdq.d
            regulateIq(&idqFdb.q, &idqCmd.q);   // calculate vdq.q

            break;
        }
        default:
            break;
    }

    MC.vA = idqFdb.q;
    MC.vB = idqFdb.d;
    MC.vD = mc.vdq.d;

    // Park inverse transforms
    MCT_parkInvTransform(&angleTri, &mc.vdq, &vAB); // FIXME:needs to be tested
    PWM_svmGenerateor(&vAB, PWM_HALF_PERIOD, MC.dcBusV,
            &MC.phaseTimeA, &MC.phaseTimeB, &MC.phaseTimeC);
}

static void MC_slewCmd(tFloat slewRate, tFloat inputCmd, tFloat* outputCmd)
{
   if (inputCmd >= 0.0F)
   {
       *outputCmd = *outputCmd + slewRate * SAMPLETIME_20K_HZ;
       *outputCmd = GF_MIN(*outputCmd, inputCmd);
   }
   else
   {
       *outputCmd = *outputCmd - slewRate * SAMPLETIME_20K_HZ;
       *outputCmd = GF_MAX(*outputCmd, inputCmd);
   }
}

static tBool currentOffsetLearnDone(int16_t iaInputRaw, int16_t ibInputRaw)
{
    tBool currentOffsetLearnDoneFlag;
    uint16_t iaOffsetRaw, ibOffsetRaw;

    if (mc.currentOffsetLearnCnt < CURRENT_OFFSET_LEARN_PERIOD)
    {
        mc.currentOffsetLearnCnt++;
        mc.iaOffsetSumRaw += iaInputRaw;
        mc.ibOffsetSumRaw += ibInputRaw;
        currentOffsetLearnDoneFlag = FALSE;
    }
    else
    {
        iaOffsetRaw = mc.iaOffsetSumRaw >> CURRENT_OFFSET_LEARN_SHIFT;
        ibOffsetRaw = mc.ibOffsetSumRaw >> CURRENT_OFFSET_LEARN_SHIFT;

        //FIXME: need to set an alert signal when the offset learn is off the bound.
        mc.iaOffsetRaw = GF_RANGE_CHECK(iaOffsetRaw, CS_IA_ZERO_RAW - CURRENT_OFFSET_BOUND, CS_IA_ZERO_RAW + CURRENT_OFFSET_BOUND) ? iaOffsetRaw : CS_IA_ZERO_RAW;
        mc.ibOffsetRaw = GF_RANGE_CHECK(ibOffsetRaw, CS_IB_ZERO_RAW - CURRENT_OFFSET_BOUND, CS_IB_ZERO_RAW + CURRENT_OFFSET_BOUND) ? ibOffsetRaw : CS_IB_ZERO_RAW;
        currentOffsetLearnDoneFlag = TRUE;
    }

    return currentOffsetLearnDoneFlag;
}

void MC_init(void)
{
    tFloat Kp;
    tFloat Ki;
    currentControllerParaInit(&Kp, &Ki);
    PID_init(&mc.pidId,
            0.1977F,
            0.0123F,
            0.0F,
            1000.0F,
            2000.0F,
            1000.0F);
    PID_init(&mc.pidIq,
            0.1977F,
            0.0123F,
            0.0F,
            1000.0F,
            2000.0F,
            1000.0F);
    PID_init(&mc.pidSpd,
            0.1518F,
            0.00001F,
            0.0F,
            1000.0F,
            1000.0F,
            1000.0F);
    FLTR_init(&mc.fltrLPA2kHz,
            0.0001F,
            0.3F,
            300.0F,
            E_FLTR_LOWP);
    FLTR_init(&mc.fltrLPB2kHz,
            0.0001F,
            0.3F,
            300.0F,
            E_FLTR_LOWP);
    FLTR_init(&mc.fltrLPC2kHz,
            0.0001F,
            0.3F,
            300.0F,
            E_FLTR_LOWP);
    FLTR_init(&mc.fltrHPFluxA30Hz,
            0.0001F,
            0.3F,
            30.0F,
            E_FLTR_HIGHP);
    FLTR_init(&mc.fltrHPFluxB30Hz,
            0.0001F,
            0.3F,
            30.0F,
            E_FLTR_HIGHP);
    PID_init(&mc.pidIdObs,
            Kp,
            Ki,
            0.0F,
            1000.0F,
            1000.0F,
            1000.0F);
    PID_init(&mc.pidIqObs,
            Kp,
            Ki,
            0.0F,
            1000.0F,
            1000.0F,
            1000.0F);
    // Need to be removed
    MC.dcBusV = 50.0F;
    MC.slipAngle = 0.0F;
    MC.rotorFluxEstdq = (MCT_motorRotorFluxdq_S){0.0F, 0.0F ,0.0F};
    MC.iEstAB = (MCT_motorIAlphaBeta_S){0.0F, 0.0F, 0.0F};
    MC.vabc = (MCT_motorVabc_S){0.0F, 0.0F, 0.0F};
    MC.motorControlState = E_MC_STATE_STANDBY;
    mc.gateDrvPwrUpCnt = 0;
    MC.resolverOffsetAngle = 0.0F;
    mc.vabcOld = (MCT_motorVabc_S){0.0F, 0.0F, 0.0F};
    mc.currentOffsetLearnCnt = 0U;
    mc.iaOffsetSumRaw = 0U;
    mc.ibOffsetSumRaw = 0U;
    mc.iaOffsetRaw = CS_IA_ZERO_RAW;
    mc.ibOffsetRaw = CS_IB_ZERO_RAW;
    MC.theta_er = 0.0F;
}

void MC_20kHz(void)
{
    tBool currentOffsetLearnDoneFlag;

    MC.ia = (CTUS.iaAdcRaw - mc.iaOffsetRaw) * (tFloat) (CS_IA_MAX / CS_IA_HALF_RANGE_RAW);
    MC.ib = (CTUS.ibAdcRaw - mc.ibOffsetRaw) * (tFloat) (CS_IB_MAX / CS_IB_HALF_RANGE_RAW);

    MC.dcBusV = ADC_CONVERSION(CTUS.vDCBusAdcRaw, DCBUS_VOL_RANGE, DCBUS_VOL_OFFSET);
    RES_20kHz();

    MC.dcBusV = DCBUS_VOLTAGE_OPENLOOP;
    MC.motorCtrlMode = E_MC_MODE_CLOSE_LOOP;
    MC.spdModeEnbl = TRUE;
    MC.slipFreqCmd = SLIP_FREQ_CMD;
    MC.openLoopFreqCmd = FREQ_CMD_OPENLOOP;
    MC.openLoopVoltCmd = VOL_CMD_OPENLOOP;

    switch (MC.motorControlState)
    {
        case E_MC_STATE_STANDBY:
        {
            currentOffsetLearnDoneFlag = currentOffsetLearnDone(CTUS.iaAdcRaw, CTUS.ibAdcRaw);
            MC.motorControlState = (currentOffsetLearnDoneFlag)? E_MC_STATE_PWMLOW : E_MC_STATE_STANDBY ;
            break;
        }
        case E_MC_STATE_PWMLOW:
        {
            MC.motorControlState = (mc.gateDrvPwrUpCnt == GATEDRV_POWER_UP_PERIOD)? E_MC_STATE_PWMHIGH : E_MC_STATE_PWMLOW;
            mc.gateDrvPwrUpCnt = (mc.gateDrvPwrUpCnt == GATEDRV_POWER_UP_PERIOD) ? (0) : (mc.gateDrvPwrUpCnt + 1);
            MC.phaseTimeA = 0U;
            MC.phaseTimeB = 0U;
            MC.phaseTimeC = 0U;
#if !EVPM
            MC.resolverOffsetAngle = RES.thetaElec;
#endif
            break;
        }
        case E_MC_STATE_PWMHIGH:
        {
            MC.motorControlState = (mc.gateDrvPwrUpCnt == GATEDRV_POWER_UP_PERIOD)? E_MC_STATE_RUN: E_MC_STATE_PWMHIGH;
            mc.gateDrvPwrUpCnt = (mc.gateDrvPwrUpCnt == GATEDRV_POWER_UP_PERIOD) ? (0) : (mc.gateDrvPwrUpCnt + 1);
            MC.phaseTimeA = PWM_HALF_PERIOD;
            MC.phaseTimeB = PWM_HALF_PERIOD;
            MC.phaseTimeC = PWM_HALF_PERIOD;
            break;
        }
        case E_MC_STATE_RUN:
        {
            MC_slewCmd(SPD_SLEWRATE, SPD_SLEW_INPUT_CMD, &MC.spdCmd);
            MC_slewCmd(IMAG_SLEWRATE, IMAG_SLEW_INPUT_CMD, &MC.iMagCmd);
            runStateMain();
            break;
        }
        case E_MC_STATE_FAULT:
        {
            //FIXME: need to include diagnostic results in this case
            break;
        }
        default:
            break;
    }
    PWM_update(0, 1, 2, 10000, 100, MC.phaseTimeA, MC.phaseTimeB, MC.phaseTimeC, 1, 0, 0, DEAD_TIME_CAL(DEAD_TIME), 0, 0, 0);
   //   PWM_update(0, 1, 2, 10000, 100, 7000U, 7000U, 7000U, 1, 0, 0, DEAD_TIME_CAL(DEAD_TIME), 0, 0, 0);
}

void MC_10Hz(void)
{
    tFloat Kp;
    tFloat Ki;
    currentControllerParaInit(&Kp, &Ki);
    PID_init(&mc.pidId,
            Kp,
            Ki,
            0.0F,
            1000.0F,
            1000.0F,
            1000.0F);
    PID_init(&mc.pidIq,
            Kp,
            Ki,
            0.0F,
            1000.0F,
            1000.0F,
            1000.0F);
    PID_init(&mc.pidSpd,
            20.0F,
            0.01F,
            0.0F,
            1000.0F,
            1000.0F,
            1000.0F);
}
