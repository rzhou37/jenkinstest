/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-06-29
* Description: CTU configuration and interrupts.
***************************************************************************************************/

#include "CTU_564xL_library.h"
#include "GPIO.h"
#include "MotorControl.h"
#include "GeneralFunction.h"
#include "CTU.h"
#include "ADC.h"

#define FIFO0_SAMPLE_NUM    3U
#define FIFO1_SAMPLE_NUM    12U
#define FIFO0_INT_TH        12U
#define FIFO0               0U
#define FIFO1               1U
#define FIFO_ADC0_AN1       0x11U
#define FIFO_ADC1_AN1       0x01U
#define FIFO_ADC1_AN4       0x04U
#define FIFO_ADC0_AN2       0x12U
#define FIFO_ADC1_AN0       0x00U
#define FIFO_ADC1_AN2       0x2U

#define FTR_NXP_GATE_DRIVE  false

CTU_S CTUS;

void CTU_init(void)
{
    // Enable trigger 0
    CTU.CTUIR.B.T0_I = 1;
    // Enable FIFO 0 with threshold 4
    CTU.TH1.B.THRESHOLD0 = FIFO0_INT_TH;            //FIFO0 threshold value
    CTU.FCR.B.FIFO_OVERFLOW_EN0 = 1;                //FIFO0 threshold overflow interrupt enable
    //INTC.PSR[206].R = 0x0C;                       // CTU ADC command INT enable
    //INTC.PSR[194].R = 0x0C;                       // CTU trigger 0 INT enable
    INTC.PSR[202].R = 0x0C;                         // CTU FIFO0 INT enable
    // Config pin 49 as CTU external trigger pin
    ctu_init_pcr_out_564xL(49, 255);
    {
        // No delay on the triggers.
        // FIXME: remove static keywords
        static uint16_t triggerDelay[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
        static uint32_t TriggerOutput[2] = { 97, 0 };
        static uint16_t ADCcommands[24] =
        {
            49153, 32801,32804,33794,33826,33824,   // 49153:FIFO0,ADC0_AN1
                                                    // 32801:FIFO0,ADC1_AN1
                                                    // 32804:FIFO0,ADC1_AN4
                                                    // 33794:FIFO1,ADC0_AN2
                                                    // 33826:FIFO1,ADC1_AN2
                                                    // 33824:FIFO1,ADC1_AN0
            16384,0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0
        };
        static uint32_t ADC_CmdIndex[2] = { 0, 0 };
        ctu_init_564xL (0, 256, TriggerOutput, 0, 20, triggerDelay, ADC_CmdIndex, ADCcommands);
    }
}

static void CTU_fifoRead(uint8_t fifoNum,uint8_t count,uint32_t* taget)
{
    uint8_t i;
    for(i = 0; i < count; i++)
    {
        taget[i] = CTU.FRA[fifoNum].R;
    }
}

void CTU_fifo0_ISR(void)
{
    static tBool phaseShift = TRUE;
    uint8_t i = 0U;
    /*
     * fifo1GroupNum indicate which group the ADC value belongs to.
     * Every 12 sample data belongs to 4 groups.
     * 3 data per group including resolver sin/cos and SWG data.
     */
    uint8_t fifo1GroupNum = 0U;
    uint32_t fifo0Raw[FIFO0_SAMPLE_NUM], fifo1Raw[FIFO1_SAMPLE_NUM];
#if FTR_NXP_GATE_DRIVE
    tBool pinPwmA0 = GPIO_PWM0_A0;                                      // Read PWM0 A0 (high side) GPIO
#endif

    gpo_pin_update_564xl_fnc(86, 1);
    if (phaseShift)
    {
        CTU_fifoRead(FIFO0, FIFO0_INT_TH - FIFO0_SAMPLE_NUM, NULL);     // Read FIFO for the first time to adjust the interrupt phase to the 10kHz PWM.
        phaseShift = FALSE;
    }
    else
    {
        CTU_fifoRead(FIFO0, FIFO0_INT_TH - FIFO0_SAMPLE_NUM, NULL);     // Read the unused data 9 times from FIFO 0
        CTU_fifoRead(FIFO0, FIFO0_SAMPLE_NUM, fifo0Raw);                // Read the useful data 3 times from FIFO 0
        CTU_fifoRead(FIFO1,FIFO1_SAMPLE_NUM,fifo1Raw);                  // Read the useful data 12 times from FIFO 1

#if FTR_NXP_GATE_DRIVE
        if (pinPwmA0 == GPIO_HIGH)                                      // Verify sampling in the middle of PWM A0 high, only for NXP gate drive board
#endif
        {
            for (i = 0; i < FIFO0_SAMPLE_NUM; i++)
            {
                uint8_t fifo0ModuleChannel = (uint8_t) GF_BITS_EXTRACT(fifo0Raw[i], 0x1FU, 16U);
                uint16_t fifo0Data = (uint16_t) GF_BITS_EXTRACT(fifo0Raw[i], 0xFFFF, 0U);

                if (fifo0ModuleChannel == FIFO_ADC1_AN1)                // ADC1_AN1
                {
                    CTUS.ibAdcRaw = fifo0Data;                          // bits mask to keep the data for IB
                }
                else if (fifo0ModuleChannel == FIFO_ADC1_AN4)           // ADC1_AN4
                {
                    CTUS.vDCBusAdcRaw = fifo0Data;                      // bits mask to keep the data for VDCBUS
                }
                else if (fifo0ModuleChannel == FIFO_ADC0_AN1)           // ADC0_AN1
                {
                    CTUS.iaAdcRaw = fifo0Data;                          // bits mask to keep the data for IA
                }
                else
                {
                    //MISRA
                }
            }
        }

        for (i = 0; i < FIFO1_SAMPLE_NUM; i++)
        {
            fifo1GroupNum = i / 3U;
            uint8_t fifo1ModuleChannel = (uint8_t) GF_BITS_EXTRACT(fifo1Raw[i], 0x1FU, 16U);
            uint16_t fif10Data = (uint16_t) GF_BITS_EXTRACT(fifo1Raw[i], 0xFFFF, 0U);

            if (fifo1ModuleChannel == FIFO_ADC1_AN0)                    // ADC1_AN0
            {
                CTUS.swgAdcRaw[fifo1GroupNum] = fif10Data;              // bits mask to keep the data for swg
            }
            else if (fifo1ModuleChannel == FIFO_ADC0_AN2)               // ADC0_AN2
            {
                CTUS.cosAdcRaw[fifo1GroupNum] = fif10Data;              // bits mask to keep the data for cos
            }
            else if (fifo1ModuleChannel == FIFO_ADC1_AN2)               // ADC1_AN2
            {
                CTUS.sinAdcRaw[fifo1GroupNum] = fif10Data;              // bits mask to keep the data for sin
            }
            else
            {
                //MISRA
            }
        }
    }
    MC_20kHz(); // running time 23us 8/1/2017  22us for close loop 7/23/2017
    gpo_pin_update_564xl_fnc(86, 0);
}
