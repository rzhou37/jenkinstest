/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-06-29
* Description: PWM configurations
***************************************************************************************************/

#ifdef __cplusplus

extern "C"{

#endif

/******************************************************************************
*                  Includes
******************************************************************************/
#include <SWLIBS_Defines.h>

#include "PWM.h"
#include "FlexPWM_564xL_library.h"
#include "GeneralFunction.h"

/******************************************************************************
*                   Global functions
******************************************************************************/
static volatile mcPWM_tag* FlexPWM_module[2] = { &FLEXPWM_0, &FLEXPWM_1 };
/******************************************************************************
*   Function:  flexpwm_GenConfig_fnc
     @brief    General Configuration for Flex PWM module 0 .
     @details   FlexPWM General Configuration Initialization code for FLEXPWM_0 Device

     @return none
*/
static void flexpwm_GenConfig_fnc (uint8_t pwmMod)
{
    /*----------------------------------------------------------- */
    /*    FlexPWM General Configuration Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->OUTEN.R = 0x0FFF;

    /* PWM A O/P : Enabled    */
    /* PWM B O/P : Enabled    */
    /* PWM X O/P : Enabled    */
    FlexPWM_module[pwmMod]->MASK.R = 0x0000;

    /* PWM A Mask : Disabled    */
    /* PWM B Mask : Disabled    */
    /* PWM X Mask : Disabled    */
    FlexPWM_module[pwmMod]->SWCOUT.R = 0x0000;

    /* S/W Controlled O/P OUTA_3 : A logic 0 is supplied to the deadtime generator of submodule 3    */
    /* S/W Controlled O/P OUTB_3 : A logic 0 is supplied to the deadtime generator of submodule 3    */
    /* S/W Controlled O/P OUTA_2 : A logic 0 is supplied to the deadtime generator of submodule 2    */
    /* S/W Controlled O/P OUTB_2 : A logic 0 is supplied to the deadtime generator of submodule 2    */
    /* S/W Controlled O/P OUTA_1 : A logic 0 is supplied to the deadtime generator of submodule 1    */
    /* S/W Controlled O/P OUTB_1 : A logic 0 is supplied to the deadtime generator of submodule 1    */
    /* S/W Controlled O/P OUTA_0 : A logic 0 is supplied to the deadtime generator of submodule 0    */
    /* S/W Controlled O/P OUTB_0 : A logic 0 is supplied to the deadtime generator of submodule 0    */
    FlexPWM_module[pwmMod]->DTSRCSEL.R = 0x0000;

    /* Deadtime source SELA_3 : Generated PWM signal is used by the deadtime logic.    */
    /* Deadtime source SELB_3 : Generated PWM signal is used by the deadtime logic.    */
    /* Deadtime source SELA_2 : Generated PWM signal is used by the deadtime logic.    */
    /* Deadtime source SELB_2 : Generated PWM signal is used by the deadtime logic.    */
    /* Deadtime source SELA_1 : Generated PWM signal is used by the deadtime logic.    */
    /* Deadtime source SELB_1 : Generated PWM signal is used by the deadtime logic.    */
    /* Deadtime source SELA_0 : Generated PWM signal is used by the deadtime logic.    */
    /* Deadtime source SELB_0 : Generated PWM signal is used by the deadtime logic.    */
    FlexPWM_module[pwmMod]->FSTS.B.FFLAG = 0xF;

    /* Clear Fault FFLAGx     */
    FlexPWM_module[pwmMod]->FCTRL.R = 0x0000;

    /* Fault Level : logic 0    */
    /* Automatic Fault Clearing : Disabled    */
    /*Fault Safe Mode         : Disabled    */
    /*Fault Interrupt         : Disabled    */
    FlexPWM_module[pwmMod]->FFILT.R = 0x0000;

    /* Filter Count : 3 samples    */
    /* Filter Period : 0    */
    FlexPWM_module[pwmMod]->MCTRL.B.IPOL= 0x0;

    /* Current Polarity (PWM) Submodule 0 : PWMA    */
    /* Current Polarity (PWM) Submodule 1 : PWMA    */
    /* Current Polarity (PWM) Submodule 2 : PWMA    */
    /* Current Polarity (PWM) Submodule 3 : PWMA    */
}

/******************************************************************************
*   Function:  flexpwm_sub0_init_fnc
     @brief    Sub module config 0  for Flex PWM module 0 .
     @details   FlexPWM Configuration  Initialization code for FLEXPWM_0 Device

     @return none
*/
/* Sub module config 0 */
static void flexpwm_sub0_init_fnc (uint8_t pwmMod)
{
    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 1 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[0].INIT.R = 0xFF00;

    /* Initial count value for the PWM : 0xFF00    */
    FlexPWM_module[pwmMod]->SUB[0].CTRL2.R = 0x9000;

    /* Debug Enable : Enabled    */
    /* Wait Enable : Disabled    */
    /* Pair Operation : Complementary PWM    */
    /* Initial value for PWMA : 1    */
    /* Initial value for PWMB : 0    */
    /* Initial value for PWMX : 0    */
    /* Initialization Control: Local sync    */
    /* Initialization from a Force Out event : Disabled    */
    /* Force Source : Local force    */
    /* Reload Source : Local reload    */
    /* Clock source for the local prescaler and counter : IPBus clock    */
    FlexPWM_module[pwmMod]->SUB[0].CTRL.R = 0x0470;

    /* PWM load frequency  : Every PWM    */
    /* Half Cycle Reload : Disabled    */
    /* Full Cycle Reload : Enabled    */
    /* Prescalar : fclk/128    */
    /* Double Switching : Disabled    */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 2 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[0].VAL[0].R = 0x0000;

    /* Mid-cycle reload point for the PWM in PWM clock periods : 0000    */
    FlexPWM_module[pwmMod]->SUB[0].VAL[1].R = 0x0100;

    /* Modulo count value (maximum count) for the submodule counter : 0x0100    */
    FlexPWM_module[pwmMod]->SUB[0].VAL[2].R = 0xFF74;

    /* Count value to set PWMA high : 0xFF74    */
    FlexPWM_module[pwmMod]->SUB[0].VAL[3].R = 0x008C;

    /* Count value to set PWMA low : 0x008C    */
    FlexPWM_module[pwmMod]->SUB[0].VAL[4].R = 0xFF7E;

    /* Count value to set PWMB high : 0xFF7E    */
    FlexPWM_module[pwmMod]->SUB[0].VAL[5].R = 0x0082;

    /* Count value to set PWMB low : 0x0082    */
    FlexPWM_module[pwmMod]->SUB[0].OCTRL.R = 0x0000;

    /* PWM A O/P Polarity : PWM A output Not Inverted    */
    /* PWM B O/P Polarity : PWM B output Not Inverted    */
    /* PWM X O/P Polarity : PWM X output Not Inverted    */
    /* PWM A O/P Fault State : Output is forced to Logic 0    */
    /* PWM B O/P Fault State : Output is forced to Logic 0    */
    /* PWM X O/P Fault State : Output is forced to Logic 0    */
    FlexPWM_module[pwmMod]->SUB[0].TCTRL.R = 0x0000;

    /* Output Trigger Control : OUT_TRIGx not set when the counter value matches the VALx value     */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 3 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[0].INTEN.R = 0x0000;

    /* REF CPU interrupt requests (Reload Error Interrupt) : Disabled    */
    /* RF CPU interrupt requests ( Reload Error ) : Disabled    */
    /* Interrupt request for CFX1 (Capture X1 Interrupt) : Disabled    */
    /* Interrupt request for CFX0 (Capture X0 Interrupt) : Disabled    */
    /* Compare 0 Interrupt : Disabled    */
    /* Compare 1 Interrupt : Disabled    */
    /* Compare 2 Interrupt : Disabled    */
    /* Compare 3 Interrupt : Disabled    */
    /* Compare 4 Interrupt : Disabled    */
    /* Compare 5 Interrupt : Disabled    */
    FlexPWM_module[pwmMod]->SUB[0].DMAEN.R = 0x0000;

    /* DMA write requests : Disabled    */
    /* Selected FIFO watermarks : OR' ed together.    */
    /* Read DMA requests : Read DMA Disabled.    */
    /* DMA read requests for the Capture A1 FIFO data : Disabled    */
    /* DMA read requests for the Capture A0 FIFO data : Disabled    */
    /* DMA read requests for the Capture B1 FIFO data : Disabled    */
    /* DMA read requests for the Capture B0 FIFO data : Disabled    */
    /* DMA read requests for the Capture X1 FIFO data : Disabled    */
    /* DMA read requests for the Capture X0 FIFO data : Disabled    */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 4 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[0].DISMAP.R = 0xF000;

    /* PWM X Fault Mask : 0000    */
    /* PWM B Fault Mask : 0000    */
    /* PWM A Fault Mask : 0000    */
    FlexPWM_module[pwmMod]->SUB[0].DTCNT0.R = 0x07FF;

    /* PWM A Dead Count : 0x7ff    */
    FlexPWM_module[pwmMod]->SUB[0].DTCNT1.R = 0x07FF;

    /* PWM B Dead Count : 0x7ff    */
    FlexPWM_module[pwmMod]->SUB[0].CAPTCTRLX.R = 0x0000;

    /* Capture X0 FIFOs Water Mark : Disabled    */
    /* Capture X1 FIFOs Water Mark : Disabled    */
    /* Edge Counter X : Disabled    */
    /* Input signal selected as source for the input capture circuit ( Input Select X ) : Raw PWMX    */
    /* Input Edge X1 Select : Disabled    */
    /* Input Edge X0 Select : Disabled    */
    /* Capture Mode : Free Running    */
    /* Input Capture Mode : Disabled    */
    FlexPWM_module[pwmMod]->SUB[0].CAPTCOMPX.R = 0x0000;

    /* Compare value associated with the edge counter for the PWMX input capture circuitry : 0x0000    */
}

/******************************************************************************
*   Function:  flexpwm_sub1_init_fnc
     @brief    Sub module config 1  for Flex PWM module 0 .
     @details   FlexPWM Configuration  Initialization code for FLEXPWM_0 Device

     @return none
*/
/* Sub module config 1 */
static void flexpwm_sub1_init_fnc (uint8_t pwmMod)
{
    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 1 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[1].INIT.R = 0xFF00;

    /* Initial count value for the PWM : 0xFF00    */
    FlexPWM_module[pwmMod]->SUB[1].CTRL2.R = 0x9000;

    /* Debug Enable : Enabled    */
    /* Wait Enable : Disabled    */
    /* Pair Operation : Complementary PWM    */
    /* Initial value for PWMA : 1    */
    /* Initial value for PWMB : 0    */
    /* Initial value for PWMX : 0    */
    /* Initialization Control: Local sync    */
    /* Initialization from a Force Out event : Disabled    */
    /* Force Source : Local force    */
    /* Reload Source : Local reload    */
    /* Clock source for the local prescaler and counter : IPBus clock    */
    FlexPWM_module[pwmMod]->SUB[1].CTRL.R = 0x0470;

    /* PWM load frequency  : Every PWM    */
    /* Half Cycle Reload : Disabled    */
    /* Full Cycle Reload : Enabled    */
    /* Prescalar : fclk/128    */
    /* Double Switching : Disabled    */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 2 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[1].VAL[0].R = 0x0000;

    /* Mid-cycle reload point for the PWM in PWM clock periods : 0000    */
    FlexPWM_module[pwmMod]->SUB[1].VAL[1].R = 0x0100;

    /* Modulo count value (maximum count) for the submodule counter : 0x0100    */
    FlexPWM_module[pwmMod]->SUB[1].VAL[2].R = 0xFF74;

    /* Count value to set PWMA high : 0xFF74    */
    FlexPWM_module[pwmMod]->SUB[1].VAL[3].R = 0x008C;

    /* Count value to set PWMA low : 0x008C    */
    FlexPWM_module[pwmMod]->SUB[1].VAL[4].R = 0xFF7E;

    /* Count value to set PWMB high : 0xFF7E    */
    FlexPWM_module[pwmMod]->SUB[1].VAL[5].R = 0x0082;

    /* Count value to set PWMB low : 0x0082    */
    FlexPWM_module[pwmMod]->SUB[1].OCTRL.R = 0x0000;

    /* PWM A O/P Polarity : PWM A output Not Inverted    */
    /* PWM B O/P Polarity : PWM B output Not Inverted    */
    /* PWM X O/P Polarity : PWM X output Not Inverted    */
    /* PWM A O/P Fault State : Output is forced to Logic 0    */
    /* PWM B O/P Fault State : Output is forced to Logic 0    */
    /* PWM X O/P Fault State : Output is forced to Logic 0    */
    FlexPWM_module[pwmMod]->SUB[1].TCTRL.R = 0x0000;

    /* Output Trigger Control : OUT_TRIGx not set when the counter value matches the VALx value     */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 3 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[1].INTEN.R = 0x0000;

    /* REF CPU interrupt requests (Reload Error Interrupt) : Disabled    */
    /* RF CPU interrupt requests ( Reload Error ) : Disabled    */
    /* Interrupt request for CFX1 (Capture X1 Interrupt) : Disabled    */
    /* Interrupt request for CFX0 (Capture X0 Interrupt) : Disabled    */
    /* Compare 0 Interrupt : Disabled    */
    /* Compare 1 Interrupt : Disabled    */
    /* Compare 2 Interrupt : Disabled    */
    /* Compare 3 Interrupt : Disabled    */
    /* Compare 4 Interrupt : Disabled    */
    /* Compare 5 Interrupt : Disabled    */
    FlexPWM_module[pwmMod]->SUB[1].DMAEN.R = 0x0000;

    /* DMA write requests : Disabled    */
    /* Selected FIFO watermarks : OR' ed together.    */
    /* Read DMA requests : Read DMA Disabled.    */
    /* DMA read requests for the Capture A1 FIFO data : Disabled    */
    /* DMA read requests for the Capture A0 FIFO data : Disabled    */
    /* DMA read requests for the Capture B1 FIFO data : Disabled    */
    /* DMA read requests for the Capture B0 FIFO data : Disabled    */
    /* DMA read requests for the Capture X1 FIFO data : Disabled    */
    /* DMA read requests for the Capture X0 FIFO data : Disabled    */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 4 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[1].DISMAP.R = 0xF000;

    /* PWM X Fault Mask : 0000    */
    /* PWM B Fault Mask : 0000    */
    /* PWM A Fault Mask : 0000    */
    FlexPWM_module[pwmMod]->SUB[1].DTCNT0.R = 0x07FF;

    /* PWM A Dead Count : 0x7ff    */
    FlexPWM_module[pwmMod]->SUB[1].DTCNT1.R = 0x07FF;

    /* PWM B Dead Count : 0x7ff    */
    FlexPWM_module[pwmMod]->SUB[1].CAPTCTRLX.R = 0x0000;

    /* Capture X0 FIFOs Water Mark : Disabled    */
    /* Capture X1 FIFOs Water Mark : Disabled    */
    /* Edge Counter X : Disabled    */
    /* Input signal selected as source for the input capture circuit ( Input Select X ) : Raw PWMX    */
    /* Input Edge X1 Select : Disabled    */
    /* Input Edge X0 Select : Disabled    */
    /* Capture Mode : Free Running    */
    /* Input Capture Mode : Disabled    */
    FlexPWM_module[pwmMod]->SUB[1].CAPTCOMPX.R = 0x0000;

    /* Compare value associated with the edge counter for the PWMX input capture circuitry : 0x0000    */
}

/******************************************************************************
*   Function:  flexpwm_sub2_init_fnc
     @brief    Sub module config 2  for Flex PWM module 0 .
     @details   FlexPWM Configuration  Initialization code for FLEXPWM_0 Device

     @return none
*/
static void flexpwm_sub2_init_fnc (uint8_t pwmMod)
{
    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 1 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[2].INIT.R = 0xFF00;

    /* Initial count value for the PWM : 0xFF00    */
    FlexPWM_module[pwmMod]->SUB[2].CTRL2.R = 0x9000;

    /* Debug Enable : Enabled    */
    /* Wait Enable : Disabled    */
    /* Pair Operation : Complementary PWM    */
    /* Initial value for PWMA : 1    */
    /* Initial value for PWMB : 0    */
    /* Initial value for PWMX : 0    */
    /* Initialization Control: Local sync    */
    /* Initialization from a Force Out event : Disabled    */
    /* Force Source : Local force    */
    /* Reload Source : Local reload    */
    /* Clock source for the local prescaler and counter : IPBus clock    */
    FlexPWM_module[pwmMod]->SUB[2].CTRL.R = 0x0470;

    /* PWM load frequency  : Every PWM    */
    /* Half Cycle Reload : Disabled    */
    /* Full Cycle Reload : Enabled    */
    /* Prescalar : fclk/128    */
    /* Double Switching : Disabled    */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 2 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[2].VAL[0].R = 0x0000;

    /* Mid-cycle reload point for the PWM in PWM clock periods : 0000    */
    FlexPWM_module[pwmMod]->SUB[2].VAL[1].R = 0x0100;

    /* Modulo count value (maximum count) for the submodule counter : 0x0100    */
    FlexPWM_module[pwmMod]->SUB[2].VAL[2].R = 0xFF74;

    /* Count value to set PWMA high : 0xFF74    */
    FlexPWM_module[pwmMod]->SUB[2].VAL[3].R = 0x008C;

    /* Count value to set PWMA low : 0x008C    */
    FlexPWM_module[pwmMod]->SUB[2].VAL[4].R = 0xFF7E;

    /* Count value to set PWMB high : 0xFF7E    */
    FlexPWM_module[pwmMod]->SUB[2].VAL[5].R = 0x0082;

    /* Count value to set PWMB low : 0x0082    */
    FlexPWM_module[pwmMod]->SUB[2].OCTRL.R = 0x0000;

    /* PWM A O/P Polarity : PWM A output Not Inverted    */
    /* PWM B O/P Polarity : PWM B output Not Inverted    */
    /* PWM X O/P Polarity : PWM X output Not Inverted    */
    /* PWM A O/P Fault State : Output is forced to Logic 0    */
    /* PWM B O/P Fault State : Output is forced to Logic 0    */
    /* PWM X O/P Fault State : Output is forced to Logic 0    */
    FlexPWM_module[pwmMod]->SUB[2].TCTRL.R = 0x0000;

    /* Output Trigger Control : OUT_TRIGx not set when the counter value matches the VALx value     */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 3 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[2].INTEN.R = 0x0000;

    /* REF CPU interrupt requests (Reload Error Interrupt) : Disabled    */
    /* RF CPU interrupt requests ( Reload Error ) : Disabled    */
    /* Interrupt request for CFX1 (Capture X1 Interrupt) : Disabled    */
    /* Interrupt request for CFX0 (Capture X0 Interrupt) : Disabled    */
    /* Compare 0 Interrupt : Disabled    */
    /* Compare 1 Interrupt : Disabled    */
    /* Compare 2 Interrupt : Disabled    */
    /* Compare 3 Interrupt : Disabled    */
    /* Compare 4 Interrupt : Disabled    */
    /* Compare 5 Interrupt : Disabled    */
    FlexPWM_module[pwmMod]->SUB[2].DMAEN.R = 0x0000;

    /* DMA write requests : Disabled    */
    /* Selected FIFO watermarks : OR' ed together.    */
    /* Read DMA requests : Read DMA Disabled.    */
    /* DMA read requests for the Capture A1 FIFO data : Disabled    */
    /* DMA read requests for the Capture A0 FIFO data : Disabled    */
    /* DMA read requests for the Capture B1 FIFO data : Disabled    */
    /* DMA read requests for the Capture B0 FIFO data : Disabled    */
    /* DMA read requests for the Capture X1 FIFO data : Disabled    */
    /* DMA read requests for the Capture X0 FIFO data : Disabled    */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 4 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[2].DISMAP.R = 0xF000;

    /* PWM X Fault Mask : 0000    */
    /* PWM B Fault Mask : 0000    */
    /* PWM A Fault Mask : 0000    */
    FlexPWM_module[pwmMod]->SUB[2].DTCNT0.R = 0x07FF;

    /* PWM A Dead Count : 0x7ff    */
    FlexPWM_module[pwmMod]->SUB[2].DTCNT1.R = 0x07FF;

    /* PWM B Dead Count : 0x7ff    */
    FlexPWM_module[pwmMod]->SUB[2].CAPTCTRLX.R = 0x0000;

    /* Capture X0 FIFOs Water Mark : Disabled    */
    /* Capture X1 FIFOs Water Mark : Disabled    */
    /* Edge Counter X : Disabled    */
    /* Input signal selected as source for the input capture circuit ( Input Select X ) : Raw PWMX    */
    /* Input Edge X1 Select : Disabled    */
    /* Input Edge X0 Select : Disabled    */
    /* Capture Mode : Free Running    */
    /* Input Capture Mode : Disabled    */
    FlexPWM_module[pwmMod]->SUB[2].CAPTCOMPX.R = 0x0000;

    /* Compare value associated with the edge counter for the PWMX input capture circuitry : 0x0000    */
}

/******************************************************************************
*   Function:  flexpwm_sub3_init_fnc
     @brief    Sub module config 3  for Flex PWM module 0 .
     @details   FlexPWM Configuration  Initialization code for FLEXPWM_0 Device

     @return none
*/
static void flexpwm_sub3_init_fnc (uint8_t pwmMod)
{
    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 1 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[3].INIT.R = 0x0000;

    /* Initial count value for the PWM : 0x0    */
    FlexPWM_module[pwmMod]->SUB[3].CTRL2.R = 0x0000;

    /* Debug Enable : Disabled    */
    /* Wait Enable : Disabled    */
    /* Pair Operation : Complementary PWM    */
    /* Initial value for PWMA : 0    */
    /* Initial value for PWMB : 0    */
    /* Initial value for PWMX : 0    */
    /* Initialization Control: Local sync    */
    /* Initialization from a Force Out event : Disabled    */
    /* Force Source : Local force    */
    /* Reload Source : Local reload    */
    /* Clock source for the local prescaler and counter : IPBus clock    */
    FlexPWM_module[pwmMod]->SUB[3].CTRL.R = 0x0420;

    /* PWM load frequency  : Every PWM    */
    /* Half Cycle Reload : Disabled    */
    /* Full Cycle Reload : Enabled    */
    /* Prescalar : fclk/4    */
    /* Double Switching : Disabled    */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 2 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[3].VAL[0].R = 0x0000;

    /* Mid-cycle reload point for the PWM in PWM clock periods : 0000    */
    FlexPWM_module[pwmMod]->SUB[3].VAL[1].R = 0x0000;

    /* Modulo count value (maximum count) for the submodule counter : 0x0000    */
    FlexPWM_module[pwmMod]->SUB[3].VAL[2].R = 0x0000;

    /* Count value to set PWMA high : 0x0000    */
    FlexPWM_module[pwmMod]->SUB[3].VAL[3].R = 0x0000;

    /* Count value to set PWMA low : 0x0000    */
    FlexPWM_module[pwmMod]->SUB[3].VAL[4].R = 0x0000;

    /* Count value to set PWMB high : 0x0000    */
    FlexPWM_module[pwmMod]->SUB[3].VAL[5].R = 0x0000;

    /* Count value to set PWMB low : 0x0000    */
    FlexPWM_module[pwmMod]->SUB[3].OCTRL.R = 0x0000;

    /* PWM A O/P Polarity : PWM A output Not Inverted    */
    /* PWM B O/P Polarity : PWM B output Not Inverted    */
    /* PWM X O/P Polarity : PWM X output Not Inverted    */
    /* PWM A O/P Fault State : Output is forced to Logic 0    */
    /* PWM B O/P Fault State : Output is forced to Logic 0    */
    /* PWM X O/P Fault State : Output is forced to Logic 0    */
    FlexPWM_module[pwmMod]->SUB[3].TCTRL.R = 0x0000;

    /* Output Trigger Control : OUT_TRIGx not set when the counter value matches the VALx value     */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 3 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[3].INTEN.R = 0x0000;

    /* REF CPU interrupt requests (Reload Error Interrupt) : Disabled    */
    /* RF CPU interrupt requests ( Reload Error ) : Disabled    */
    /* Interrupt request for CFX1 (Capture X1 Interrupt) : Disabled    */
    /* Interrupt request for CFX0 (Capture X0 Interrupt) : Disabled    */
    /* Compare 0 Interrupt : Disabled    */
    /* Compare 1 Interrupt : Disabled    */
    /* Compare 2 Interrupt : Disabled    */
    /* Compare 3 Interrupt : Disabled    */
    /* Compare 4 Interrupt : Disabled    */
    /* Compare 5 Interrupt : Disabled    */
    FlexPWM_module[pwmMod]->SUB[3].DMAEN.R = 0x0000;

    /* DMA write requests : Disabled    */
    /* Selected FIFO watermarks : OR' ed together.    */
    /* Read DMA requests : Read DMA Disabled.    */
    /* DMA read requests for the Capture A1 FIFO data : Disabled    */
    /* DMA read requests for the Capture A0 FIFO data : Disabled    */
    /* DMA read requests for the Capture B1 FIFO data : Disabled    */
    /* DMA read requests for the Capture B0 FIFO data : Disabled    */
    /* DMA read requests for the Capture X1 FIFO data : Disabled    */
    /* DMA read requests for the Capture X0 FIFO data : Disabled    */

    /*----------------------------------------------------------- */
    /*    FlexPWM Configuration 4 Initialization code for FLEXPWM_0 Device    */
    /*----------------------------------------------------------- */
    FlexPWM_module[pwmMod]->SUB[3].DISMAP.R = 0xF000;

    /* PWM X Fault Mask : 0000    */
    /* PWM B Fault Mask : 0000    */
    /* PWM A Fault Mask : 0000    */
    FlexPWM_module[pwmMod]->SUB[3].DTCNT0.R = 0x07FF;

    /* PWM A Dead Count : 0x7ff    */
    FlexPWM_module[pwmMod]->SUB[3].DTCNT1.R = 0x07FF;

    /* PWM B Dead Count : 0x7ff    */
    FlexPWM_module[pwmMod]->SUB[3].CAPTCTRLX.R = 0x0000;

    /* Capture X0 FIFOs Water Mark : Disabled    */
    /* Capture X1 FIFOs Water Mark : Disabled    */
    /* Edge Counter X : Disabled    */
    /* Input signal selected as source for the input capture circuit ( Input Select X ) : Raw PWMX    */
    /* Input Edge X1 Select : Disabled    */
    /* Input Edge X0 Select : Disabled    */
    /* Capture Mode : Free Running    */
    /* Input Capture Mode : Disabled    */
    FlexPWM_module[pwmMod]->SUB[3].CAPTCOMPX.R = 0x0000;

    /* Compare value associated with the edge counter for the PWMX input capture circuitry : 0x0000    */
}


/**********************  Initialization Function(s) *************************/

/******************************************************************************
*   Function:  flexpwm_init_fnc
 @brief    initialization for Flex PWM module 0 .
 @details   initialization for Flex PWM module 0

 @return none
*/
void PWM_init(void)
{
    flexpwm_sub0_init_fnc(0);
    flexpwm_sub1_init_fnc(0);
    flexpwm_sub2_init_fnc(0);
    flexpwm_sub3_init_fnc(0);
    flexpwm_GenConfig_fnc(0);
    flexpwm_sub0_init_fnc(1);
    flexpwm_sub1_init_fnc(1);
    flexpwm_sub2_init_fnc(1);
    flexpwm_sub3_init_fnc(1);
    flexpwm_GenConfig_fnc(1);

    /* Initialize Pad Configuration Registers PAn & PBn as Output */
    pwm_init_pcr_out_564xL(0,3,0,102);
    pwm_init_pcr_out_564xL(0,3,1,103);
    pwm_init_pcr_out_564xL(0,3,2,101);

    /* Initialize PWM Submodule 3 to correct dutycycle and frequency */
    pwm_init_564xL_simple(0,3, 80000, 100, 5000, 6666, 0, 0, 0, 1, 0, 0, 0);

    /* Initialize Pad Configuration Registers PAn & PBn as Output */
    pwm_init_pcr_out_564xL(0,0,0,11);
    pwm_init_pcr_out_564xL(0,0,1,10);
    pwm_init_pcr_out_564xL(0,1,0,47);
    pwm_init_pcr_out_564xL(0,1,1,48);
    pwm_init_pcr_out_564xL(0,2,0,12);
    pwm_init_pcr_out_564xL(0,2,1,13);

    /* Initialize PWM Submodules to correct dutycycle and frequency */
    pwm_init_564xL_three_phase(0,1,2,5000,5000,5000,6000,9000,1,0,0,360,0,0,0);
}

// Magnitude must not be larger than sqrt(3)/2, or 0.866
void PWM_svmGenerateor(MCT_transformFrameAlphaBeta_S* vAB, uint32_t pwmHalfPeriod, tFloat vDcBus,
        uint32_t* tAout, uint32_t* tBout, uint32_t* tCout)
{
    uint32_t sector;
    // PWM timings
    uint32_t tA, tB, tC;
    tFloat vAlpha, vBeta;

    vAlpha = vAB->alpha / vDcBus;
    vBeta = vAB->beta / vDcBus;

    if (vBeta >= 0.0F)
    {
        if (vAlpha >= 0.0F)
        {
            //quadrant I
            if (FLOAT_DIVBY_SQRT3 * vBeta > vAlpha)
            {
                sector = 2;
            }
            else
            {
                sector = 1;
            }
        }
        else
        {
            //quadrant II
            if (-FLOAT_DIVBY_SQRT3 * vBeta > vAlpha)
            {
                sector = 3;
            }
            else
            {
                sector = 2;
            }
        }
    }
    else
    {
        if (vAlpha >= 0.0F)
        {
            //quadrant IV5
            if (-FLOAT_DIVBY_SQRT3 * vBeta > vAlpha)
            {
                sector = 5;
            }
            else
            {
                sector = 6;
            }
        }
        else
        {
            //quadrant III
            if (FLOAT_DIVBY_SQRT3 * vBeta > vAlpha)
            {
                sector = 4;
            }
            else
            {
                sector = 5;
            }
        }
    }

    switch (sector)
    {
        // sector 1-2
        case 1:
        {
            // Vector on-times
            uint32_t t1 = (uint32_t)(1.5F * (vAlpha - (FLOAT_DIVBY_SQRT3 * vBeta)) * pwmHalfPeriod);
            uint32_t t2 = (uint32_t)(1.5F * (FLOAT_2_DIVBY_SQRT3 * vBeta) * pwmHalfPeriod);

            // PWM timings
            tA = (pwmHalfPeriod + t1 + t2) / 2;
            tB = tA - t1;
            tC = tB - t2;

            break;
        }

        // sector 2-3
        case 2:
        {
            // Vector on-times
            uint32_t t2 = (uint32_t)(1.5F * (vAlpha + (FLOAT_DIVBY_SQRT3 * vBeta)) * pwmHalfPeriod);
            uint32_t t3 = (uint32_t)(1.5F * (-vAlpha + (FLOAT_DIVBY_SQRT3 * vBeta)) * pwmHalfPeriod);

            // PWM timings
            tB = (pwmHalfPeriod + t2 + t3) / 2;
            tA = tB - t3;
            tC = tA - t2;

            break;
        }

        // sector 3-4
        case 3:
        {
            // Vector on-times
            uint32_t t3 = (uint32_t)(1.5F * (FLOAT_2_DIVBY_SQRT3 * vBeta) * pwmHalfPeriod);
            uint32_t t4 = (uint32_t)(1.5F * (-vAlpha - (FLOAT_DIVBY_SQRT3 * vBeta)) * pwmHalfPeriod);

            // PWM timings
            tB = (pwmHalfPeriod + t3 + t4) / 2;
            tC = tB - t3;
            tA = tC - t4;

            break;
        }

        // sector 4-5
        case 4:
        {
            // Vector on-times
            uint32_t t4 = (uint32_t)(1.5F * (-vAlpha + (FLOAT_DIVBY_SQRT3 * vBeta)) * pwmHalfPeriod);
            uint32_t t5 = (uint32_t)(1.5F * (-FLOAT_2_DIVBY_SQRT3 * vBeta) * pwmHalfPeriod);

            // PWM timings
            tC = (pwmHalfPeriod + t4 + t5) / 2;
            tB = tC - t5;
            tA = tB - t4;

            break;
        }

        // sector 5-6
        case 5:
        {
            // Vector on-times
            uint32_t t5 = (uint32_t)(1.5F * (-vAlpha - (FLOAT_DIVBY_SQRT3 * vBeta)) * pwmHalfPeriod);
            uint32_t t6 = (uint32_t)(1.5F * (vAlpha - (FLOAT_DIVBY_SQRT3 * vBeta)) * pwmHalfPeriod);

            // PWM timings
            tC = (pwmHalfPeriod + t5 + t6) / 2;
            tA = tC - t5;
            tB = tA - t6;

            break;
        }

        // sector 6-1
        case 6:
        {
            // Vector on-times
            uint32_t t6 = (uint32_t)(1.5F * (-FLOAT_2_DIVBY_SQRT3 * vBeta) * pwmHalfPeriod);
            uint32_t t1 = (uint32_t)(1.5F * (vAlpha + (FLOAT_DIVBY_SQRT3 * vBeta)) * pwmHalfPeriod);

            // PWM timings
            tA = (pwmHalfPeriod + t6 + t1) / 2;
            tC = tA - t1;
            tB = tC - t6;

            break;
        }
    }

    *tAout = tA;
    *tBout = tB;
    *tCout = tC;
}
void PWM_update(uint8_t pwmMod, uint8_t subModB, uint8_t
   subModC, uint32_t Freq, uint16_t resolution, uint16_t DutyA, uint16_t DutyB,
   uint16_t DutyC,
   uint16_t outTrigA, uint16_t outTrigB, uint16_t outTrigC, uint16_t DeadTime,
   uint16_t OCTRL_A,
   uint16_t OCTRL_B, uint16_t OCTRL_C)
 {
   uint16_t prescaler, mask, maskA, maskB, maskC;
   uint16_t init, val1, val2_A, val3_A, val2_B, val3_B, val2_C, val3_C;
   uint32_t period = 0U;
   volatile mcPWM_tag* FlexPWM = FlexPWM_module[pwmMod];
   uint32_t DutyMax = 100 * resolution;
   maskA = 0x1U;
   maskB = (uint16_t)(0x1U << subModB);
   maskC = (uint16_t)(0x1U << subModC);
   mask = maskA | maskB | maskC;
   prescaler = pwm_prescaler (Freq, &period);
   val1 = (uint16_t) (period / 2);
   init = 1 - val1 ;
   if (DutyA >= DutyMax) {
     val2_A = init;
     val3_A = val1+1;
   } else {
     val3_A = (uint16_t)((DutyA * val1)/DutyMax);
     val2_A = 0 - val3_A;
   }

   if (DutyB >= DutyMax) {
     val2_B = init;
     val3_B = val1+1;
   } else {
     val3_B = (uint16_t)((DutyB * val1)/DutyMax);
     val2_B = 0 - val3_B;
   }

   if (DutyC >= DutyMax) {
     val2_C = init;
     val3_C = val1+1;
   } else {
     val3_C = (uint16_t)((DutyC * val1)/DutyMax);
     val2_C = 0 - val3_C;
   }

   FlexPWM->SUB[0].INIT.R = init;
   FlexPWM->SUB[0].VAL[1].R = val1;
   FlexPWM->SUB[0].VAL[2].R = val2_A;
//    FlexPWM->SUB[0].VAL[2].R = init;
   FlexPWM->SUB[0].VAL[3].R = val3_A;
//    FlexPWM->SUB[0].VAL[3].R = 0;
//     FlexPWM->SUB[0].DTCNT0.R = DeadTime;
//     FlexPWM->SUB[0].DTCNT1.R = DeadTime;
   FlexPWM->SUB[0].CTRL.R = prescaler | 0x0400;
//    FlexPWM->SUB[0].TCTRL.B.OUT_TRIG_EN = outTrigA;
//     FlexPWM->SUB[0].OCTRL.R = OCTRL_A;
   FlexPWM->SUB[subModB].INIT.R = init;
   FlexPWM->SUB[subModB].VAL[2].R = val2_B;
   FlexPWM->SUB[subModB].VAL[3].R = val3_B;
//     FlexPWM->SUB[subModB].DTCNT0.R = DeadTime;
//     FlexPWM->SUB[subModB].DTCNT1.R = DeadTime;
   FlexPWM->SUB[subModB].CTRL.R = prescaler | 0x0400;
//    FlexPWM->SUB[subModB].TCTRL.B.OUT_TRIG_EN = outTrigB;
//     FlexPWM->SUB[subModB].OCTRL.R = OCTRL_B;
   FlexPWM->SUB[subModC].INIT.R = init;
   FlexPWM->SUB[subModC].VAL[2].R = val2_C;
   FlexPWM->SUB[subModC].VAL[3].R = val3_C;
//     FlexPWM->SUB[subModC].DTCNT0.R = DeadTime;
//     FlexPWM->SUB[subModC].DTCNT1.R = DeadTime;
   FlexPWM->SUB[subModC].CTRL.R = prescaler | 0x0400;
//    FlexPWM->SUB[subModC].TCTRL.B.OUT_TRIG_EN = outTrigC;
//     FlexPWM->SUB[subModC].OCTRL.R = OCTRL_C;
//     FlexPWM->SUB[subModB].CTRL2.R = 0x9200;

   /* Initialization Control: Master sync    */
//     FlexPWM->SUB[subModC].CTRL2.R = 0x9200;

   /* Initialization Control: Master sync    */
   FlexPWM->MCTRL.R = FlexPWM->MCTRL.R | mask;/* LDOK */
   //FlexPWM->MCTRL.R = FlexPWM->MCTRL.R | (mask << 8);/* RUN  */
//     FlexPWM->MCTRL.R = FlexPWM->MCTRL.R | (0b1111 << 8);/* RUN  */
 }
#ifdef __cplusplus

}
#endif
/*
 *######################################################################
 *                           End of File
 *######################################################################
 */
