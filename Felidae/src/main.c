/*
 * main implementation: use this 'C' sample to create your own application
 *
 */

#include "derivative.h" /* include peripheral declarations */
#include "MotorControl.h"
#include "System.h"
#include "PWM.h"
#include "ADC.h"
#include "GPIO.h"
#include "CTU.h"
#include "freemaster.h"
#include "flexcan_init.h"
#include "CAN.h"
#include "CRC.h"
#include "LINFlex.h"
#include "Resolver.h"

extern void xcptn_xmpl(void);

int main(void)
{
    SYS_init();             // System init
    ADC_init();             // ADC init
    GPIO_init();            // GPIO init
    CRC_init();             // CRC init
    CAN_init();             // CAN init
    flexcan0_init();        // CAN0 init for Freemaster. FIXME: Need to be moved to CAN_init()
    LINFlex_init();
    (void) FMSTR_Init();    // Freemaster init
    MC_init();              // Motor control init
    RES_init();             // Resolver init
    PWM_init();             // PWM init
    CTU_init();             // CTU init, need to be the last init function before enable interrupt

    INTC.CPR.R = 0x0A;  // Global Minimum Interrupt Priority
    xcptn_xmpl ();      // Configure and Enable Interrupts

    // Loop forever
    for(;;)
    {
        volatile int i;
        for (i = 0; i < 20U; i++)
        {
            // delay
            i = i * 1U;
        }
        FMSTR_Poll();               // Freemaster poll
        CAN_100Hz();    //FIXME: Move to 100Hz
        CAN_1Hz();      //FIXME: Move to 1Hz
    }
}
