/*
 * File: sysclk.h
 *
 * Code generated for Simulink model 'MCU_2016b'.
 *
 * Model version                  : 1.291
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Wed Jun 14 15:28:17 2017
 *
 * Target selection: rappid564xl.tlc
 * Embedded hardware selection: Freescale->32-bit PowerPC
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objective: MISRA C:2012 guidelines
 * Validation result: Not run
 */

#ifndef RTW_HEADER_sysclk_h_
#define RTW_HEADER_sysclk_h_

/*                                    Parameters for 40Mhz XTAL 120Mhz Clk   */
#define IDF_0                          4 - 1                     /* Input Division Factor: 4   */
#define ODF_0                          1                         /* Output Division Factor: 4   */
#define NDIV_0                         48                        /* Loop Division Factor: 48    */

/* XTAL_40MHz / 4 / 4 * 48 = 120MHz  */
#define IDF_1                          4 - 1                     /* Input Division Factor: 4   */
#define ODF_1                          1                         /* Output Division Factor: 4   */
#define NDIV_1                         48                        /* Loop Division Factor: 48    */

/* XTAL_40MHz / 4 / 4 * 48 = 120MHz  */
#define MC_DIV                         0                        /* Motor Control Clock Divider Value: (11 + 1), Frequency 10000000 Hz */
#define SW_DIV                         5                         /* Sinewave Generator Clock Divider Value: (5 + 1), Frequency 20000000 Hz */
#define MC_CLOCK                       120000000                  /* Motor Control Clock */
#define SWG_CLOCK                      20000000                  /* Sinewave Generator Clock */
#endif                                 /* RTW_HEADER_sysclk_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
