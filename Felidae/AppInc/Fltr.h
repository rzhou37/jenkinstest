/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: patel.reddy@sfmotors.com
* Date: 2017-05-19
* Description: Filter functions
***************************************************************************************************/

#ifndef FLTR_H_
#define FLTR_H_

#include "typedefs.h"

typedef enum e_fltrMode
{
    E_FLTR_LOWP = 1,
    E_FLTR_HIGHP = 2,
    E_FLTR_BANDP = 3,
} fltrMode_E;

typedef struct s_fltr
{
    // Q - 1/2/Overshoot
    tFloat q;
    // Sample Step
    tFloat dT;
    // Cutoff Filter Frequency for low pass
    tFloat fC1;
    // Cutoff Filter Frequency for low pass
    tFloat wC1;
    // Filter Coefficients
    tFloat a,b,b1,b2,h1,h2;
    // Filter History
    tFloat yN1, yN2, xN1;

    fltrMode_E fltrMode;
} fltr_S;

typedef fltr_S                 fltrLP;
typedef fltr_S                 fltrHP;

extern void FLTR_init(fltr_S* fltr, tFloat dT, tFloat q, tFloat fC1, fltrMode_E fltrMode);
extern tFloat FLTR_periodic(tFloat xI, fltr_S* fltr);

#endif /* FLTR_H_ */
