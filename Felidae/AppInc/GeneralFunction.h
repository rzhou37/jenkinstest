/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-05-25
* Description: General functions that is shared in the project.
***************************************************************************************************/

#ifndef GENERALFUNCTION_H_
#define GENERALFUNCTION_H_

#define FREQ_1M_HZ      1000000U
#define FREQ_100K_HZ    100000U
#define FREQ_10K_HZ     10000U
#define FREQ_20K_HZ     20000U
#define FREQ_1K_HZ      1000U
#define FREQ_100_HZ     100U
#define FREQ_50_HZ      50U
#define FREQ_10_HZ      10U
#define FREQ_1_HZ       1U

#define SAMPLETIME_20K_HZ (1.0F/(tFloat)FREQ_20K_HZ)


// Constants
#define DIVBY_SQRT3             0.57735026918962576450914878050196
#define TWO_DIVBY_SQRT3         1.1547005383792515290182975610039
#define SQRT3_DIVBY_2           0.86602540378443864676372317075294
#define TWO_PI                  6.283185307179586476925286766559
#define PI                      3.1415926535897932384626433832795
#define TWO_DIVBY_PI            0.63661977236758134307553505349006
#define ONE_OVER_THREE          0.33333333333333333333333333333333
#define SQUARE_ROOT_2           1.4142135623730950488016887242097

#ifndef FLOAT_DIVBY_SQRT3
#define FLOAT_DIVBY_SQRT3       ((tFloat) DIVBY_SQRT3)
#endif
#ifndef FLOAT_2_DIVBY_SQRT3
#define FLOAT_2_DIVBY_SQRT3     ((tFloat) TWO_DIVBY_SQRT3)
#endif
#ifndef FLOAT_SQRT3_DIVBY_2
#define FLOAT_SQRT3_DIVBY_2     ((tFloat) SQRT3_DIVBY_2)
#endif
#ifndef FLOAT_PI
#define FLOAT_PI                ((tFloat) PI)
#endif
#ifndef FLOAT_2_PI
#define FLOAT_2_PI              ((tFloat) TWO_PI)
#endif
#ifndef FLOAT_2_DIVBY_PI
#define FLOAT_2_DIVBY_PI        ((tFloat) TWO_DIVBY_PI)
#endif
#ifndef FLOAT_1_OVER_3
#define FLOAT_1_OVER_3          ((tFloat) ONE_OVER_THREE)
#endif
#ifndef FLOAT_2_OVER_3
#define FLOAT_2_OVER_3          ((tFloat) ONE_OVER_THREE * 2.0F)
#endif
#ifndef FLOAT_4_OVER_3
#define FLOAT_4_OVER_3          ((tFloat) ONE_OVER_THREE * 4.0F)
#endif
#ifndef FLOAT_SQUARE_ROOT_2
#define FLOAT_SQUARE_ROOT_2     ((tFloat) SQUARE_ROOT_2)
#endif
#ifndef FLOAT_1_OVER_2_PI
#define FLOAT_1_OVER_2_PI       ((tFloat) 1.0F / FLOAT_2_PI)
#endif

#define GF_FREQ_DIVIDER(baseFreq, targetFreq)   ((uint32_t)((baseFreq)/(targetFreq)))

#define GF_MIN(val1, val2)                      (((val1)<(val2))?(val1):(val2))
#define GF_MAX(val1, val2)                      (((val1)>(val2))?(val1):(val2))
#define GF_SAT(val, min, max)                   (GF_MAX(GF_MIN((val), (max)), (min)))
#define GF_RANGE_CHECK(val, min, max)           (((val<=max)&&(val>=min))?(TRUE):(FALSE))

#define GF_BITS_EXTRACT(value, mask, shift)     (((value)&((mask)<<(shift)))>>(shift))

#define GF_UINT8_16_COMBINE(lsb, msb)           (((lsb) & 0x0FU) + (((msb) << 8U) & 0xF0U))

#endif /* GENERALFUNCTION_H_ */
