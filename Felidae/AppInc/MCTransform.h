/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-05-19
* Description: Motor control transforms
***************************************************************************************************/

#ifndef MCT_H
#define MCT_H

#include "typedefs.h"

typedef struct s_angleTrigonometric
{
    tFloat angle;
    tFloat sin;
    tFloat cos;
} MCT_angleTrigonometric_S;

typedef struct s_transformFrameABC
{
    tFloat a;
    tFloat b;
    tFloat c;
} MCT_transformFrameABC_S;
    
typedef struct s_transformFrameAlphaBeta
{
    tFloat alpha;
    tFloat beta;
    tFloat zeroAB;
} MCT_transformFrameAlphaBeta_S;

typedef struct s_transformFrameDQ
{
    tFloat d;
    tFloat q;
    tFloat zeroDQ;
} MCT_transformFrameDQ_S;

typedef MCT_transformFrameABC_S                 MCT_motorIabc_S;
typedef MCT_transformFrameAlphaBeta_S           MCT_motorIAlphaBeta_S;
typedef MCT_transformFrameDQ_S                  MCT_motorIdq_S;

typedef MCT_transformFrameABC_S                 MCT_motorVabc_S;
typedef MCT_transformFrameAlphaBeta_S           MCT_motorVAlphaBeta_S;
typedef MCT_transformFrameDQ_S                  MCT_motorVdq_S;

typedef MCT_transformFrameAlphaBeta_S           MCT_motorStatorFluxAlphaBeta_S;
typedef MCT_transformFrameDQ_S                  MCT_motorStatorFluxdq_S;
typedef MCT_transformFrameAlphaBeta_S           MCT_motorRotorFluxAlphaBeta_S;
typedef MCT_transformFrameDQ_S                  MCT_motorRotorFluxdq_S;

extern void MCT_clarkeTransform(const MCT_motorIabc_S* const iabc, MCT_motorIAlphaBeta_S* const iAB);
extern void MCT_clarkeInvTransform(const MCT_motorVAlphaBeta_S* const vAB, MCT_motorVabc_S* const vabc);
extern void MCT_angleTrigonometric(const tFloat angle, MCT_angleTrigonometric_S* const trigonometric);
extern void MCT_parkTransform(const MCT_angleTrigonometric_S* const rotorFluxAngle, const MCT_motorIAlphaBeta_S* const iAB, MCT_motorIdq_S* const idq);
extern void MCT_parkInvTransform(const MCT_angleTrigonometric_S* const rotorFluxAngle, const MCT_motorVdq_S* const vdq, MCT_motorVAlphaBeta_S* const vAB);
extern tFloat MCT_rms(const MCT_motorIabc_S* const iabc);
extern tFloat MCT_thetaWrap(tFloat thetaIn);

#endif /* MCT_H */
