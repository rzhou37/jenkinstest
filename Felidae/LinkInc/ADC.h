/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-06-29
* Description: ADC configurations header.
***************************************************************************************************/
#ifndef ADC_H_
#define ADC_H_

#ifdef __cplusplus

extern "C"{

#endif

/******************************************************************************
*                  Includes
******************************************************************************/
//#include "target.h"
#include "mpc5643l.h"

#define ADC_RANGE                                       4095U
#define ADC_CONVERSION(rawValue,range,offset)           ((tFloat)(rawValue) * ((range) / (tFloat)(ADC_RANGE)) + (offset))


/******************************************************************************
*                   Global function prototypes
******************************************************************************/
void ADC_init(void);

#ifdef __cplusplus

}
#endif

#endif /* ADC_H_ */
