/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-07-23
* Description: CRC configuration and calculation.
***************************************************************************************************/

#ifndef CRC_H_
#define CRC_H_

typedef union {
    uint32_t word;
    uint16_t half[2];
    uint8_t  byte[4];
} CRC_DATA_IN_U;

void CRC_init(void);
uint16_t CRC_calculate16(const uint32_t context, const CRC_DATA_IN_U * const input, const uint32_t length);
uint32_t CRC_calculate32(const uint32_t context, const CRC_DATA_IN_U * const input, const uint32_t length);
uint8_t CRC_calculate8(const uint32_t context, const CRC_DATA_IN_U * const input, const uint32_t length);

#endif /* CRC_H_ */
