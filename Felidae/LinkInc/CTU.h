/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-06-29
* Description: CTU header.
***************************************************************************************************/

#ifndef CTU_H_
#define CTU_H_

typedef struct s_CTU
{
    uint32_t swgAdcRaw[4];
    uint32_t sinAdcRaw[4];
    uint32_t cosAdcRaw[4];
    int16_t iaAdcRaw;
    int16_t ibAdcRaw;
    uint32_t vDCBusAdcRaw;
} CTU_S;

extern CTU_S CTUS;  // the keyword"CTU" has been used by MPC5643L.h as hardware register structure

extern void CTU_init(void);
extern void CTU_fifo0_ISR(void);

#endif /* CTU_H_ */
