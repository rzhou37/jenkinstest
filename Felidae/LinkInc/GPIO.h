/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: shawn.yuan@sfmotors.com
* Date: 2017-06-29
* Description: GPIO configurations.
***************************************************************************************************/

#ifndef GPIO_H_
#define GPIO_H_

#include "gpio_564xl_library.h"

#define GPIO_PWM0_A0     SIU.GPDI[11].R

#define GPIO_HIGH        1U
#define GPIO_LOW         0U

extern void GPIO_init(void);

#endif /* GPIO_H_ */
