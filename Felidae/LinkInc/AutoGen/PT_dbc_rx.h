/**
 * This is an auto-generated file from C:\C\Common\CAN_PT.dbc. 
 * Generated at: 2017-07-27 10:36:49
 */

#ifndef _PT_DBC_RX_H_
#define _PT_DBC_RX_H_

#include "PT_dbc_common.h"

/**
 * Message: DYNO_MCUR0
 * MID: 1807
 * Network: PT
 * Transmitter: DYNO
 * Receiver: MCUR0
 */
typedef union
{
    struct
    {
        uint32_t dyno_mcur0Mult:8;                       // Multiplexer index. 
        union
        {
            struct
            {
                uint32_t dyno_r0StateRequest:1;          // Multiplexer signal. 
                uint32_t unused_0:15;                    // Multiplexer signal. 
                uint32_t dyno_r0VoltageCmd_0:8;          // Multiplexer signal. 
                uint32_t dyno_r0VoltageCmd_1:1;          // Multiplexer signal. 
                uint32_t unused_1:31;                    // Multiplexer signal. 
            } m0;
        } multiUnion;
    } signalsStruct;
    uint32_t rawData[2];
} PT_DYNO_MCUR0_U;
#define PT_DYNO_MCUR0_LEN                                      8U

#define PT_DYNO_MCUR0MULT_FACTOR                        1.000000F
#define PT_DYNO_MCUR0MULT_OFFSET                        0.000000F
#define PT_DYNO_MCUR0MULT_MIN                           0.000000F
#define PT_DYNO_MCUR0MULT_MAX                         255.000000F
#define PT_DYNO_R0STATEREQUEST_FACTOR                   1.000000F
#define PT_DYNO_R0STATEREQUEST_OFFSET                   0.000000F
#define PT_DYNO_R0STATEREQUEST_MIN                      0.000000F
#define PT_DYNO_R0STATEREQUEST_MAX                      0.000000F
#define PT_DYNO_R0VOLTAGECMD_FACTOR                     1.000000F
#define PT_DYNO_R0VOLTAGECMD_OFFSET                     0.000000F
#define PT_DYNO_R0VOLTAGECMD_MIN                        0.000000F
#define PT_DYNO_R0VOLTAGECMD_MAX                      511.000000F

#define PT_DYNO_MCUR0_MID                                  0X70FU

static inline uint32_t getRaw_PT_dyno_mcur0Mult(const PT_DYNO_MCUR0_U * DYNO_MCUR0)
{
	return (uint32_t)(DYNO_MCUR0->signalsStruct.dyno_mcur0Mult);
}
#define get_PT_dyno_mcur0Mult(message_s) getRaw_PT_dyno_mcur0Mult(message_s)

static inline uint32_t getRaw_PT_dyno_r0StateRequest(const PT_DYNO_MCUR0_U * DYNO_MCUR0)
{
	return (uint32_t)(DYNO_MCUR0->signalsStruct.multiUnion.m0.dyno_r0StateRequest);
}
#define get_PT_dyno_r0StateRequest(message_s) getRaw_PT_dyno_r0StateRequest(message_s)

static inline uint32_t getRaw_PT_dyno_r0VoltageCmd(const PT_DYNO_MCUR0_U * DYNO_MCUR0)
{
	return (uint32_t)(((uint32_t)DYNO_MCUR0->signalsStruct.multiUnion.m0.dyno_r0VoltageCmd_0 << 0)|
    ((uint32_t)DYNO_MCUR0->signalsStruct.multiUnion.m0.dyno_r0VoltageCmd_1 << 8));
}
static inline tFloat get_PT_dyno_r0VoltageCmd(PT_DYNO_MCUR0_U* DYNO_MCUR0, tFloat factor, tFloat offset)
{
    const tFloat convFactor =  PT_DYNO_R0VOLTAGECMD_FACTOR / factor;
    const tFloat convOffset = (PT_DYNO_R0VOLTAGECMD_OFFSET - offset)/ factor;
    
    return (tFloat)((getRaw_PT_dyno_r0VoltageCmd(DYNO_MCUR0) * convFactor) + convOffset);
}

/**
 * Message: VCU_torque
 * MID: 257
 * Network: PT
 * Transmitter: VCU
 * Receiver: MCUR0, MCUF0
 */
typedef union
{
    struct
    {
        uint32_t vcu_torqueR0Cmd:16;                     // Normal signal. Main motor torque command
        uint32_t doNotCare_0:16;                         // Normal signal. Assist motor torque command
        uint32_t vcu_torqueSplitRatioR:10;               // Normal signal. Torque split ratio on main motor
        uint32_t unused_0:22;                            // Normal signal. 
    } signalsStruct;
    uint32_t rawData[2];
} PT_VCU_torque_U;
#define PT_VCU_TORQUE_LEN                                      8U

#define PT_VCU_TORQUER0CMD_FACTOR                       0.040000F
#define PT_VCU_TORQUER0CMD_OFFSET                       0.000000F
#define PT_VCU_TORQUER0CMD_MIN                      -1300.000000F
#define PT_VCU_TORQUER0CMD_MAX                       1300.000000F
#define PT_VCU_TORQUESPLITRATIOR_FACTOR                 0.100000F
#define PT_VCU_TORQUESPLITRATIOR_OFFSET                 0.000000F
#define PT_VCU_TORQUESPLITRATIOR_MIN                    0.000000F
#define PT_VCU_TORQUESPLITRATIOR_MAX                  100.000000F

#define PT_VCU_TORQUE_MID                                  0X101U

static inline int32_t getRaw_PT_vcu_torqueR0Cmd(const PT_VCU_torque_U * VCU_torque)
{
	return (int32_t)(VCU_torque->signalsStruct.vcu_torqueR0Cmd);
}
static inline tFloat get_PT_vcu_torqueR0Cmd(PT_VCU_torque_U* VCU_torque, tFloat factor, tFloat offset)
{
    const tFloat convFactor =  PT_VCU_TORQUER0CMD_FACTOR / factor;
    const tFloat convOffset = (PT_VCU_TORQUER0CMD_OFFSET - offset)/ factor;
    
    return (tFloat)((getRaw_PT_vcu_torqueR0Cmd(VCU_torque) * convFactor) + convOffset);
}

static inline uint32_t getRaw_PT_vcu_torqueSplitRatioR(const PT_VCU_torque_U * VCU_torque)
{
	return (uint32_t)(VCU_torque->signalsStruct.vcu_torqueSplitRatioR);
}
static inline tFloat get_PT_vcu_torqueSplitRatioR(PT_VCU_torque_U* VCU_torque, tFloat factor, tFloat offset)
{
    const tFloat convFactor =  PT_VCU_TORQUESPLITRATIOR_FACTOR / factor;
    const tFloat convOffset = (PT_VCU_TORQUESPLITRATIOR_OFFSET - offset)/ factor;
    
    return (tFloat)((getRaw_PT_vcu_torqueSplitRatioR(VCU_torque) * convFactor) + convOffset);
}

#define PT_TOTAL_MSG_RECEIVING                                 2U

#endif /* _PT_DBC_RX_H_ */
