/**
 * This is an auto-generated file from C:\C\Common\CAN_PT.dbc. 
 * Generated at: 2017-07-27 10:36:49
 */

#ifndef _PT_DBC_TX_H_
#define _PT_DBC_TX_H_

#include "PT_dbc_common.h"

/**
 * Message: MCUR0_torque
 * MID: 258
 * Network: PT
 * Transmitter: MCUR0
 * Receiver: VCU
 */
typedef union
{
    struct
    {
        uint32_t mcur0_torqueAvbl:16;                    // Normal signal. MCU R0 available torque
        uint32_t mcur0_torqueCmdEcho:16;                 // Normal signal. MCU R0 torque command echo
        uint32_t mcur0_torqueEst:16;                     // Normal signal. MCU R0 estimated torque
        uint32_t mcur0_torqueChecksum:8;                 // Normal signal. 
        uint32_t unused_0:4;                             // Normal signal. 
        uint32_t mcur0_torqueCounter:4;                  // Normal signal. 
    } signalsStruct;
    uint32_t rawData[2];
} PT_MCUR0_torque_U;
#define PT_MCUR0_TORQUE_LEN                                    8U

#define PT_MCUR0_TORQUEAVBL_FACTOR                      0.040000F
#define PT_MCUR0_TORQUEAVBL_OFFSET                      0.000000F
#define PT_MCUR0_TORQUEAVBL_MIN                     -1300.000000F
#define PT_MCUR0_TORQUEAVBL_MAX                      1300.000000F
#define PT_MCUR0_TORQUECMDECHO_FACTOR                   0.040000F
#define PT_MCUR0_TORQUECMDECHO_OFFSET                   0.000000F
#define PT_MCUR0_TORQUECMDECHO_MIN                  -1300.000000F
#define PT_MCUR0_TORQUECMDECHO_MAX                   1300.000000F
#define PT_MCUR0_TORQUEEST_FACTOR                       0.040000F
#define PT_MCUR0_TORQUEEST_OFFSET                       0.000000F
#define PT_MCUR0_TORQUEEST_MIN                      -1300.000000F
#define PT_MCUR0_TORQUEEST_MAX                       1300.000000F
#define PT_MCUR0_TORQUECHECKSUM_FACTOR                  1.000000F
#define PT_MCUR0_TORQUECHECKSUM_OFFSET                  0.000000F
#define PT_MCUR0_TORQUECHECKSUM_MIN                     0.000000F
#define PT_MCUR0_TORQUECHECKSUM_MAX                    15.000000F
#define PT_MCUR0_TORQUECOUNTER_FACTOR                   1.000000F
#define PT_MCUR0_TORQUECOUNTER_OFFSET                   0.000000F
#define PT_MCUR0_TORQUECOUNTER_MIN                      0.000000F
#define PT_MCUR0_TORQUECOUNTER_MAX                     15.000000F

#define PT_MCUR0_TORQUE_MID                                0X102U

static inline void setRaw_PT_mcur0_torqueAvbl(PT_MCUR0_torque_U* MCUR0_torque, int32_t value)
{
    MCUR0_torque->signalsStruct.mcur0_torqueAvbl = value;
}
static inline void set_PT_mcur0_torqueAvbl(PT_MCUR0_torque_U* MCUR0_torque, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_TORQUEAVBL_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_TORQUEAVBL_OFFSET)/ PT_MCUR0_TORQUEAVBL_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_TORQUEAVBL_MIN)
	{result = PT_MCUR0_TORQUEAVBL_MIN;}

	if (result > PT_MCUR0_TORQUEAVBL_MAX)
	{result = PT_MCUR0_TORQUEAVBL_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_torqueAvbl(MCUR0_torque, (int32_t) result);
}

static inline void setRaw_PT_mcur0_torqueCmdEcho(PT_MCUR0_torque_U* MCUR0_torque, int32_t value)
{
    MCUR0_torque->signalsStruct.mcur0_torqueCmdEcho = value;
}
static inline void set_PT_mcur0_torqueCmdEcho(PT_MCUR0_torque_U* MCUR0_torque, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_TORQUECMDECHO_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_TORQUECMDECHO_OFFSET)/ PT_MCUR0_TORQUECMDECHO_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_TORQUECMDECHO_MIN)
	{result = PT_MCUR0_TORQUECMDECHO_MIN;}

	if (result > PT_MCUR0_TORQUECMDECHO_MAX)
	{result = PT_MCUR0_TORQUECMDECHO_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_torqueCmdEcho(MCUR0_torque, (int32_t) result);
}

static inline void setRaw_PT_mcur0_torqueEst(PT_MCUR0_torque_U* MCUR0_torque, int32_t value)
{
    MCUR0_torque->signalsStruct.mcur0_torqueEst = value;
}
static inline void set_PT_mcur0_torqueEst(PT_MCUR0_torque_U* MCUR0_torque, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_TORQUEEST_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_TORQUEEST_OFFSET)/ PT_MCUR0_TORQUEEST_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_TORQUEEST_MIN)
	{result = PT_MCUR0_TORQUEEST_MIN;}

	if (result > PT_MCUR0_TORQUEEST_MAX)
	{result = PT_MCUR0_TORQUEEST_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_torqueEst(MCUR0_torque, (int32_t) result);
}

static inline void setRaw_PT_mcur0_torqueChecksum(PT_MCUR0_torque_U* MCUR0_torque, uint32_t value)
{
    MCUR0_torque->signalsStruct.mcur0_torqueChecksum = value;
}
#define set_PT_mcur0_torqueChecksum(message_s, value) setRaw_PT_mcur0_torqueChecksum(message_s, value)

static inline void setRaw_PT_mcur0_torqueCounter(PT_MCUR0_torque_U* MCUR0_torque, uint32_t value)
{
    MCUR0_torque->signalsStruct.mcur0_torqueCounter = value;
}
#define set_PT_mcur0_torqueCounter(message_s, value) setRaw_PT_mcur0_torqueCounter(message_s, value)


/**
 * Message: MCUR0_state
 * MID: 274
 * Network: PT
 * Transmitter: MCUR0
 * Receiver: VCU
 */
typedef union
{
    struct
    {
        uint32_t mcur0_state:4;                          // Normal signal. MCU R0 state
        uint32_t mcur0_stateCounter:3;                   // Normal signal. 
        uint32_t unused_0:1;                             // Normal signal. 
        uint32_t mcur0_stateChecksum:8;                  // Normal signal. 
        uint32_t mcur0_speed_0:16;                       // Normal signal. MCU R0 motor speed
        uint32_t mcur0_speed_1:2;                        // Normal signal. MCU R0 motor speed
        uint32_t unused_1:30;                            // Normal signal. 
    } signalsStruct;
    uint32_t rawData[2];
} PT_MCUR0_state_U;
#define PT_MCUR0_STATE_LEN                                     8U

#define PT_MCUR0_STATE_FACTOR                           1.000000F
#define PT_MCUR0_STATE_OFFSET                           0.000000F
#define PT_MCUR0_STATE_MIN                              0.000000F
#define PT_MCUR0_STATE_MAX                             15.000000F
#define PT_MCUR0_STATECOUNTER_FACTOR                    1.000000F
#define PT_MCUR0_STATECOUNTER_OFFSET                    0.000000F
#define PT_MCUR0_STATECOUNTER_MIN                       0.000000F
#define PT_MCUR0_STATECOUNTER_MAX                       7.000000F
#define PT_MCUR0_STATECHECKSUM_FACTOR                   1.000000F
#define PT_MCUR0_STATECHECKSUM_OFFSET                   0.000000F
#define PT_MCUR0_STATECHECKSUM_MIN                      0.000000F
#define PT_MCUR0_STATECHECKSUM_MAX                     15.000000F
#define PT_MCUR0_SPEED_FACTOR                           0.200000F
#define PT_MCUR0_SPEED_OFFSET                           0.000000F
#define PT_MCUR0_SPEED_MIN                         -26000.000000F
#define PT_MCUR0_SPEED_MAX                          26000.000000F

#define PT_MCUR0_STATE_MID                                 0X112U

static inline void setRaw_PT_mcur0_state(PT_MCUR0_state_U* MCUR0_state, uint32_t value)
{
    MCUR0_state->signalsStruct.mcur0_state = value;
}
#define set_PT_mcur0_state(message_s, value) setRaw_PT_mcur0_state(message_s, value)

static inline void setRaw_PT_mcur0_stateCounter(PT_MCUR0_state_U* MCUR0_state, uint32_t value)
{
    MCUR0_state->signalsStruct.mcur0_stateCounter = value;
}
#define set_PT_mcur0_stateCounter(message_s, value) setRaw_PT_mcur0_stateCounter(message_s, value)

static inline void setRaw_PT_mcur0_stateChecksum(PT_MCUR0_state_U* MCUR0_state, uint32_t value)
{
    MCUR0_state->signalsStruct.mcur0_stateChecksum = value;
}
#define set_PT_mcur0_stateChecksum(message_s, value) setRaw_PT_mcur0_stateChecksum(message_s, value)

static inline void setRaw_PT_mcur0_speed(PT_MCUR0_state_U* MCUR0_state, int32_t value)
{
    MCUR0_state->signalsStruct.mcur0_speed_0 = (int32_t)((value >> 0) & 0X0FFFF);
    MCUR0_state->signalsStruct.mcur0_speed_1 = (int32_t)((value >> 16) & 0X00003);
}
static inline void set_PT_mcur0_speed(PT_MCUR0_state_U* MCUR0_state, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_SPEED_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_SPEED_OFFSET)/ PT_MCUR0_SPEED_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_SPEED_MIN)
	{result = PT_MCUR0_SPEED_MIN;}

	if (result > PT_MCUR0_SPEED_MAX)
	{result = PT_MCUR0_SPEED_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_speed(MCUR0_state, (int32_t) result);
}


/**
 * Message: MCUR0_state2
 * MID: 1026
 * Network: PT
 * Transmitter: MCUR0
 * Receiver: VCU, BMS, GTW
 */
typedef union
{
    struct
    {
        uint32_t mcur0_vBus:14;                          // Normal signal. 
        uint32_t mcur0_hvil:1;                           // Normal signal. 
        uint32_t unused_0:1;                             // Normal signal. 
        uint32_t mcur0_iBus:14;                          // Normal signal. 
        uint32_t unused_1:2;                             // Normal signal. 
        uint32_t mcur0_iRms:14;                          // Normal signal. 
        uint32_t unused_2:18;                            // Normal signal. 
    } signalsStruct;
    uint32_t rawData[2];
} PT_MCUR0_state2_U;
#define PT_MCUR0_STATE2_LEN                                    8U

#define PT_MCUR0_VBUS_FACTOR                            0.050000F
#define PT_MCUR0_VBUS_OFFSET                            0.000000F
#define PT_MCUR0_VBUS_MIN                               0.000000F
#define PT_MCUR0_VBUS_MAX                             800.000000F
typedef enum
{
    PT_MCUR0_HVIL_HVIL_OFF = 0,
    PT_MCUR0_HVIL_HVIL_ON = 1
} PT_MCUR0_HVIL_E;
#define PT_MCUR0_IBUS_FACTOR                            0.200000F
#define PT_MCUR0_IBUS_OFFSET                         -600.000000F
#define PT_MCUR0_IBUS_MIN                            -600.000000F
#define PT_MCUR0_IBUS_MAX                            2500.000000F
#define PT_MCUR0_IRMS_FACTOR                            0.125000F
#define PT_MCUR0_IRMS_OFFSET                            0.000000F
#define PT_MCUR0_IRMS_MIN                               0.000000F
#define PT_MCUR0_IRMS_MAX                            2000.000000F

#define PT_MCUR0_STATE2_MID                                0X402U

static inline void setRaw_PT_mcur0_vBus(PT_MCUR0_state2_U* MCUR0_state2, uint32_t value)
{
    MCUR0_state2->signalsStruct.mcur0_vBus = value;
}
static inline void set_PT_mcur0_vBus(PT_MCUR0_state2_U* MCUR0_state2, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_VBUS_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_VBUS_OFFSET)/ PT_MCUR0_VBUS_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_VBUS_MIN)
	{result = PT_MCUR0_VBUS_MIN;}

	if (result > PT_MCUR0_VBUS_MAX)
	{result = PT_MCUR0_VBUS_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_vBus(MCUR0_state2, (uint32_t) result);
}

static inline void set_PT_mcur0_hvil(PT_MCUR0_state2_U* MCUR0_state2, PT_MCUR0_HVIL_E value)
{
    MCUR0_state2->signalsStruct.mcur0_hvil = value;
}
static inline void setRaw_PT_mcur0_iBus(PT_MCUR0_state2_U* MCUR0_state2, uint32_t value)
{
    MCUR0_state2->signalsStruct.mcur0_iBus = value;
}
static inline void set_PT_mcur0_iBus(PT_MCUR0_state2_U* MCUR0_state2, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_IBUS_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_IBUS_OFFSET)/ PT_MCUR0_IBUS_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_IBUS_MIN)
	{result = PT_MCUR0_IBUS_MIN;}

	if (result > PT_MCUR0_IBUS_MAX)
	{result = PT_MCUR0_IBUS_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_iBus(MCUR0_state2, (uint32_t) result);
}

static inline void setRaw_PT_mcur0_iRms(PT_MCUR0_state2_U* MCUR0_state2, uint32_t value)
{
    MCUR0_state2->signalsStruct.mcur0_iRms = value;
}
static inline void set_PT_mcur0_iRms(PT_MCUR0_state2_U* MCUR0_state2, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_IRMS_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_IRMS_OFFSET)/ PT_MCUR0_IRMS_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_IRMS_MIN)
	{result = PT_MCUR0_IRMS_MIN;}

	if (result > PT_MCUR0_IRMS_MAX)
	{result = PT_MCUR0_IRMS_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_iRms(MCUR0_state2, (uint32_t) result);
}


/**
 * Message: MCUR0_temperature
 * MID: 1282
 * Network: PT
 * Transmitter: MCUR0
 * Receiver: VCU
 */
typedef union
{
    struct
    {
        uint32_t mcur0_tempCoolant:9;                    // Normal signal. 
        uint32_t unused_0:14;                            // Normal signal. 
        uint32_t mcur0_tempStator:9;                     // Normal signal. 
        uint32_t mcur0_tempHeatSinkPhA:9;                // Normal signal. 
        uint32_t mcur0_tempHeatSinkPhB:9;                // Normal signal. 
        uint32_t unused_1:5;                             // Normal signal. 
        uint32_t mcur0_tempHeatSinkPhC:9;                // Normal signal. 
    } signalsStruct;
    uint32_t rawData[2];
} PT_MCUR0_temperature_U;
#define PT_MCUR0_TEMPERATURE_LEN                               8U

#define PT_MCUR0_TEMPCOOLANT_FACTOR                     0.500000F
#define PT_MCUR0_TEMPCOOLANT_OFFSET                   -40.000000F
#define PT_MCUR0_TEMPCOOLANT_MIN                      -40.000000F
#define PT_MCUR0_TEMPCOOLANT_MAX                      215.500000F
#define PT_MCUR0_TEMPSTATOR_FACTOR                      0.500000F
#define PT_MCUR0_TEMPSTATOR_OFFSET                    -40.000000F
#define PT_MCUR0_TEMPSTATOR_MIN                       -40.000000F
#define PT_MCUR0_TEMPSTATOR_MAX                       215.500000F
#define PT_MCUR0_TEMPHEATSINKPHA_FACTOR                 0.500000F
#define PT_MCUR0_TEMPHEATSINKPHA_OFFSET               -40.000000F
#define PT_MCUR0_TEMPHEATSINKPHA_MIN                  -40.000000F
#define PT_MCUR0_TEMPHEATSINKPHA_MAX                  215.500000F
#define PT_MCUR0_TEMPHEATSINKPHB_FACTOR                 0.500000F
#define PT_MCUR0_TEMPHEATSINKPHB_OFFSET               -40.000000F
#define PT_MCUR0_TEMPHEATSINKPHB_MIN                  -40.000000F
#define PT_MCUR0_TEMPHEATSINKPHB_MAX                  215.500000F
#define PT_MCUR0_TEMPHEATSINKPHC_FACTOR                 0.500000F
#define PT_MCUR0_TEMPHEATSINKPHC_OFFSET               -40.000000F
#define PT_MCUR0_TEMPHEATSINKPHC_MIN                  -40.000000F
#define PT_MCUR0_TEMPHEATSINKPHC_MAX                  215.500000F

#define PT_MCUR0_TEMPERATURE_MID                           0X502U

static inline void setRaw_PT_mcur0_tempCoolant(PT_MCUR0_temperature_U* MCUR0_temperature, uint32_t value)
{
    MCUR0_temperature->signalsStruct.mcur0_tempCoolant = value;
}
static inline void set_PT_mcur0_tempCoolant(PT_MCUR0_temperature_U* MCUR0_temperature, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_TEMPCOOLANT_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_TEMPCOOLANT_OFFSET)/ PT_MCUR0_TEMPCOOLANT_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_TEMPCOOLANT_MIN)
	{result = PT_MCUR0_TEMPCOOLANT_MIN;}

	if (result > PT_MCUR0_TEMPCOOLANT_MAX)
	{result = PT_MCUR0_TEMPCOOLANT_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_tempCoolant(MCUR0_temperature, (uint32_t) result);
}

static inline void setRaw_PT_mcur0_tempStator(PT_MCUR0_temperature_U* MCUR0_temperature, uint32_t value)
{
    MCUR0_temperature->signalsStruct.mcur0_tempStator = value;
}
static inline void set_PT_mcur0_tempStator(PT_MCUR0_temperature_U* MCUR0_temperature, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_TEMPSTATOR_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_TEMPSTATOR_OFFSET)/ PT_MCUR0_TEMPSTATOR_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_TEMPSTATOR_MIN)
	{result = PT_MCUR0_TEMPSTATOR_MIN;}

	if (result > PT_MCUR0_TEMPSTATOR_MAX)
	{result = PT_MCUR0_TEMPSTATOR_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_tempStator(MCUR0_temperature, (uint32_t) result);
}

static inline void setRaw_PT_mcur0_tempHeatSinkPhA(PT_MCUR0_temperature_U* MCUR0_temperature, uint32_t value)
{
    MCUR0_temperature->signalsStruct.mcur0_tempHeatSinkPhA = value;
}
static inline void set_PT_mcur0_tempHeatSinkPhA(PT_MCUR0_temperature_U* MCUR0_temperature, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_TEMPHEATSINKPHA_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_TEMPHEATSINKPHA_OFFSET)/ PT_MCUR0_TEMPHEATSINKPHA_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_TEMPHEATSINKPHA_MIN)
	{result = PT_MCUR0_TEMPHEATSINKPHA_MIN;}

	if (result > PT_MCUR0_TEMPHEATSINKPHA_MAX)
	{result = PT_MCUR0_TEMPHEATSINKPHA_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_tempHeatSinkPhA(MCUR0_temperature, (uint32_t) result);
}

static inline void setRaw_PT_mcur0_tempHeatSinkPhB(PT_MCUR0_temperature_U* MCUR0_temperature, uint32_t value)
{
    MCUR0_temperature->signalsStruct.mcur0_tempHeatSinkPhB = value;
}
static inline void set_PT_mcur0_tempHeatSinkPhB(PT_MCUR0_temperature_U* MCUR0_temperature, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_TEMPHEATSINKPHB_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_TEMPHEATSINKPHB_OFFSET)/ PT_MCUR0_TEMPHEATSINKPHB_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_TEMPHEATSINKPHB_MIN)
	{result = PT_MCUR0_TEMPHEATSINKPHB_MIN;}

	if (result > PT_MCUR0_TEMPHEATSINKPHB_MAX)
	{result = PT_MCUR0_TEMPHEATSINKPHB_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_tempHeatSinkPhB(MCUR0_temperature, (uint32_t) result);
}

static inline void setRaw_PT_mcur0_tempHeatSinkPhC(PT_MCUR0_temperature_U* MCUR0_temperature, uint32_t value)
{
    MCUR0_temperature->signalsStruct.mcur0_tempHeatSinkPhC = value;
}
static inline void set_PT_mcur0_tempHeatSinkPhC(PT_MCUR0_temperature_U* MCUR0_temperature, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_TEMPHEATSINKPHC_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_TEMPHEATSINKPHC_OFFSET)/ PT_MCUR0_TEMPHEATSINKPHC_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_TEMPHEATSINKPHC_MIN)
	{result = PT_MCUR0_TEMPHEATSINKPHC_MIN;}

	if (result > PT_MCUR0_TEMPHEATSINKPHC_MAX)
	{result = PT_MCUR0_TEMPHEATSINKPHC_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_tempHeatSinkPhC(MCUR0_temperature, (uint32_t) result);
}


/**
 * Message: MCUR0_debug
 * MID: 1794
 * Network: PT
 * Transmitter: MCUR0
 * Receiver: Vector__XXX
 */
typedef union
{
    struct
    {
        uint32_t mcur0_debugMult:8;                      // Multiplexer index. 
        union
        {
            struct
            {
                uint32_t mcur0_slip:12;                  // Multiplexer signal. 
                uint32_t mcur0_statorFlux:12;            // Multiplexer signal. 
                uint32_t mcur0_idFdb:14;                 // Multiplexer signal. 
                uint32_t unused_0:4;                     // Multiplexer signal. 
                uint32_t mcur0_iqFdb:14;                 // Multiplexer signal. 
            } m0;
        } multiUnion;
    } signalsStruct;
    uint32_t rawData[2];
} PT_MCUR0_debug_U;
#define PT_MCUR0_DEBUG_LEN                                     8U

#define PT_MCUR0_DEBUGMULT_FACTOR                       1.000000F
#define PT_MCUR0_DEBUGMULT_OFFSET                       0.000000F
#define PT_MCUR0_DEBUGMULT_MIN                          0.000000F
#define PT_MCUR0_DEBUGMULT_MAX                        255.000000F
#define PT_MCUR0_SLIP_FACTOR                            0.010000F
#define PT_MCUR0_SLIP_OFFSET                            0.000000F
#define PT_MCUR0_SLIP_MIN                             -20.000000F
#define PT_MCUR0_SLIP_MAX                              20.000000F
#define PT_MCUR0_STATORFLUX_FACTOR                      1.000000F
#define PT_MCUR0_STATORFLUX_OFFSET                      0.000000F
#define PT_MCUR0_STATORFLUX_MIN                         0.000000F
#define PT_MCUR0_STATORFLUX_MAX                         0.000000F
#define PT_MCUR0_IDFDB_FACTOR                           0.200000F
#define PT_MCUR0_IDFDB_OFFSET                           0.000000F
#define PT_MCUR0_IDFDB_MIN                          -1600.000000F
#define PT_MCUR0_IDFDB_MAX                           1600.000000F
#define PT_MCUR0_IQFDB_FACTOR                           0.200000F
#define PT_MCUR0_IQFDB_OFFSET                           0.000000F
#define PT_MCUR0_IQFDB_MIN                          -1600.000000F
#define PT_MCUR0_IQFDB_MAX                           1600.000000F

#define PT_MCUR0_DEBUG_MID                                 0X702U

static inline void setRaw_PT_mcur0_debugMult(PT_MCUR0_debug_U* MCUR0_debug, uint32_t value)
{
    MCUR0_debug->signalsStruct.mcur0_debugMult = value;
}
#define set_PT_mcur0_debugMult(message_s, value) setRaw_PT_mcur0_debugMult(message_s, value)

static inline void setRaw_PT_mcur0_slip(PT_MCUR0_debug_U* MCUR0_debug, int32_t value)
{
    MCUR0_debug->signalsStruct.multiUnion.m0.mcur0_slip = value;
}
static inline void set_PT_mcur0_slip(PT_MCUR0_debug_U* MCUR0_debug, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_SLIP_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_SLIP_OFFSET)/ PT_MCUR0_SLIP_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_SLIP_MIN)
	{result = PT_MCUR0_SLIP_MIN;}

	if (result > PT_MCUR0_SLIP_MAX)
	{result = PT_MCUR0_SLIP_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_slip(MCUR0_debug, (int32_t) result);
}

static inline void setRaw_PT_mcur0_statorFlux(PT_MCUR0_debug_U* MCUR0_debug, uint32_t value)
{
    MCUR0_debug->signalsStruct.multiUnion.m0.mcur0_statorFlux = value;
}
static inline void set_PT_mcur0_statorFlux(PT_MCUR0_debug_U* MCUR0_debug, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_STATORFLUX_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_STATORFLUX_OFFSET)/ PT_MCUR0_STATORFLUX_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_STATORFLUX_MIN)
	{result = PT_MCUR0_STATORFLUX_MIN;}

	if (result > PT_MCUR0_STATORFLUX_MAX)
	{result = PT_MCUR0_STATORFLUX_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_statorFlux(MCUR0_debug, (uint32_t) result);
}

static inline void setRaw_PT_mcur0_idFdb(PT_MCUR0_debug_U* MCUR0_debug, int32_t value)
{
    MCUR0_debug->signalsStruct.multiUnion.m0.mcur0_idFdb = value;
}
static inline void set_PT_mcur0_idFdb(PT_MCUR0_debug_U* MCUR0_debug, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_IDFDB_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_IDFDB_OFFSET)/ PT_MCUR0_IDFDB_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_IDFDB_MIN)
	{result = PT_MCUR0_IDFDB_MIN;}

	if (result > PT_MCUR0_IDFDB_MAX)
	{result = PT_MCUR0_IDFDB_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_idFdb(MCUR0_debug, (int32_t) result);
}

static inline void setRaw_PT_mcur0_iqFdb(PT_MCUR0_debug_U* MCUR0_debug, int32_t value)
{
    MCUR0_debug->signalsStruct.multiUnion.m0.mcur0_iqFdb = value;
}
static inline void set_PT_mcur0_iqFdb(PT_MCUR0_debug_U* MCUR0_debug, tFloat value, tFloat factor, tFloat offset)
{
	const tFloat convFactor = factor / PT_MCUR0_IQFDB_FACTOR;
	const tFloat convOffset = (offset - PT_MCUR0_IQFDB_OFFSET)/ PT_MCUR0_IQFDB_FACTOR;
	tFloat result = (tFloat)(value * convFactor + convOffset);

	if (result < PT_MCUR0_IQFDB_MIN)
	{result = PT_MCUR0_IQFDB_MIN;}

	if (result > PT_MCUR0_IQFDB_MAX)
	{result = PT_MCUR0_IQFDB_MAX;}

	if (result > 0.0)
	{result += 0.5F;}
	else
	{result -= 0.5F;}

	setRaw_PT_mcur0_iqFdb(MCUR0_debug, (int32_t) result);
}


#define PT_TOTAL_MSG_TRANSMITTING                              5U

#endif /* _PT_DBC_TX_H_ */
