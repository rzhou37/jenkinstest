
/***************************************************************************************************
* SF Motors Confidential � Copyright [2017], all rights reserved.
* Author: xiaoxi.sun@sfmotors.com
* Date: 2017-07-12
* Description: header file for flexcan drivers
***************************************************************************************************/

#ifndef FLEXCAN_INIT_H_
#define FLEXCAN_INIT_H_

extern void flexcan0_init (void);

#endif /* FLEXCAN_INIT_H_ */
